<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_a731ad127d7544b0d030d170b4a8d8e9bd166147c7bb9cf5d4e22a3caf9ddc9e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_04c4c024c23c0ff11cf93a7e9cd75148cbc6aa6bed46995fe1ef5645352affae = $this->env->getExtension("native_profiler");
        $__internal_04c4c024c23c0ff11cf93a7e9cd75148cbc6aa6bed46995fe1ef5645352affae->enter($__internal_04c4c024c23c0ff11cf93a7e9cd75148cbc6aa6bed46995fe1ef5645352affae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_04c4c024c23c0ff11cf93a7e9cd75148cbc6aa6bed46995fe1ef5645352affae->leave($__internal_04c4c024c23c0ff11cf93a7e9cd75148cbc6aa6bed46995fe1ef5645352affae_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_107f04c90a4e427132b63c9166a7539ca3a9033d9fe271905e57e6b796b1c1f2 = $this->env->getExtension("native_profiler");
        $__internal_107f04c90a4e427132b63c9166a7539ca3a9033d9fe271905e57e6b796b1c1f2->enter($__internal_107f04c90a4e427132b63c9166a7539ca3a9033d9fe271905e57e6b796b1c1f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Profile:show_content.html.twig", "FOSUserBundle:Profile:show.html.twig", 4)->display($context);
        
        $__internal_107f04c90a4e427132b63c9166a7539ca3a9033d9fe271905e57e6b796b1c1f2->leave($__internal_107f04c90a4e427132b63c9166a7539ca3a9033d9fe271905e57e6b796b1c1f2_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Profile:show_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
