<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_1d6ab75cde782f7d6bc2f3a2918f2dbaec953d82dd915410f224bbc6f30deaf3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4f122522181b858b2a08b4b0ecde70ae42afa48eb0aec8238d9f07dcc6de5952 = $this->env->getExtension("native_profiler");
        $__internal_4f122522181b858b2a08b4b0ecde70ae42afa48eb0aec8238d9f07dcc6de5952->enter($__internal_4f122522181b858b2a08b4b0ecde70ae42afa48eb0aec8238d9f07dcc6de5952_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_4f122522181b858b2a08b4b0ecde70ae42afa48eb0aec8238d9f07dcc6de5952->leave($__internal_4f122522181b858b2a08b4b0ecde70ae42afa48eb0aec8238d9f07dcc6de5952_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/* <?php foreach ($form as $child): ?>*/
/*     <?php echo $view['form']->widget($child) ?>*/
/*     <?php echo $view['form']->label($child, null, array('translation_domain' => $choice_translation_domain)) ?>*/
/* <?php endforeach ?>*/
/* </div>*/
/* */
