<?php

/* @Framework/Form/button_widget.html.php */
class __TwigTemplate_1348cbceefff8f73518b0959d8d1b1c6d2c2b401b7d34bbbd9dfbc60582a77a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_95c568c8ff225535bc3fc119a3ff27faa63e7ad1e8654e395f801ee36570c812 = $this->env->getExtension("native_profiler");
        $__internal_95c568c8ff225535bc3fc119a3ff27faa63e7ad1e8654e395f801ee36570c812->enter($__internal_95c568c8ff225535bc3fc119a3ff27faa63e7ad1e8654e395f801ee36570c812_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        // line 1
        echo "<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
";
        
        $__internal_95c568c8ff225535bc3fc119a3ff27faa63e7ad1e8654e395f801ee36570c812->leave($__internal_95c568c8ff225535bc3fc119a3ff27faa63e7ad1e8654e395f801ee36570c812_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!$label) { $label = isset($label_format)*/
/*     ? strtr($label_format, array('%name%' => $name, '%id%' => $id))*/
/*     : $view['form']->humanize($name); } ?>*/
/* <button type="<?php echo isset($type) ? $view->escape($type) : 'button' ?>" <?php echo $view['form']->block($form, 'button_attributes') ?>><?php echo $view->escape(false !== $translation_domain ? $view['translator']->trans($label, array(), $translation_domain) : $label) ?></button>*/
/* */
