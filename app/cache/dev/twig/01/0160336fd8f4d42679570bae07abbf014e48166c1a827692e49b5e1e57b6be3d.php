<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_a95ce441e25b2ab5090ef4f93685f60e8ffc4919c8abaf419b18d03a9115de2b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7a755608c42454bf6eece9db2e9b5d3523c7de8fad0ab64bb296e393c17181e2 = $this->env->getExtension("native_profiler");
        $__internal_7a755608c42454bf6eece9db2e9b5d3523c7de8fad0ab64bb296e393c17181e2->enter($__internal_7a755608c42454bf6eece9db2e9b5d3523c7de8fad0ab64bb296e393c17181e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        $this->displayBlock('body_text', $context, $blocks);
        // line 12
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_7a755608c42454bf6eece9db2e9b5d3523c7de8fad0ab64bb296e393c17181e2->leave($__internal_7a755608c42454bf6eece9db2e9b5d3523c7de8fad0ab64bb296e393c17181e2_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_ecf6db1be90650fb6272d5de05982ebfd964635d8d79d816312b8aee2c0969e2 = $this->env->getExtension("native_profiler");
        $__internal_ecf6db1be90650fb6272d5de05982ebfd964635d8d79d816312b8aee2c0969e2->enter($__internal_ecf6db1be90650fb6272d5de05982ebfd964635d8d79d816312b8aee2c0969e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('translator')->trans("resetting.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle");
        echo "
";
        
        $__internal_ecf6db1be90650fb6272d5de05982ebfd964635d8d79d816312b8aee2c0969e2->leave($__internal_ecf6db1be90650fb6272d5de05982ebfd964635d8d79d816312b8aee2c0969e2_prof);

    }

    // line 7
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_f6153fcca821dba659c3ff705cc8ef6d4cff45d0484d356b196d51229278bb5c = $this->env->getExtension("native_profiler");
        $__internal_f6153fcca821dba659c3ff705cc8ef6d4cff45d0484d356b196d51229278bb5c->enter($__internal_f6153fcca821dba659c3ff705cc8ef6d4cff45d0484d356b196d51229278bb5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 9
        echo $this->env->getExtension('translator')->trans("resetting.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_f6153fcca821dba659c3ff705cc8ef6d4cff45d0484d356b196d51229278bb5c->leave($__internal_f6153fcca821dba659c3ff705cc8ef6d4cff45d0484d356b196d51229278bb5c_prof);

    }

    // line 12
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_a731a5181a18c59c351bf2e4e564d2bb76e300d8797535227ea265cd686cb7e6 = $this->env->getExtension("native_profiler");
        $__internal_a731a5181a18c59c351bf2e4e564d2bb76e300d8797535227ea265cd686cb7e6->enter($__internal_a731a5181a18c59c351bf2e4e564d2bb76e300d8797535227ea265cd686cb7e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_a731a5181a18c59c351bf2e4e564d2bb76e300d8797535227ea265cd686cb7e6->leave($__internal_a731a5181a18c59c351bf2e4e564d2bb76e300d8797535227ea265cd686cb7e6_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  66 => 12,  57 => 9,  51 => 7,  42 => 4,  36 => 2,  29 => 12,  27 => 7,  25 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* {% block subject %}*/
/* {% autoescape false %}*/
/* {{ 'resetting.email.subject'|trans({'%username%': user.username}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_text %}*/
/* {% autoescape false %}*/
/* {{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_html %}{% endblock %}*/
/* */
