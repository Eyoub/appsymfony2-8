<?php

/* @FOSUser/Resetting/reset.html.twig */
class __TwigTemplate_aa669ee635f09a1624692123ba93677ef654b1333c95098baccb89acbb5cde2d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Resetting/reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b3dbc5168fa23f49de369684f51eb3a0725aa4fe08cd2c29d643b1689957c93a = $this->env->getExtension("native_profiler");
        $__internal_b3dbc5168fa23f49de369684f51eb3a0725aa4fe08cd2c29d643b1689957c93a->enter($__internal_b3dbc5168fa23f49de369684f51eb3a0725aa4fe08cd2c29d643b1689957c93a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b3dbc5168fa23f49de369684f51eb3a0725aa4fe08cd2c29d643b1689957c93a->leave($__internal_b3dbc5168fa23f49de369684f51eb3a0725aa4fe08cd2c29d643b1689957c93a_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_0ca87e96f8eb2e952bfef9c538e9ad7c08cf8112488fd90fd7ef91b5c2906ac1 = $this->env->getExtension("native_profiler");
        $__internal_0ca87e96f8eb2e952bfef9c538e9ad7c08cf8112488fd90fd7ef91b5c2906ac1->enter($__internal_0ca87e96f8eb2e952bfef9c538e9ad7c08cf8112488fd90fd7ef91b5c2906ac1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Resetting:reset_content.html.twig", "@FOSUser/Resetting/reset.html.twig", 4)->display($context);
        
        $__internal_0ca87e96f8eb2e952bfef9c538e9ad7c08cf8112488fd90fd7ef91b5c2906ac1->leave($__internal_0ca87e96f8eb2e952bfef9c538e9ad7c08cf8112488fd90fd7ef91b5c2906ac1_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Resetting:reset_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
