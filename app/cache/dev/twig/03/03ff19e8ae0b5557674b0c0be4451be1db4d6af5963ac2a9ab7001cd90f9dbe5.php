<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_e0a2443007d500bffc6bb571d070304c538c1ddc18d79c3f6854656e4127d954 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_676c52fce632707c3375373e6d5aee2296bc7c7881c5c6f7e5fe8833195f9ac4 = $this->env->getExtension("native_profiler");
        $__internal_676c52fce632707c3375373e6d5aee2296bc7c7881c5c6f7e5fe8833195f9ac4->enter($__internal_676c52fce632707c3375373e6d5aee2296bc7c7881c5c6f7e5fe8833195f9ac4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_676c52fce632707c3375373e6d5aee2296bc7c7881c5c6f7e5fe8833195f9ac4->leave($__internal_676c52fce632707c3375373e6d5aee2296bc7c7881c5c6f7e5fe8833195f9ac4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'submit')) ?>*/
/* */
