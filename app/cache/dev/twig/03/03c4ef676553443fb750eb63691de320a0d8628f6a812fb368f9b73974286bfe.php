<?php

/* @Twig/Exception/exception.rdf.twig */
class __TwigTemplate_42d99da8418699c313448401b44c085f1800a28a7b02cf6e416aeefa25353049 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c80ed498dd0c8945d1647660de1311b40d233bd4c61eba1534331c45df5d1e69 = $this->env->getExtension("native_profiler");
        $__internal_c80ed498dd0c8945d1647660de1311b40d233bd4c61eba1534331c45df5d1e69->enter($__internal_c80ed498dd0c8945d1647660de1311b40d233bd4c61eba1534331c45df5d1e69_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "@Twig/Exception/exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_c80ed498dd0c8945d1647660de1311b40d233bd4c61eba1534331c45df5d1e69->leave($__internal_c80ed498dd0c8945d1647660de1311b40d233bd4c61eba1534331c45df5d1e69_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
