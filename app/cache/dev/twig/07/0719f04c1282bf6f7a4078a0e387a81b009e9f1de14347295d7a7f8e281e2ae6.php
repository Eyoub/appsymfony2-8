<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_ecfe25936c358b6b3de6b5ece15bcc3798da2fe92d902f8f4df1d47fcf4e8399 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5aab254be401c228736fa6de7b202f367546d7959c70282923743d78ecedbe5f = $this->env->getExtension("native_profiler");
        $__internal_5aab254be401c228736fa6de7b202f367546d7959c70282923743d78ecedbe5f->enter($__internal_5aab254be401c228736fa6de7b202f367546d7959c70282923743d78ecedbe5f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5aab254be401c228736fa6de7b202f367546d7959c70282923743d78ecedbe5f->leave($__internal_5aab254be401c228736fa6de7b202f367546d7959c70282923743d78ecedbe5f_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_3c6fd721b5d6509ca77643a5ca0b5ac109a589d7fefa59b7bfcad59f3d382ab8 = $this->env->getExtension("native_profiler");
        $__internal_3c6fd721b5d6509ca77643a5ca0b5ac109a589d7fefa59b7bfcad59f3d382ab8->enter($__internal_3c6fd721b5d6509ca77643a5ca0b5ac109a589d7fefa59b7bfcad59f3d382ab8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_3c6fd721b5d6509ca77643a5ca0b5ac109a589d7fefa59b7bfcad59f3d382ab8->leave($__internal_3c6fd721b5d6509ca77643a5ca0b5ac109a589d7fefa59b7bfcad59f3d382ab8_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_6764c595510629a08321021cff8ccd819e4c9313e38e9d25cb8a0e5daf02a8ed = $this->env->getExtension("native_profiler");
        $__internal_6764c595510629a08321021cff8ccd819e4c9313e38e9d25cb8a0e5daf02a8ed->enter($__internal_6764c595510629a08321021cff8ccd819e4c9313e38e9d25cb8a0e5daf02a8ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_6764c595510629a08321021cff8ccd819e4c9313e38e9d25cb8a0e5daf02a8ed->leave($__internal_6764c595510629a08321021cff8ccd819e4c9313e38e9d25cb8a0e5daf02a8ed_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_0f75bf0ea62e77d65fb09ec4ccab2c156425956b34173ba510d30a940690e1c1 = $this->env->getExtension("native_profiler");
        $__internal_0f75bf0ea62e77d65fb09ec4ccab2c156425956b34173ba510d30a940690e1c1->enter($__internal_0f75bf0ea62e77d65fb09ec4ccab2c156425956b34173ba510d30a940690e1c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_0f75bf0ea62e77d65fb09ec4ccab2c156425956b34173ba510d30a940690e1c1->leave($__internal_0f75bf0ea62e77d65fb09ec4ccab2c156425956b34173ba510d30a940690e1c1_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
