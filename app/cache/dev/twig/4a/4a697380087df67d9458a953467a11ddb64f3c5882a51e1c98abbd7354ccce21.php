<?php

/* FOSUserBundle:Group:list.html.twig */
class __TwigTemplate_443429f05185f311ff9c9d3bf7cc710c10677c34fe44fe311e7b3ac809643325 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Group:list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b59456205d11ec3c25f36ec3fe845878ca5fc7dac6515f618293e36d3f7a092c = $this->env->getExtension("native_profiler");
        $__internal_b59456205d11ec3c25f36ec3fe845878ca5fc7dac6515f618293e36d3f7a092c->enter($__internal_b59456205d11ec3c25f36ec3fe845878ca5fc7dac6515f618293e36d3f7a092c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b59456205d11ec3c25f36ec3fe845878ca5fc7dac6515f618293e36d3f7a092c->leave($__internal_b59456205d11ec3c25f36ec3fe845878ca5fc7dac6515f618293e36d3f7a092c_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_39fd1ac02110477118beef33722a9b867f5ec83d4b6ad48a1fdf45efee3b44b1 = $this->env->getExtension("native_profiler");
        $__internal_39fd1ac02110477118beef33722a9b867f5ec83d4b6ad48a1fdf45efee3b44b1->enter($__internal_39fd1ac02110477118beef33722a9b867f5ec83d4b6ad48a1fdf45efee3b44b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:list_content.html.twig", "FOSUserBundle:Group:list.html.twig", 4)->display($context);
        
        $__internal_39fd1ac02110477118beef33722a9b867f5ec83d4b6ad48a1fdf45efee3b44b1->leave($__internal_39fd1ac02110477118beef33722a9b867f5ec83d4b6ad48a1fdf45efee3b44b1_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:list_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
