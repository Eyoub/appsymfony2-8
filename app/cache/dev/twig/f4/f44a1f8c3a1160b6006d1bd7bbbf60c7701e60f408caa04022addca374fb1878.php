<?php

/* FOSUserBundle:Group:show.html.twig */
class __TwigTemplate_55207d2916f052e16fe214a8391fab9cb52aab58967e89ade221a46a5da9d93d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Group:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f731739ac192a29eb979ffa8d8cdc2daf64cc22c8ab0a753d6fbd3d48510528b = $this->env->getExtension("native_profiler");
        $__internal_f731739ac192a29eb979ffa8d8cdc2daf64cc22c8ab0a753d6fbd3d48510528b->enter($__internal_f731739ac192a29eb979ffa8d8cdc2daf64cc22c8ab0a753d6fbd3d48510528b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f731739ac192a29eb979ffa8d8cdc2daf64cc22c8ab0a753d6fbd3d48510528b->leave($__internal_f731739ac192a29eb979ffa8d8cdc2daf64cc22c8ab0a753d6fbd3d48510528b_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_96775b7c62cb335cc05f3952c1796f9606e84b3feca8b1e83311c5045de5a55f = $this->env->getExtension("native_profiler");
        $__internal_96775b7c62cb335cc05f3952c1796f9606e84b3feca8b1e83311c5045de5a55f->enter($__internal_96775b7c62cb335cc05f3952c1796f9606e84b3feca8b1e83311c5045de5a55f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:show_content.html.twig", "FOSUserBundle:Group:show.html.twig", 4)->display($context);
        
        $__internal_96775b7c62cb335cc05f3952c1796f9606e84b3feca8b1e83311c5045de5a55f->leave($__internal_96775b7c62cb335cc05f3952c1796f9606e84b3feca8b1e83311c5045de5a55f_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:show_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
