<?php

/* film/new.html.twig */
class __TwigTemplate_00742048866e87ba62583742d2775de5eb3793b8798ce1907a8174c5d75934c6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FilmBundle::index.html.twig", "film/new.html.twig", 1);
        $this->blocks = array(
            'A' => array($this, 'block_A'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FilmBundle::index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_335e5cd7e0503434d8d404c33d3e9c2d13305596a645af83ad7935df0c13fca3 = $this->env->getExtension("native_profiler");
        $__internal_335e5cd7e0503434d8d404c33d3e9c2d13305596a645af83ad7935df0c13fca3->enter($__internal_335e5cd7e0503434d8d404c33d3e9c2d13305596a645af83ad7935df0c13fca3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "film/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_335e5cd7e0503434d8d404c33d3e9c2d13305596a645af83ad7935df0c13fca3->leave($__internal_335e5cd7e0503434d8d404c33d3e9c2d13305596a645af83ad7935df0c13fca3_prof);

    }

    // line 3
    public function block_A($context, array $blocks = array())
    {
        $__internal_bcef3c43f642e048e862bd959e33a0b8aeb5fa4e2215b9534019cf117ddba829 = $this->env->getExtension("native_profiler");
        $__internal_bcef3c43f642e048e862bd959e33a0b8aeb5fa4e2215b9534019cf117ddba829->enter($__internal_bcef3c43f642e048e862bd959e33a0b8aeb5fa4e2215b9534019cf117ddba829_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "A"));

        // line 4
        echo "
    <div class=\"row\">

            <div class=\"col-md-3\">
                <p class=\"lead\">Gestion des filmes</p>
                <div class=\"list-group\">
                    <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("film_index");
        echo "\" class=\"list-group-item\">lister</a>
                    <a href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("film_new");
        echo "\" class=\"list-group-item\">creer</a>
                    <a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("film_editer");
        echo "\" class=\"list-group-item\">editer</a>
                </div>
            </div>
<div class=\"col-md-9\">

    ";
        // line 17
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 20
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
</div>
</div>
";
        
        $__internal_bcef3c43f642e048e862bd959e33a0b8aeb5fa4e2215b9534019cf117ddba829->leave($__internal_bcef3c43f642e048e862bd959e33a0b8aeb5fa4e2215b9534019cf117ddba829_prof);

    }

    public function getTemplateName()
    {
        return "film/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 20,  68 => 18,  64 => 17,  56 => 12,  52 => 11,  48 => 10,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'FilmBundle::index.html.twig' %}*/
/* */
/* {% block A %}*/
/* */
/*     <div class="row">*/
/* */
/*             <div class="col-md-3">*/
/*                 <p class="lead">Gestion des filmes</p>*/
/*                 <div class="list-group">*/
/*                     <a href="{{path('film_index')}}" class="list-group-item">lister</a>*/
/*                     <a href="{{path('film_new')}}" class="list-group-item">creer</a>*/
/*                     <a href="{{path('film_editer')}}" class="list-group-item">editer</a>*/
/*                 </div>*/
/*             </div>*/
/* <div class="col-md-9">*/
/* */
/*     {{ form_start(form) }}*/
/*         {{ form_widget(form) }}*/
/*         <input type="submit" value="Create" />*/
/*     {{ form_end(form) }}*/
/* </div>*/
/* </div>*/
/* {% endblock %}*/
/* */
