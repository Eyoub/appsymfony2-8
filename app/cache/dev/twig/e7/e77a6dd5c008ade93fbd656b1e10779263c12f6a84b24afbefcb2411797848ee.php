<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_cd88baba79171d56402db8d49aa02fb5589b61d2b8888ab09ac4b56e3a0423ec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3d2bc018f292bdfee1a8ac9e0ed67db240205ff1d81c1e24a8743c68c9c0e6d0 = $this->env->getExtension("native_profiler");
        $__internal_3d2bc018f292bdfee1a8ac9e0ed67db240205ff1d81c1e24a8743c68c9c0e6d0->enter($__internal_3d2bc018f292bdfee1a8ac9e0ed67db240205ff1d81c1e24a8743c68c9c0e6d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_3d2bc018f292bdfee1a8ac9e0ed67db240205ff1d81c1e24a8743c68c9c0e6d0->leave($__internal_3d2bc018f292bdfee1a8ac9e0ed67db240205ff1d81c1e24a8743c68c9c0e6d0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
