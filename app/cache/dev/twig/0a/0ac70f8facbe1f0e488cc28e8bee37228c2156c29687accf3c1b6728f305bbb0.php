<?php

/* @Twig/Exception/error.atom.twig */
class __TwigTemplate_8842a1c95de1e1d4b506a09c3d109e2a26d1924c2bd7ebe0c4c1c67ce18e61f7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_67ddda6ae1e6714cb1150c35801d952683ed47ce37ff56e1b106a1d07c941556 = $this->env->getExtension("native_profiler");
        $__internal_67ddda6ae1e6714cb1150c35801d952683ed47ce37ff56e1b106a1d07c941556->enter($__internal_67ddda6ae1e6714cb1150c35801d952683ed47ce37ff56e1b106a1d07c941556_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "@Twig/Exception/error.atom.twig", 1)->display($context);
        
        $__internal_67ddda6ae1e6714cb1150c35801d952683ed47ce37ff56e1b106a1d07c941556->leave($__internal_67ddda6ae1e6714cb1150c35801d952683ed47ce37ff56e1b106a1d07c941556_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.atom.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/error.xml.twig' %}*/
/* */
