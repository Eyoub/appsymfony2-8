<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_7a66c856c00d36b295f24e6b4382b2c46f0ea589c38d83d4154133a406d99799 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c44c3459fa0d0598e9ed592baf6619760ebdd51acd7c4b650ae173f1dc450c16 = $this->env->getExtension("native_profiler");
        $__internal_c44c3459fa0d0598e9ed592baf6619760ebdd51acd7c4b650ae173f1dc450c16->enter($__internal_c44c3459fa0d0598e9ed592baf6619760ebdd51acd7c4b650ae173f1dc450c16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_c44c3459fa0d0598e9ed592baf6619760ebdd51acd7c4b650ae173f1dc450c16->leave($__internal_c44c3459fa0d0598e9ed592baf6619760ebdd51acd7c4b650ae173f1dc450c16_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->widget($form) ?>*/
/* */
