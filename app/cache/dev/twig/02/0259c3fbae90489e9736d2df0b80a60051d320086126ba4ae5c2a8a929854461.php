<?php

/* UserBundle::layout.html.twig */
class __TwigTemplate_caeb6d6442fc787da075240e958d405c36368a206143f6d30f9c7f19e743a616 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FilmBundle::index.html.twig", "UserBundle::layout.html.twig", 1);
        $this->blocks = array(
            'A' => array($this, 'block_A'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FilmBundle::index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8a92470183422a4b11afaa9c7cf3820abf1058f74a55d6580f943355379b18e0 = $this->env->getExtension("native_profiler");
        $__internal_8a92470183422a4b11afaa9c7cf3820abf1058f74a55d6580f943355379b18e0->enter($__internal_8a92470183422a4b11afaa9c7cf3820abf1058f74a55d6580f943355379b18e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "UserBundle::layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8a92470183422a4b11afaa9c7cf3820abf1058f74a55d6580f943355379b18e0->leave($__internal_8a92470183422a4b11afaa9c7cf3820abf1058f74a55d6580f943355379b18e0_prof);

    }

    // line 4
    public function block_A($context, array $blocks = array())
    {
        $__internal_ddf517eecef366064bdc6c2e6732ae6a7202a500fd2fd571e05cd74b1ce6377d = $this->env->getExtension("native_profiler");
        $__internal_ddf517eecef366064bdc6c2e6732ae6a7202a500fd2fd571e05cd74b1ce6377d->enter($__internal_ddf517eecef366064bdc6c2e6732ae6a7202a500fd2fd571e05cd74b1ce6377d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "A"));

        // line 5
        echo "
    <div class=\"row\">    
        <div class=\"col-md-3\">
            <h1> Connexion </h1>
        </div>
        <div class=\"col-md-3\">
            ";
        // line 11
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "hasPreviousSession", array())) {
            // line 12
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "all", array(), "method"));
            foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                // line 13
                echo "                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 14
                    echo "                        <div class=\"flash-";
                    echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                    echo "\">
                            ";
                    // line 15
                    echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                    echo "
                        </div>
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 18
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 19
            echo "            ";
        }
        // line 20
        echo "            <div>
                ";
        // line 21
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 23
        echo "            </div>
        </div> 
    </div>



";
        
        $__internal_ddf517eecef366064bdc6c2e6732ae6a7202a500fd2fd571e05cd74b1ce6377d->leave($__internal_ddf517eecef366064bdc6c2e6732ae6a7202a500fd2fd571e05cd74b1ce6377d_prof);

    }

    // line 21
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_1ffd32c1e4e68cf99f9f0c9feab25627cba3967579e929d41bb89d36404d527e = $this->env->getExtension("native_profiler");
        $__internal_1ffd32c1e4e68cf99f9f0c9feab25627cba3967579e929d41bb89d36404d527e->enter($__internal_1ffd32c1e4e68cf99f9f0c9feab25627cba3967579e929d41bb89d36404d527e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 22
        echo "                ";
        
        $__internal_1ffd32c1e4e68cf99f9f0c9feab25627cba3967579e929d41bb89d36404d527e->leave($__internal_1ffd32c1e4e68cf99f9f0c9feab25627cba3967579e929d41bb89d36404d527e_prof);

    }

    public function getTemplateName()
    {
        return "UserBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 22,  102 => 21,  89 => 23,  87 => 21,  84 => 20,  81 => 19,  75 => 18,  66 => 15,  61 => 14,  56 => 13,  51 => 12,  49 => 11,  41 => 5,  35 => 4,  11 => 1,);
    }
}
/* {% extends "FilmBundle::index.html.twig" %}*/
/*  */
/* */
/* {% block A %}*/
/* */
/*     <div class="row">    */
/*         <div class="col-md-3">*/
/*             <h1> Connexion </h1>*/
/*         </div>*/
/*         <div class="col-md-3">*/
/*             {% if app.request.hasPreviousSession %}*/
/*                 {% for type, messages in app.session.flashbag.all() %}*/
/*                     {% for message in messages %}*/
/*                         <div class="flash-{{ type }}">*/
/*                             {{ message }}*/
/*                         </div>*/
/*                     {% endfor %}*/
/*                 {% endfor %}*/
/*             {% endif %}*/
/*             <div>*/
/*                 {% block fos_user_content %}*/
/*                 {% endblock fos_user_content %}*/
/*             </div>*/
/*         </div> */
/*     </div>*/
/* */
/* */
/* */
/* {% endblock %}*/
