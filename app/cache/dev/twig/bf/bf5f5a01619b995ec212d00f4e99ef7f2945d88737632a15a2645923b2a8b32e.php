<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_197088e8c066cb203d7252e72ad7338e752eaf6e5104af411e063e01b6f4a17f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1ee7c3935751ed122a07495d972381591383e854e996d8d8560c202fba5f7e38 = $this->env->getExtension("native_profiler");
        $__internal_1ee7c3935751ed122a07495d972381591383e854e996d8d8560c202fba5f7e38->enter($__internal_1ee7c3935751ed122a07495d972381591383e854e996d8d8560c202fba5f7e38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_1ee7c3935751ed122a07495d972381591383e854e996d8d8560c202fba5f7e38->leave($__internal_1ee7c3935751ed122a07495d972381591383e854e996d8d8560c202fba5f7e38_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'search')) ?>*/
/* */
