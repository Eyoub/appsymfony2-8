<?php

/* ::base.html.twig */
class __TwigTemplate_94776b716fe8e858b365c7b824f607e0540d82e12e21d7b2522d4dfeb054cbeb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2ca74b9f5dcb765823af41a17dfc29d1a8305e6cea69a370684d69b73c30cc7a = $this->env->getExtension("native_profiler");
        $__internal_2ca74b9f5dcb765823af41a17dfc29d1a8305e6cea69a370684d69b73c30cc7a->enter($__internal_2ca74b9f5dcb765823af41a17dfc29d1a8305e6cea69a370684d69b73c30cc7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_2ca74b9f5dcb765823af41a17dfc29d1a8305e6cea69a370684d69b73c30cc7a->leave($__internal_2ca74b9f5dcb765823af41a17dfc29d1a8305e6cea69a370684d69b73c30cc7a_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_62a2b5c05274d78c6b3cf7b19b946054c96b4355be87cc3d904a5e6b0942bc2b = $this->env->getExtension("native_profiler");
        $__internal_62a2b5c05274d78c6b3cf7b19b946054c96b4355be87cc3d904a5e6b0942bc2b->enter($__internal_62a2b5c05274d78c6b3cf7b19b946054c96b4355be87cc3d904a5e6b0942bc2b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_62a2b5c05274d78c6b3cf7b19b946054c96b4355be87cc3d904a5e6b0942bc2b->leave($__internal_62a2b5c05274d78c6b3cf7b19b946054c96b4355be87cc3d904a5e6b0942bc2b_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_64823b96f11c98a953f6f3e27cf5aa37baebc88a50373c5e46117ab5e499bb7f = $this->env->getExtension("native_profiler");
        $__internal_64823b96f11c98a953f6f3e27cf5aa37baebc88a50373c5e46117ab5e499bb7f->enter($__internal_64823b96f11c98a953f6f3e27cf5aa37baebc88a50373c5e46117ab5e499bb7f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_64823b96f11c98a953f6f3e27cf5aa37baebc88a50373c5e46117ab5e499bb7f->leave($__internal_64823b96f11c98a953f6f3e27cf5aa37baebc88a50373c5e46117ab5e499bb7f_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_3b0a7a3f8a5231305fcc4d9a23c0fc536a7179eb9fd8fd662c19499ddfbcc4e5 = $this->env->getExtension("native_profiler");
        $__internal_3b0a7a3f8a5231305fcc4d9a23c0fc536a7179eb9fd8fd662c19499ddfbcc4e5->enter($__internal_3b0a7a3f8a5231305fcc4d9a23c0fc536a7179eb9fd8fd662c19499ddfbcc4e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_3b0a7a3f8a5231305fcc4d9a23c0fc536a7179eb9fd8fd662c19499ddfbcc4e5->leave($__internal_3b0a7a3f8a5231305fcc4d9a23c0fc536a7179eb9fd8fd662c19499ddfbcc4e5_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_7143339ead173aecaecf06cbb1e662a4290b6041234678d44d2155f9dd199710 = $this->env->getExtension("native_profiler");
        $__internal_7143339ead173aecaecf06cbb1e662a4290b6041234678d44d2155f9dd199710->enter($__internal_7143339ead173aecaecf06cbb1e662a4290b6041234678d44d2155f9dd199710_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_7143339ead173aecaecf06cbb1e662a4290b6041234678d44d2155f9dd199710->leave($__internal_7143339ead173aecaecf06cbb1e662a4290b6041234678d44d2155f9dd199710_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
