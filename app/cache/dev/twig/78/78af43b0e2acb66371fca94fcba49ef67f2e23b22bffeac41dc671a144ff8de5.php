<?php

/* @FOSUser/Registration/email.txt.twig */
class __TwigTemplate_9c86210fb23e2a8e9614415e489560b0809de06254a2539f04c4ccaefdf36479 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d2f302794178882eaf4c93c163207ca2787ec023cc0beecc822622d938f373dd = $this->env->getExtension("native_profiler");
        $__internal_d2f302794178882eaf4c93c163207ca2787ec023cc0beecc822622d938f373dd->enter($__internal_d2f302794178882eaf4c93c163207ca2787ec023cc0beecc822622d938f373dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        $this->displayBlock('body_text', $context, $blocks);
        // line 12
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_d2f302794178882eaf4c93c163207ca2787ec023cc0beecc822622d938f373dd->leave($__internal_d2f302794178882eaf4c93c163207ca2787ec023cc0beecc822622d938f373dd_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_89fa8aa878cbe1233360085bbf81c08dbbdf67aa1d0be13de935c543959e90c2 = $this->env->getExtension("native_profiler");
        $__internal_89fa8aa878cbe1233360085bbf81c08dbbdf67aa1d0be13de935c543959e90c2->enter($__internal_89fa8aa878cbe1233360085bbf81c08dbbdf67aa1d0be13de935c543959e90c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('translator')->trans("registration.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_89fa8aa878cbe1233360085bbf81c08dbbdf67aa1d0be13de935c543959e90c2->leave($__internal_89fa8aa878cbe1233360085bbf81c08dbbdf67aa1d0be13de935c543959e90c2_prof);

    }

    // line 7
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_f1855d14b56359eeeb0f13ec0e27dbf8bb16bfc3cb67241c50e1963189c4d52c = $this->env->getExtension("native_profiler");
        $__internal_f1855d14b56359eeeb0f13ec0e27dbf8bb16bfc3cb67241c50e1963189c4d52c->enter($__internal_f1855d14b56359eeeb0f13ec0e27dbf8bb16bfc3cb67241c50e1963189c4d52c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 9
        echo $this->env->getExtension('translator')->trans("registration.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_f1855d14b56359eeeb0f13ec0e27dbf8bb16bfc3cb67241c50e1963189c4d52c->leave($__internal_f1855d14b56359eeeb0f13ec0e27dbf8bb16bfc3cb67241c50e1963189c4d52c_prof);

    }

    // line 12
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_ae9e1c21a58d24bde607279b48f810d09ce65f492a354de937d889430cca179d = $this->env->getExtension("native_profiler");
        $__internal_ae9e1c21a58d24bde607279b48f810d09ce65f492a354de937d889430cca179d->enter($__internal_ae9e1c21a58d24bde607279b48f810d09ce65f492a354de937d889430cca179d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_ae9e1c21a58d24bde607279b48f810d09ce65f492a354de937d889430cca179d->leave($__internal_ae9e1c21a58d24bde607279b48f810d09ce65f492a354de937d889430cca179d_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  66 => 12,  57 => 9,  51 => 7,  42 => 4,  36 => 2,  29 => 12,  27 => 7,  25 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* {% block subject %}*/
/* {% autoescape false %}*/
/* {{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_text %}*/
/* {% autoescape false %}*/
/* {{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_html %}{% endblock %}*/
/* */
