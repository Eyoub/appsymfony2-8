<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_d6a5b258ad37671cae1948b01f1b71843b3e51df3c5b3edbf343aafc3ddf22b2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8c0a1e171bc7f235fe1f2dfcf380e053cdcc8944e2904f14c8963e48f5fa8b27 = $this->env->getExtension("native_profiler");
        $__internal_8c0a1e171bc7f235fe1f2dfcf380e053cdcc8944e2904f14c8963e48f5fa8b27->enter($__internal_8c0a1e171bc7f235fe1f2dfcf380e053cdcc8944e2904f14c8963e48f5fa8b27_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_8c0a1e171bc7f235fe1f2dfcf380e053cdcc8944e2904f14c8963e48f5fa8b27->leave($__internal_8c0a1e171bc7f235fe1f2dfcf380e053cdcc8944e2904f14c8963e48f5fa8b27_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr style="display: none">*/
/*     <td colspan="2">*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
