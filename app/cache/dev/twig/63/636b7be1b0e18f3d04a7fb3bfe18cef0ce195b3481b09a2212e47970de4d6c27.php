<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_493fc95c7fcd0cd2e589d131baa9388d2d1f68cb486573569971f5d9d6f1ddb4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Registration:register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8b519895c52d54d39412f39db7c17dc0e2fd0574797af8e649204ceea3426f06 = $this->env->getExtension("native_profiler");
        $__internal_8b519895c52d54d39412f39db7c17dc0e2fd0574797af8e649204ceea3426f06->enter($__internal_8b519895c52d54d39412f39db7c17dc0e2fd0574797af8e649204ceea3426f06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8b519895c52d54d39412f39db7c17dc0e2fd0574797af8e649204ceea3426f06->leave($__internal_8b519895c52d54d39412f39db7c17dc0e2fd0574797af8e649204ceea3426f06_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_15474f94403eff70d3a3ab84d2c52b1eea55bcb60d242ca6ed71abcd96b79284 = $this->env->getExtension("native_profiler");
        $__internal_15474f94403eff70d3a3ab84d2c52b1eea55bcb60d242ca6ed71abcd96b79284->enter($__internal_15474f94403eff70d3a3ab84d2c52b1eea55bcb60d242ca6ed71abcd96b79284_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Registration:register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 4)->display($context);
        
        $__internal_15474f94403eff70d3a3ab84d2c52b1eea55bcb60d242ca6ed71abcd96b79284->leave($__internal_15474f94403eff70d3a3ab84d2c52b1eea55bcb60d242ca6ed71abcd96b79284_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Registration:register_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
