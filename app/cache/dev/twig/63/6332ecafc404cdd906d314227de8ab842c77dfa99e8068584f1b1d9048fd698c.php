<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_6f787819e49da26811f46dc2112d652e97a5efff41508ec5df80e15232769d21 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c8d2f0ab4f54728737a5a413811af5da240d24bb55a829915d94a8b2683b241b = $this->env->getExtension("native_profiler");
        $__internal_c8d2f0ab4f54728737a5a413811af5da240d24bb55a829915d94a8b2683b241b->enter($__internal_c8d2f0ab4f54728737a5a413811af5da240d24bb55a829915d94a8b2683b241b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_c8d2f0ab4f54728737a5a413811af5da240d24bb55a829915d94a8b2683b241b->leave($__internal_c8d2f0ab4f54728737a5a413811af5da240d24bb55a829915d94a8b2683b241b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($widget == 'single_text'): ?>*/
/*     <?php echo $view['form']->block($form, 'form_widget_simple'); ?>*/
/* <?php else: ?>*/
/*     <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*         <?php echo $view['form']->widget($form['date']).' '.$view['form']->widget($form['time']) ?>*/
/*     </div>*/
/* <?php endif ?>*/
/* */
