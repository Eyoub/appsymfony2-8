<?php

/* @User/Security/login.html.twig */
class __TwigTemplate_3719e38abc8ababed4c47d3373505c7df068ad66dad15b1c28e05a3cccee6f8b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("UserBundle::layout.html.twig", "@User/Security/login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "UserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ed246c5ace01a9ec38952b5ecd41e9283edd5f88962428e17a75c9fe7c63e450 = $this->env->getExtension("native_profiler");
        $__internal_ed246c5ace01a9ec38952b5ecd41e9283edd5f88962428e17a75c9fe7c63e450->enter($__internal_ed246c5ace01a9ec38952b5ecd41e9283edd5f88962428e17a75c9fe7c63e450_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@User/Security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ed246c5ace01a9ec38952b5ecd41e9283edd5f88962428e17a75c9fe7c63e450->leave($__internal_ed246c5ace01a9ec38952b5ecd41e9283edd5f88962428e17a75c9fe7c63e450_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_7507024264d9aee181643d5122a6a650b025eb3158e9351d8dca8d5bf675cbd7 = $this->env->getExtension("native_profiler");
        $__internal_7507024264d9aee181643d5122a6a650b025eb3158e9351d8dca8d5bf675cbd7->enter($__internal_7507024264d9aee181643d5122a6a650b025eb3158e9351d8dca8d5bf675cbd7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "FOSUserBundle:Security:login_content.html.twig");
        echo "
";
        
        $__internal_7507024264d9aee181643d5122a6a650b025eb3158e9351d8dca8d5bf675cbd7->leave($__internal_7507024264d9aee181643d5122a6a650b025eb3158e9351d8dca8d5bf675cbd7_prof);

    }

    public function getTemplateName()
    {
        return "@User/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "UserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/*     {{ include('FOSUserBundle:Security:login_content.html.twig') }}*/
/* {% endblock fos_user_content %}*/
/* */
