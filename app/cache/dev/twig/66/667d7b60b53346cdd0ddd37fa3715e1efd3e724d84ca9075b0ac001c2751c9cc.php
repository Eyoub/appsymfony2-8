<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_8a1216f3a9718ca278ae359569e74e3ed1be5db4967760fad5c70a3b1c746fd7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c9892f182605c8668c2c813ba427393d715ec8f61efd8f4a35ff2c019c76ec1c = $this->env->getExtension("native_profiler");
        $__internal_c9892f182605c8668c2c813ba427393d715ec8f61efd8f4a35ff2c019c76ec1c->enter($__internal_c9892f182605c8668c2c813ba427393d715ec8f61efd8f4a35ff2c019c76ec1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "exception" => $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_c9892f182605c8668c2c813ba427393d715ec8f61efd8f4a35ff2c019c76ec1c->leave($__internal_c9892f182605c8668c2c813ba427393d715ec8f61efd8f4a35ff2c019c76ec1c_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}*/
/* */
