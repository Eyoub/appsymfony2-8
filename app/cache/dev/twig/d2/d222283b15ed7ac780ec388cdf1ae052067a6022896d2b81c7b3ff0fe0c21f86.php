<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_c6a33ca9bda4c596c3ad78ecc5390c6e8c3f3775d6347db76290cd6ead30b5e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d8822fff171988cfe3cb0a735dc662994dfe41de9780423eee6b4bf42ed8677f = $this->env->getExtension("native_profiler");
        $__internal_d8822fff171988cfe3cb0a735dc662994dfe41de9780423eee6b4bf42ed8677f->enter($__internal_d8822fff171988cfe3cb0a735dc662994dfe41de9780423eee6b4bf42ed8677f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_d8822fff171988cfe3cb0a735dc662994dfe41de9780423eee6b4bf42ed8677f->leave($__internal_d8822fff171988cfe3cb0a735dc662994dfe41de9780423eee6b4bf42ed8677f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <textarea <?php echo $view['form']->block($form, 'widget_attributes') ?>><?php echo $view->escape($value) ?></textarea>*/
/* */
