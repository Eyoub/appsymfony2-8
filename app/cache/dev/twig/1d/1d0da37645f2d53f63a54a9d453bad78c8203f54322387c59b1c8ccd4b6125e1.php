<?php

/* FilmBundle:Acteur:ajoutActeur.html.twig */
class __TwigTemplate_e151e646b5a846c7759fca041431d605fa6e89e2d9568dc6916ed4f371fcec10 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FilmBundle::index.html.twig", "FilmBundle:Acteur:ajoutActeur.html.twig", 1);
        $this->blocks = array(
            'A' => array($this, 'block_A'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FilmBundle::index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_eb6ef3faa8a06f3aebbc57adfd712d1f3331e9820906fdb2fea74d2f484990e5 = $this->env->getExtension("native_profiler");
        $__internal_eb6ef3faa8a06f3aebbc57adfd712d1f3331e9820906fdb2fea74d2f484990e5->enter($__internal_eb6ef3faa8a06f3aebbc57adfd712d1f3331e9820906fdb2fea74d2f484990e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FilmBundle:Acteur:ajoutActeur.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_eb6ef3faa8a06f3aebbc57adfd712d1f3331e9820906fdb2fea74d2f484990e5->leave($__internal_eb6ef3faa8a06f3aebbc57adfd712d1f3331e9820906fdb2fea74d2f484990e5_prof);

    }

    // line 2
    public function block_A($context, array $blocks = array())
    {
        $__internal_cf623549f1da795b30954bc08c9c4a6ab20a533629a13519cca3bfcaa60d02f9 = $this->env->getExtension("native_profiler");
        $__internal_cf623549f1da795b30954bc08c9c4a6ab20a533629a13519cca3bfcaa60d02f9->enter($__internal_cf623549f1da795b30954bc08c9c4a6ab20a533629a13519cca3bfcaa60d02f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "A"));

        // line 3
        echo "    <div class=\"row\">

        <div class=\"col-md-3\">
            <p class=\"lead\">Gestion des acteurs</p>
            <div class=\"list-group\">
                <a href=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("affich_act");
        echo "\" class=\"list-group-item\">Afficher</a>
                <a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("ajout_act");
        echo "\" class=\"list-group-item\">Ajout</a>
                <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("modif_act2");
        echo "\" class=\"list-group-item\">Editer</a>
            </div>
        </div>
        <div class=\"col-md-9\">
            <form method=\"POST\">

                ";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
                <input type=\"submit\" value=\"envoyer\"/>    
            </form>
        </div>
    </div>
    ";
        
        $__internal_cf623549f1da795b30954bc08c9c4a6ab20a533629a13519cca3bfcaa60d02f9->leave($__internal_cf623549f1da795b30954bc08c9c4a6ab20a533629a13519cca3bfcaa60d02f9_prof);

    }

    public function getTemplateName()
    {
        return "FilmBundle:Acteur:ajoutActeur.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 16,  55 => 10,  51 => 9,  47 => 8,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends "FilmBundle::index.html.twig" %}*/
/* {% block A %}*/
/*     <div class="row">*/
/* */
/*         <div class="col-md-3">*/
/*             <p class="lead">Gestion des acteurs</p>*/
/*             <div class="list-group">*/
/*                 <a href="{{path('affich_act')}}" class="list-group-item">Afficher</a>*/
/*                 <a href="{{path('ajout_act')}}" class="list-group-item">Ajout</a>*/
/*                 <a href="{{path('modif_act2')}}" class="list-group-item">Editer</a>*/
/*             </div>*/
/*         </div>*/
/*         <div class="col-md-9">*/
/*             <form method="POST">*/
/* */
/*                 {{form_widget(form)}}*/
/*                 <input type="submit" value="envoyer"/>    */
/*             </form>*/
/*         </div>*/
/*     </div>*/
/*     {% endblock %}*/
