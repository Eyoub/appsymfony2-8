<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_ea3a1f47ff6067b112823b9117033e4c01b572ce172ea1d1450cd388a5d3cf26 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6be915b9c36deb4c9f5b3d62c3c4c58e07b5a323778e8ea0f3f520780155c9ea = $this->env->getExtension("native_profiler");
        $__internal_6be915b9c36deb4c9f5b3d62c3c4c58e07b5a323778e8ea0f3f520780155c9ea->enter($__internal_6be915b9c36deb4c9f5b3d62c3c4c58e07b5a323778e8ea0f3f520780155c9ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_6be915b9c36deb4c9f5b3d62c3c4c58e07b5a323778e8ea0f3f520780155c9ea->leave($__internal_6be915b9c36deb4c9f5b3d62c3c4c58e07b5a323778e8ea0f3f520780155c9ea_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="radio"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     value="<?php echo $view->escape($value) ?>"*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
