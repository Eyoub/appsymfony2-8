<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_28dbd19a6d0d0ebe34ef67a55e1700dc76aac8eb8ad7efc4c04c0d3fe49d229c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_02fd1a9829ce84e8d8d5fcf30d714ab16f30abb6f676eed1e20f715468a719b9 = $this->env->getExtension("native_profiler");
        $__internal_02fd1a9829ce84e8d8d5fcf30d714ab16f30abb6f676eed1e20f715468a719b9->enter($__internal_02fd1a9829ce84e8d8d5fcf30d714ab16f30abb6f676eed1e20f715468a719b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_02fd1a9829ce84e8d8d5fcf30d714ab16f30abb6f676eed1e20f715468a719b9->leave($__internal_02fd1a9829ce84e8d8d5fcf30d714ab16f30abb6f676eed1e20f715468a719b9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="checkbox"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     <?php if (strlen($value) > 0): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?>*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
