<?php

/* FilmBundle:Acteur:modifActeur.html.twig */
class __TwigTemplate_be4dfa1dea8caaf6c22811547c2059f9ae29c63647647641d679d5a2a2638ea7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("FilmBundle::index.html.twig", "FilmBundle:Acteur:modifActeur.html.twig", 2);
        $this->blocks = array(
            'A' => array($this, 'block_A'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FilmBundle::index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_85077a81ceccb57ae5cd7bcff90563a274f57aa9d57cf2b13f2418eaae29aba5 = $this->env->getExtension("native_profiler");
        $__internal_85077a81ceccb57ae5cd7bcff90563a274f57aa9d57cf2b13f2418eaae29aba5->enter($__internal_85077a81ceccb57ae5cd7bcff90563a274f57aa9d57cf2b13f2418eaae29aba5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FilmBundle:Acteur:modifActeur.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_85077a81ceccb57ae5cd7bcff90563a274f57aa9d57cf2b13f2418eaae29aba5->leave($__internal_85077a81ceccb57ae5cd7bcff90563a274f57aa9d57cf2b13f2418eaae29aba5_prof);

    }

    // line 3
    public function block_A($context, array $blocks = array())
    {
        $__internal_c5464c275e70728f974fb146f3c8eff25dfc1128179fd61edc2e4feaee85bca3 = $this->env->getExtension("native_profiler");
        $__internal_c5464c275e70728f974fb146f3c8eff25dfc1128179fd61edc2e4feaee85bca3->enter($__internal_c5464c275e70728f974fb146f3c8eff25dfc1128179fd61edc2e4feaee85bca3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "A"));

        // line 4
        echo "    
    <div class=\"row\">

        <div class=\"col-md-3\">
            <p class=\"lead\">Gestion des acteurs</p>
            <div class=\"list-group\">
                <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("affich_act");
        echo "\" class=\"list-group-item\">Afficher</a>
                <a href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("ajout_act");
        echo "\" class=\"list-group-item\">Ajout</a>
                <a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("modif_act2");
        echo "\" class=\"list-group-item\">Editer</a>
            </div>
        </div>
            <div class=\"col-md-9\">
<form class=\"well\" method=\"POST\">
    
    ";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
    <input type=\"submit\" value=\"envoyer\"/>    
</form>
            </div>
";
        
        $__internal_c5464c275e70728f974fb146f3c8eff25dfc1128179fd61edc2e4feaee85bca3->leave($__internal_c5464c275e70728f974fb146f3c8eff25dfc1128179fd61edc2e4feaee85bca3_prof);

    }

    public function getTemplateName()
    {
        return "FilmBundle:Acteur:modifActeur.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 18,  56 => 12,  52 => 11,  48 => 10,  40 => 4,  34 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "FilmBundle::index.html.twig" %}*/
/* {% block A %}*/
/*     */
/*     <div class="row">*/
/* */
/*         <div class="col-md-3">*/
/*             <p class="lead">Gestion des acteurs</p>*/
/*             <div class="list-group">*/
/*                 <a href="{{path('affich_act')}}" class="list-group-item">Afficher</a>*/
/*                 <a href="{{path('ajout_act')}}" class="list-group-item">Ajout</a>*/
/*                 <a href="{{path('modif_act2')}}" class="list-group-item">Editer</a>*/
/*             </div>*/
/*         </div>*/
/*             <div class="col-md-9">*/
/* <form class="well" method="POST">*/
/*     */
/*     {{form_widget(form)}}*/
/*     <input type="submit" value="envoyer"/>    */
/* </form>*/
/*             </div>*/
/* {% endblock %}*/
