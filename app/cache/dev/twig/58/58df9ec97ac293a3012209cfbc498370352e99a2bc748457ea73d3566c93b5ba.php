<?php

/* FOSUserBundle:Group:new.html.twig */
class __TwigTemplate_9906bb8b782c1eac744cad1e9b6f1f42f116c2e4e109e829d3b70bf4e002ea5a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Group:new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cfcbd87f05f7efe3a4d73374eaf4dea8e05e4df7ad04e08f11fea5f7be10773d = $this->env->getExtension("native_profiler");
        $__internal_cfcbd87f05f7efe3a4d73374eaf4dea8e05e4df7ad04e08f11fea5f7be10773d->enter($__internal_cfcbd87f05f7efe3a4d73374eaf4dea8e05e4df7ad04e08f11fea5f7be10773d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cfcbd87f05f7efe3a4d73374eaf4dea8e05e4df7ad04e08f11fea5f7be10773d->leave($__internal_cfcbd87f05f7efe3a4d73374eaf4dea8e05e4df7ad04e08f11fea5f7be10773d_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_37c6b1fe305c7deeff7c0aa68e811c435b6e1b4fc1788f6a7593c2c164ecb5ae = $this->env->getExtension("native_profiler");
        $__internal_37c6b1fe305c7deeff7c0aa68e811c435b6e1b4fc1788f6a7593c2c164ecb5ae->enter($__internal_37c6b1fe305c7deeff7c0aa68e811c435b6e1b4fc1788f6a7593c2c164ecb5ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:new_content.html.twig", "FOSUserBundle:Group:new.html.twig", 4)->display($context);
        
        $__internal_37c6b1fe305c7deeff7c0aa68e811c435b6e1b4fc1788f6a7593c2c164ecb5ae->leave($__internal_37c6b1fe305c7deeff7c0aa68e811c435b6e1b4fc1788f6a7593c2c164ecb5ae_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:new_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
