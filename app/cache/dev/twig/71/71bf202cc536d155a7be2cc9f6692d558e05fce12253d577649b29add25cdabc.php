<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_5b3d1492a9c8421bbefb59037ff4e98aacdc5cd151966b5f8f3a1b3acbeb646e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f3e2ebffb9b70bf888b4ccee3b4e539cf5eb4bd60a6123619bfc4ab493150065 = $this->env->getExtension("native_profiler");
        $__internal_f3e2ebffb9b70bf888b4ccee3b4e539cf5eb4bd60a6123619bfc4ab493150065->enter($__internal_f3e2ebffb9b70bf888b4ccee3b4e539cf5eb4bd60a6123619bfc4ab493150065_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f3e2ebffb9b70bf888b4ccee3b4e539cf5eb4bd60a6123619bfc4ab493150065->leave($__internal_f3e2ebffb9b70bf888b4ccee3b4e539cf5eb4bd60a6123619bfc4ab493150065_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_0e7a4c19935cd7cca42747dd12ebf958fadf1a82a7b30856b980c8396e55f77d = $this->env->getExtension("native_profiler");
        $__internal_0e7a4c19935cd7cca42747dd12ebf958fadf1a82a7b30856b980c8396e55f77d->enter($__internal_0e7a4c19935cd7cca42747dd12ebf958fadf1a82a7b30856b980c8396e55f77d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_0e7a4c19935cd7cca42747dd12ebf958fadf1a82a7b30856b980c8396e55f77d->leave($__internal_0e7a4c19935cd7cca42747dd12ebf958fadf1a82a7b30856b980c8396e55f77d_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_987b9554e2f9b56c3e31ae40a9f6a0817efd5b4eefc2782e717bdc67735f608d = $this->env->getExtension("native_profiler");
        $__internal_987b9554e2f9b56c3e31ae40a9f6a0817efd5b4eefc2782e717bdc67735f608d->enter($__internal_987b9554e2f9b56c3e31ae40a9f6a0817efd5b4eefc2782e717bdc67735f608d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_987b9554e2f9b56c3e31ae40a9f6a0817efd5b4eefc2782e717bdc67735f608d->leave($__internal_987b9554e2f9b56c3e31ae40a9f6a0817efd5b4eefc2782e717bdc67735f608d_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block title 'Redirection Intercepted' %}*/
/* */
/* {% block body %}*/
/*     <div class="sf-reset">*/
/*         <div class="block-exception">*/
/*             <h1>This request redirects to <a href="{{ location }}">{{ location }}</a>.</h1>*/
/* */
/*             <p>*/
/*                 <small>*/
/*                     The redirect was intercepted by the web debug toolbar to help debugging.*/
/*                     For more information, see the "intercept-redirects" option of the Profiler.*/
/*                 </small>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
