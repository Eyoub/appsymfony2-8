<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_22cc0ceb333e7f43682ea0f9b16a88769916786dab3bb556f3fc49cdebdacd7e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1a4e8d758f514bfe9f320e9270ebcaeb6815968b8ff13e6d00ba0e0006da57a1 = $this->env->getExtension("native_profiler");
        $__internal_1a4e8d758f514bfe9f320e9270ebcaeb6815968b8ff13e6d00ba0e0006da57a1->enter($__internal_1a4e8d758f514bfe9f320e9270ebcaeb6815968b8ff13e6d00ba0e0006da57a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_1a4e8d758f514bfe9f320e9270ebcaeb6815968b8ff13e6d00ba0e0006da57a1->leave($__internal_1a4e8d758f514bfe9f320e9270ebcaeb6815968b8ff13e6d00ba0e0006da57a1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?> %*/
/* */
