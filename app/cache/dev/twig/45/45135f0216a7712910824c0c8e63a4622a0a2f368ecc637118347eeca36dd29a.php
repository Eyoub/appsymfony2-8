<?php

/* FOSUserBundle:Resetting:password_already_requested.html.twig */
class __TwigTemplate_a9e0fc467530162793148be2e6ae3873a91aa33af35f519c965489a996e75a01 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Resetting:password_already_requested.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6bc1e0cedef429684c8d481e1a5aa13bb6a78c93f9b01a04aa19870dce8f6226 = $this->env->getExtension("native_profiler");
        $__internal_6bc1e0cedef429684c8d481e1a5aa13bb6a78c93f9b01a04aa19870dce8f6226->enter($__internal_6bc1e0cedef429684c8d481e1a5aa13bb6a78c93f9b01a04aa19870dce8f6226_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:password_already_requested.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6bc1e0cedef429684c8d481e1a5aa13bb6a78c93f9b01a04aa19870dce8f6226->leave($__internal_6bc1e0cedef429684c8d481e1a5aa13bb6a78c93f9b01a04aa19870dce8f6226_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_dd8c14343ff7a8d471b47f2a864db6698714b281a4286b05c4a8e0db2d2f4a3d = $this->env->getExtension("native_profiler");
        $__internal_dd8c14343ff7a8d471b47f2a864db6698714b281a4286b05c4a8e0db2d2f4a3d->enter($__internal_dd8c14343ff7a8d471b47f2a864db6698714b281a4286b05c4a8e0db2d2f4a3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.password_already_requested", array(), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_dd8c14343ff7a8d471b47f2a864db6698714b281a4286b05c4a8e0db2d2f4a3d->leave($__internal_dd8c14343ff7a8d471b47f2a864db6698714b281a4286b05c4a8e0db2d2f4a3d_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:password_already_requested.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/* <p>{{ 'resetting.password_already_requested'|trans }}</p>*/
/* {% endblock fos_user_content %}*/
/* */
