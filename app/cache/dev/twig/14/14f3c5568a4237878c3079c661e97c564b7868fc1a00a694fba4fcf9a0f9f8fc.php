<?php

/* @Twig/Exception/error.txt.twig */
class __TwigTemplate_8ccb8bb205e6d78fdf0707f7ea787204f45ad4b74949b65c38cb23a00ad41b75 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ac7550cf122145f4fe7d0c735e4366a65767ff70b34ede57d9f1bab70aa1e4ad = $this->env->getExtension("native_profiler");
        $__internal_ac7550cf122145f4fe7d0c735e4366a65767ff70b34ede57d9f1bab70aa1e4ad->enter($__internal_ac7550cf122145f4fe7d0c735e4366a65767ff70b34ede57d9f1bab70aa1e4ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code"));
        echo " ";
        echo (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_ac7550cf122145f4fe7d0c735e4366a65767ff70b34ede57d9f1bab70aa1e4ad->leave($__internal_ac7550cf122145f4fe7d0c735e4366a65767ff70b34ede57d9f1bab70aa1e4ad_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 4,  22 => 1,);
    }
}
/* Oops! An Error Occurred*/
/* =======================*/
/* */
/* The server returned a "{{ status_code }} {{ status_text }}".*/
/* */
/* Something is broken. Please let us know what you were doing when this error occurred.*/
/* We will fix it as soon as possible. Sorry for any inconvenience caused.*/
/* */
