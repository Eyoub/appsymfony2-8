<?php

/* :categorie:show.html.twig */
class __TwigTemplate_b58006a10ef9339270729089f13d6120fb5c762b9d7d4f1aab7bcf6e10e5e857 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":categorie:show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_64e372bc1e1cd06bd6cb0d3876cd6832afb2f3d4982c20e8e1ba45f3d6cff5e7 = $this->env->getExtension("native_profiler");
        $__internal_64e372bc1e1cd06bd6cb0d3876cd6832afb2f3d4982c20e8e1ba45f3d6cff5e7->enter($__internal_64e372bc1e1cd06bd6cb0d3876cd6832afb2f3d4982c20e8e1ba45f3d6cff5e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":categorie:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_64e372bc1e1cd06bd6cb0d3876cd6832afb2f3d4982c20e8e1ba45f3d6cff5e7->leave($__internal_64e372bc1e1cd06bd6cb0d3876cd6832afb2f3d4982c20e8e1ba45f3d6cff5e7_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_674ed3e48b6018c7509e5cc3576fe8e78850bb0e295a0da48ea2e4d4c2a37a49 = $this->env->getExtension("native_profiler");
        $__internal_674ed3e48b6018c7509e5cc3576fe8e78850bb0e295a0da48ea2e4d4c2a37a49->enter($__internal_674ed3e48b6018c7509e5cc3576fe8e78850bb0e295a0da48ea2e4d4c2a37a49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Categorie</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["categorie"]) ? $context["categorie"] : $this->getContext($context, "categorie")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Nom</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["categorie"]) ? $context["categorie"] : $this->getContext($context, "categorie")), "nom", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("categorie_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("categorie_edit", array("id" => $this->getAttribute((isset($context["categorie"]) ? $context["categorie"] : $this->getContext($context, "categorie")), "id", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 27
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 29
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_674ed3e48b6018c7509e5cc3576fe8e78850bb0e295a0da48ea2e4d4c2a37a49->leave($__internal_674ed3e48b6018c7509e5cc3576fe8e78850bb0e295a0da48ea2e4d4c2a37a49_prof);

    }

    public function getTemplateName()
    {
        return ":categorie:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 29,  77 => 27,  71 => 24,  65 => 21,  55 => 14,  48 => 10,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/*     <h1>Categorie</h1>*/
/* */
/*     <table>*/
/*         <tbody>*/
/*             <tr>*/
/*                 <th>Id</th>*/
/*                 <td>{{ categorie.id }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Nom</th>*/
/*                 <td>{{ categorie.nom }}</td>*/
/*             </tr>*/
/*         </tbody>*/
/*     </table>*/
/* */
/*     <ul>*/
/*         <li>*/
/*             <a href="{{ path('categorie_index') }}">Back to the list</a>*/
/*         </li>*/
/*         <li>*/
/*             <a href="{{ path('categorie_edit', { 'id': categorie.id }) }}">Edit</a>*/
/*         </li>*/
/*         <li>*/
/*             {{ form_start(delete_form) }}*/
/*                 <input type="submit" value="Delete">*/
/*             {{ form_end(delete_form) }}*/
/*         </li>*/
/*     </ul>*/
/* {% endblock %}*/
/* */
