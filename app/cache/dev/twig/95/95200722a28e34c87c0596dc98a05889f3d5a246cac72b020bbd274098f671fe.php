<?php

/* @Twig/Exception/exception.js.twig */
class __TwigTemplate_6ba3ac3d97e2e859083064037d9470538db484d089ca46cc213c2be198218db9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b40e16479ea6d78062f398514ccb5af5c02e987e10283f85498828748db3ea5e = $this->env->getExtension("native_profiler");
        $__internal_b40e16479ea6d78062f398514ccb5af5c02e987e10283f85498828748db3ea5e->enter($__internal_b40e16479ea6d78062f398514ccb5af5c02e987e10283f85498828748db3ea5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "@Twig/Exception/exception.js.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_b40e16479ea6d78062f398514ccb5af5c02e987e10283f85498828748db3ea5e->leave($__internal_b40e16479ea6d78062f398514ccb5af5c02e987e10283f85498828748db3ea5e_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
