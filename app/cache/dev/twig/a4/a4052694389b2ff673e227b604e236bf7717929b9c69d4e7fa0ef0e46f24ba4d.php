<?php

/* :categorie:new.html.twig */
class __TwigTemplate_9b07ea0ec59d6f73cab88c3953c7a16bca2c5357565659b0b37c8c74b38d7576 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FilmBundle::index.html.twig", ":categorie:new.html.twig", 1);
        $this->blocks = array(
            'A' => array($this, 'block_A'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FilmBundle::index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a5d0c4bab39f269bd4eb6d85b7375ae18d70b6d5b7f2b7d93f7e652fe6b20366 = $this->env->getExtension("native_profiler");
        $__internal_a5d0c4bab39f269bd4eb6d85b7375ae18d70b6d5b7f2b7d93f7e652fe6b20366->enter($__internal_a5d0c4bab39f269bd4eb6d85b7375ae18d70b6d5b7f2b7d93f7e652fe6b20366_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":categorie:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a5d0c4bab39f269bd4eb6d85b7375ae18d70b6d5b7f2b7d93f7e652fe6b20366->leave($__internal_a5d0c4bab39f269bd4eb6d85b7375ae18d70b6d5b7f2b7d93f7e652fe6b20366_prof);

    }

    // line 2
    public function block_A($context, array $blocks = array())
    {
        $__internal_84dc185014c17bcfab83d0c0b3d7b471b01b0d31e0ca940090631360b060e8b7 = $this->env->getExtension("native_profiler");
        $__internal_84dc185014c17bcfab83d0c0b3d7b471b01b0d31e0ca940090631360b060e8b7->enter($__internal_84dc185014c17bcfab83d0c0b3d7b471b01b0d31e0ca940090631360b060e8b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "A"));

        // line 3
        echo "    <div class=\"row\">

        <div class=\"col-md-3\">
            <p class=\"lead\">Gestion des categories</p>
            <div class=\"list-group\">
                <a href=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("category_index");
        echo "\" class=\"list-group-item\">lister</a>
                <a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("category_new");
        echo "\" class=\"list-group-item\">creer</a>
            </div>
        </div>
        <div class=\"col-md-9\">
            <form method=\"POST\">

                ";
        // line 15
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
                <input type=\"submit\" value=\"envoyer\"/>    
            </form>
        </div>
    </div>
";
        
        $__internal_84dc185014c17bcfab83d0c0b3d7b471b01b0d31e0ca940090631360b060e8b7->leave($__internal_84dc185014c17bcfab83d0c0b3d7b471b01b0d31e0ca940090631360b060e8b7_prof);

    }

    public function getTemplateName()
    {
        return ":categorie:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 15,  51 => 9,  47 => 8,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends "FilmBundle::index.html.twig" %}*/
/* {% block A %}*/
/*     <div class="row">*/
/* */
/*         <div class="col-md-3">*/
/*             <p class="lead">Gestion des categories</p>*/
/*             <div class="list-group">*/
/*                 <a href="{{path('category_index')}}" class="list-group-item">lister</a>*/
/*                 <a href="{{path('category_new')}}" class="list-group-item">creer</a>*/
/*             </div>*/
/*         </div>*/
/*         <div class="col-md-9">*/
/*             <form method="POST">*/
/* */
/*                 {{form_widget(form)}}*/
/*                 <input type="submit" value="envoyer"/>    */
/*             </form>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
