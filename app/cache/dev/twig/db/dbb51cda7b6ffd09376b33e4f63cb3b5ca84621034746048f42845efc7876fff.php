<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_1feeb99e65b2eed7c7e50424d5c3bc682b59a1dde94734c6c6c876a76d454fb4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8deac680ee7f3e65034a7c76b392054e08ba4bdc82a7e969241c30fc6b6ca13a = $this->env->getExtension("native_profiler");
        $__internal_8deac680ee7f3e65034a7c76b392054e08ba4bdc82a7e969241c30fc6b6ca13a->enter($__internal_8deac680ee7f3e65034a7c76b392054e08ba4bdc82a7e969241c30fc6b6ca13a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8deac680ee7f3e65034a7c76b392054e08ba4bdc82a7e969241c30fc6b6ca13a->leave($__internal_8deac680ee7f3e65034a7c76b392054e08ba4bdc82a7e969241c30fc6b6ca13a_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_a81525581f5005b71bce82a7a4e9cc1e5e2cf26efe91585c5515ba5b5c3a6b99 = $this->env->getExtension("native_profiler");
        $__internal_a81525581f5005b71bce82a7a4e9cc1e5e2cf26efe91585c5515ba5b5c3a6b99->enter($__internal_a81525581f5005b71bce82a7a4e9cc1e5e2cf26efe91585c5515ba5b5c3a6b99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_a81525581f5005b71bce82a7a4e9cc1e5e2cf26efe91585c5515ba5b5c3a6b99->leave($__internal_a81525581f5005b71bce82a7a4e9cc1e5e2cf26efe91585c5515ba5b5c3a6b99_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_de3675eb84485b77209097a459084ad412ecef32a5012e36bff31dfe8cbd5bc0 = $this->env->getExtension("native_profiler");
        $__internal_de3675eb84485b77209097a459084ad412ecef32a5012e36bff31dfe8cbd5bc0->enter($__internal_de3675eb84485b77209097a459084ad412ecef32a5012e36bff31dfe8cbd5bc0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_de3675eb84485b77209097a459084ad412ecef32a5012e36bff31dfe8cbd5bc0->leave($__internal_de3675eb84485b77209097a459084ad412ecef32a5012e36bff31dfe8cbd5bc0_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_ba98ab429ef5ce14e275922b0ec206d184071d43ea9f8fc7999e57705a72f779 = $this->env->getExtension("native_profiler");
        $__internal_ba98ab429ef5ce14e275922b0ec206d184071d43ea9f8fc7999e57705a72f779->enter($__internal_ba98ab429ef5ce14e275922b0ec206d184071d43ea9f8fc7999e57705a72f779_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_ba98ab429ef5ce14e275922b0ec206d184071d43ea9f8fc7999e57705a72f779->leave($__internal_ba98ab429ef5ce14e275922b0ec206d184071d43ea9f8fc7999e57705a72f779_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
