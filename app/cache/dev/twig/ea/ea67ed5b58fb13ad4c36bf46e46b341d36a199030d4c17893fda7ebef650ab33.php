<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_6eb711c350648dacddff33cf17a06aaa17d0b00be8b9a083f235aca554932791 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_083639c276d5c2eb3b11a40412db6cac3621c140e4f30d766ec0d1236739ea56 = $this->env->getExtension("native_profiler");
        $__internal_083639c276d5c2eb3b11a40412db6cac3621c140e4f30d766ec0d1236739ea56->enter($__internal_083639c276d5c2eb3b11a40412db6cac3621c140e4f30d766ec0d1236739ea56_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_083639c276d5c2eb3b11a40412db6cac3621c140e4f30d766ec0d1236739ea56->leave($__internal_083639c276d5c2eb3b11a40412db6cac3621c140e4f30d766ec0d1236739ea56_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_6ad6bac0514c710c527090867c613c081405d9ac6034429ff5b4ec2a25407853 = $this->env->getExtension("native_profiler");
        $__internal_6ad6bac0514c710c527090867c613c081405d9ac6034429ff5b4ec2a25407853->enter($__internal_6ad6bac0514c710c527090867c613c081405d9ac6034429ff5b4ec2a25407853_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_6ad6bac0514c710c527090867c613c081405d9ac6034429ff5b4ec2a25407853->leave($__internal_6ad6bac0514c710c527090867c613c081405d9ac6034429ff5b4ec2a25407853_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_5208c461a6f1da7e85268362e330af7fab5b3e0b61eb1102a95c17ccdaedc6f8 = $this->env->getExtension("native_profiler");
        $__internal_5208c461a6f1da7e85268362e330af7fab5b3e0b61eb1102a95c17ccdaedc6f8->enter($__internal_5208c461a6f1da7e85268362e330af7fab5b3e0b61eb1102a95c17ccdaedc6f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_5208c461a6f1da7e85268362e330af7fab5b3e0b61eb1102a95c17ccdaedc6f8->leave($__internal_5208c461a6f1da7e85268362e330af7fab5b3e0b61eb1102a95c17ccdaedc6f8_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_8372f09ee547f750899a9d9b7075757fef331a93885ad651a0ed24c429a26473 = $this->env->getExtension("native_profiler");
        $__internal_8372f09ee547f750899a9d9b7075757fef331a93885ad651a0ed24c429a26473->enter($__internal_8372f09ee547f750899a9d9b7075757fef331a93885ad651a0ed24c429a26473_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_8372f09ee547f750899a9d9b7075757fef331a93885ad651a0ed24c429a26473->leave($__internal_8372f09ee547f750899a9d9b7075757fef331a93885ad651a0ed24c429a26473_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
