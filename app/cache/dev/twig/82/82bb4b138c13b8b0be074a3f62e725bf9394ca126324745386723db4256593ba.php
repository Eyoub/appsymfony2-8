<?php

/* base.html.twig */
class __TwigTemplate_48ce4bdf1af7e706787525761eb5c90d0f841c6c22c038028039fa5b11049e5f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d534a3ba8c2069a20551545953639e2aa72ceb9978bda37c91307e5bc3e5e0ac = $this->env->getExtension("native_profiler");
        $__internal_d534a3ba8c2069a20551545953639e2aa72ceb9978bda37c91307e5bc3e5e0ac->enter($__internal_d534a3ba8c2069a20551545953639e2aa72ceb9978bda37c91307e5bc3e5e0ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_d534a3ba8c2069a20551545953639e2aa72ceb9978bda37c91307e5bc3e5e0ac->leave($__internal_d534a3ba8c2069a20551545953639e2aa72ceb9978bda37c91307e5bc3e5e0ac_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_c788431529deb7108faac48785782cccc905eab6361e877bd314d4a032bc8499 = $this->env->getExtension("native_profiler");
        $__internal_c788431529deb7108faac48785782cccc905eab6361e877bd314d4a032bc8499->enter($__internal_c788431529deb7108faac48785782cccc905eab6361e877bd314d4a032bc8499_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_c788431529deb7108faac48785782cccc905eab6361e877bd314d4a032bc8499->leave($__internal_c788431529deb7108faac48785782cccc905eab6361e877bd314d4a032bc8499_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_0d0ad1a2d0b474bdc1bb50509dee3a15fda49ae4439d8fa586efa109baab32c8 = $this->env->getExtension("native_profiler");
        $__internal_0d0ad1a2d0b474bdc1bb50509dee3a15fda49ae4439d8fa586efa109baab32c8->enter($__internal_0d0ad1a2d0b474bdc1bb50509dee3a15fda49ae4439d8fa586efa109baab32c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_0d0ad1a2d0b474bdc1bb50509dee3a15fda49ae4439d8fa586efa109baab32c8->leave($__internal_0d0ad1a2d0b474bdc1bb50509dee3a15fda49ae4439d8fa586efa109baab32c8_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_c79396d717836392036add5051b17f7f1cb91b189a0cb41e069b01237f849540 = $this->env->getExtension("native_profiler");
        $__internal_c79396d717836392036add5051b17f7f1cb91b189a0cb41e069b01237f849540->enter($__internal_c79396d717836392036add5051b17f7f1cb91b189a0cb41e069b01237f849540_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_c79396d717836392036add5051b17f7f1cb91b189a0cb41e069b01237f849540->leave($__internal_c79396d717836392036add5051b17f7f1cb91b189a0cb41e069b01237f849540_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_465c9725161eaac326bf2b9a6aa9fd2cf8e6340b58d435c7dcee84abf43557b5 = $this->env->getExtension("native_profiler");
        $__internal_465c9725161eaac326bf2b9a6aa9fd2cf8e6340b58d435c7dcee84abf43557b5->enter($__internal_465c9725161eaac326bf2b9a6aa9fd2cf8e6340b58d435c7dcee84abf43557b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_465c9725161eaac326bf2b9a6aa9fd2cf8e6340b58d435c7dcee84abf43557b5->leave($__internal_465c9725161eaac326bf2b9a6aa9fd2cf8e6340b58d435c7dcee84abf43557b5_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8" />*/
/*         <title>{% block title %}Welcome!{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
