<?php

/* @Security/Collector/icon.svg */
class __TwigTemplate_b4e5b9c61bd654c055e3b02e42976f0cff2b350e35f344da921ce9ff50bbe099 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5907c22b7bfc7c88b476e99c5462534adcf8aa9500f0a9e96d84dee96d1d8120 = $this->env->getExtension("native_profiler");
        $__internal_5907c22b7bfc7c88b476e99c5462534adcf8aa9500f0a9e96d84dee96d1d8120->enter($__internal_5907c22b7bfc7c88b476e99c5462534adcf8aa9500f0a9e96d84dee96d1d8120_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
";
        
        $__internal_5907c22b7bfc7c88b476e99c5462534adcf8aa9500f0a9e96d84dee96d1d8120->leave($__internal_5907c22b7bfc7c88b476e99c5462534adcf8aa9500f0a9e96d84dee96d1d8120_prof);

    }

    public function getTemplateName()
    {
        return "@Security/Collector/icon.svg";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="24" height="24" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">*/
/*     <path fill="#AAAAAA" d="M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z"/>*/
/* </svg>*/
/* */
