<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_03d891bfed20ddd97f18fe974be60cd5f2c36337abc76fcb016c39c5116ae88c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_169f72a2f0785d51d70ae97c166b75488735af806960871dd6a40d29b62bcbf0 = $this->env->getExtension("native_profiler");
        $__internal_169f72a2f0785d51d70ae97c166b75488735af806960871dd6a40d29b62bcbf0->enter($__internal_169f72a2f0785d51d70ae97c166b75488735af806960871dd6a40d29b62bcbf0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_169f72a2f0785d51d70ae97c166b75488735af806960871dd6a40d29b62bcbf0->leave($__internal_169f72a2f0785d51d70ae97c166b75488735af806960871dd6a40d29b62bcbf0_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
