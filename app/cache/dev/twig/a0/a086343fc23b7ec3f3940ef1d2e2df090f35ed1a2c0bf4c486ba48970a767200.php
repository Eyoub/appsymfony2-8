<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_3b2f3a8b4f31180458d6d3a0776b2aa1e40e4cce11a0634aaf99a11c7957cb10 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3f4a0ab38f9bb292c5368f0876b7662dae8e76ff62fb3a9af9469b2a1b65527b = $this->env->getExtension("native_profiler");
        $__internal_3f4a0ab38f9bb292c5368f0876b7662dae8e76ff62fb3a9af9469b2a1b65527b->enter($__internal_3f4a0ab38f9bb292c5368f0876b7662dae8e76ff62fb3a9af9469b2a1b65527b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_3f4a0ab38f9bb292c5368f0876b7662dae8e76ff62fb3a9af9469b2a1b65527b->leave($__internal_3f4a0ab38f9bb292c5368f0876b7662dae8e76ff62fb3a9af9469b2a1b65527b_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}*/
/* */
