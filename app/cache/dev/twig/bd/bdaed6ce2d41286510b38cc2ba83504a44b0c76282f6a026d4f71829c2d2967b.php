<?php

/* film/editer.html.twig */
class __TwigTemplate_819c0154da3e33ec35fc0533f47a121a598e25f3ea6e2ab03bdce702ed724401 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FilmBundle::index.html.twig", "film/editer.html.twig", 1);
        $this->blocks = array(
            'A' => array($this, 'block_A'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FilmBundle::index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_83f7c380a01873fdfa3b288d7eb9eeb6fbdea81b9b0e1fa0727878113626de00 = $this->env->getExtension("native_profiler");
        $__internal_83f7c380a01873fdfa3b288d7eb9eeb6fbdea81b9b0e1fa0727878113626de00->enter($__internal_83f7c380a01873fdfa3b288d7eb9eeb6fbdea81b9b0e1fa0727878113626de00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "film/editer.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_83f7c380a01873fdfa3b288d7eb9eeb6fbdea81b9b0e1fa0727878113626de00->leave($__internal_83f7c380a01873fdfa3b288d7eb9eeb6fbdea81b9b0e1fa0727878113626de00_prof);

    }

    // line 3
    public function block_A($context, array $blocks = array())
    {
        $__internal_1382d9a4f6fad2108d88e80ec478a62734979d3e2f21b3cf87e6509a2b3e13b2 = $this->env->getExtension("native_profiler");
        $__internal_1382d9a4f6fad2108d88e80ec478a62734979d3e2f21b3cf87e6509a2b3e13b2->enter($__internal_1382d9a4f6fad2108d88e80ec478a62734979d3e2f21b3cf87e6509a2b3e13b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "A"));

        // line 4
        echo "    
    <div class=\"row\">

            <div class=\"col-md-3\">
                <p class=\"lead\">Gestion des filmes</p>
                <div class=\"list-group\">
                    <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("film_index");
        echo "\" class=\"list-group-item\">lister</a>
                    <a href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("film_new");
        echo "\" class=\"list-group-item\">creer</a>
                    <a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("film_editer");
        echo "\" class=\"list-group-item\">editer</a>
                </div>
            </div>
<div class=\"col-md-9\">
<table class=\"table table-striped\">
    <thead>
            <tr>
                <th>Id</th>
                <th>Titre</th>
                <th>Description</th>
                <th>acteurs</th>
                <th>categorie</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        
";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["films"]) ? $context["films"] : $this->getContext($context, "films")));
        foreach ($context['_seq'] as $context["_key"] => $context["film"]) {
            // line 31
            echo "  
 <tr>
     <td>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["film"], "id", array()), "html", null, true);
            echo "</td><td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["film"], "titre", array()), "html", null, true);
            echo "</td><td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["film"], "description", array()), "html", null, true);
            echo "</td>
     
     <td>
    ";
            // line 36
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["film"], "acteurs", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["act"]) {
                // line 37
                echo "        ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["act"], "nom", array()), "html", null, true);
                echo " ";
                if ( !$this->getAttribute($context["loop"], "last", array())) {
                    echo ", ";
                }
                // line 38
                echo "    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['act'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 39
            echo "    </td>


     <td>";
            // line 42
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["film"], "categorie", array()), "nom", array()), "html", null, true);
            echo "</td>
     <td><a href=\"";
            // line 43
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("film_edit", array("id" => $this->getAttribute($context["film"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-edit\"></span> modifier</a></td>
     <td><a href=\"";
            // line 44
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("film_delete", array("id" => $this->getAttribute($context["film"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-danger\"><span class=\"glyphicon glyphicon-trash\"></span> supprimer</a></td>
  </tr>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['film'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "</tbody>
</table>
</div>
    </div>
    
";
        
        $__internal_1382d9a4f6fad2108d88e80ec478a62734979d3e2f21b3cf87e6509a2b3e13b2->leave($__internal_1382d9a4f6fad2108d88e80ec478a62734979d3e2f21b3cf87e6509a2b3e13b2_prof);

    }

    public function getTemplateName()
    {
        return "film/editer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 47,  146 => 44,  142 => 43,  138 => 42,  133 => 39,  119 => 38,  112 => 37,  95 => 36,  85 => 33,  81 => 31,  77 => 30,  56 => 12,  52 => 11,  48 => 10,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FilmBundle::index.html.twig" %}*/
/* */
/* {% block A %}*/
/*     */
/*     <div class="row">*/
/* */
/*             <div class="col-md-3">*/
/*                 <p class="lead">Gestion des filmes</p>*/
/*                 <div class="list-group">*/
/*                     <a href="{{path('film_index')}}" class="list-group-item">lister</a>*/
/*                     <a href="{{path('film_new')}}" class="list-group-item">creer</a>*/
/*                     <a href="{{path('film_editer')}}" class="list-group-item">editer</a>*/
/*                 </div>*/
/*             </div>*/
/* <div class="col-md-9">*/
/* <table class="table table-striped">*/
/*     <thead>*/
/*             <tr>*/
/*                 <th>Id</th>*/
/*                 <th>Titre</th>*/
/*                 <th>Description</th>*/
/*                 <th>acteurs</th>*/
/*                 <th>categorie</th>*/
/*                 <th></th>*/
/*                 <th></th>*/
/*             </tr>*/
/*         </thead>*/
/*         <tbody>*/
/*         */
/* {% for film in films %}*/
/*   */
/*  <tr>*/
/*      <td>{{film.id}}</td><td>{{film.titre}}</td><td>{{film.description}}</td>*/
/*      */
/*      <td>*/
/*     {% for act in film.acteurs %}*/
/*         {{act.nom}} {% if not loop.last %}, {% endif %}*/
/*     {% endfor %}*/
/*     </td>*/
/* */
/* */
/*      <td>{{film.categorie.nom}}</td>*/
/*      <td><a href="{{path('film_edit', {'id':film.id})}}" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span> modifier</a></td>*/
/*      <td><a href="{{path('film_delete', {'id':film.id})}}" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> supprimer</a></td>*/
/*   </tr>*/
/* {% endfor %}*/
/* </tbody>*/
/* </table>*/
/* </div>*/
/*     </div>*/
/*     */
/* {% endblock %}*/
