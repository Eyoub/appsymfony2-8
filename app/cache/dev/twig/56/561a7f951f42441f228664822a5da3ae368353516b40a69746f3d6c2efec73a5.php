<?php

/* @FOSUser/ChangePassword/change_password.html.twig */
class __TwigTemplate_44ce04239ae4c024ad68d9dcb84a461b0818dc9f0fb4803d254594f808411305 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/ChangePassword/change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6b2a89c391bc909b05f8317019f4aca331a9998fe662de32ca53f8dc529340c6 = $this->env->getExtension("native_profiler");
        $__internal_6b2a89c391bc909b05f8317019f4aca331a9998fe662de32ca53f8dc529340c6->enter($__internal_6b2a89c391bc909b05f8317019f4aca331a9998fe662de32ca53f8dc529340c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/ChangePassword/change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6b2a89c391bc909b05f8317019f4aca331a9998fe662de32ca53f8dc529340c6->leave($__internal_6b2a89c391bc909b05f8317019f4aca331a9998fe662de32ca53f8dc529340c6_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_7908618f1505ab96a30abbfead93ac10496225ad78215bfeec6610ca2ad1715c = $this->env->getExtension("native_profiler");
        $__internal_7908618f1505ab96a30abbfead93ac10496225ad78215bfeec6610ca2ad1715c->enter($__internal_7908618f1505ab96a30abbfead93ac10496225ad78215bfeec6610ca2ad1715c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:ChangePassword:change_password_content.html.twig", "@FOSUser/ChangePassword/change_password.html.twig", 4)->display($context);
        
        $__internal_7908618f1505ab96a30abbfead93ac10496225ad78215bfeec6610ca2ad1715c->leave($__internal_7908618f1505ab96a30abbfead93ac10496225ad78215bfeec6610ca2ad1715c_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/ChangePassword/change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:ChangePassword:change_password_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
