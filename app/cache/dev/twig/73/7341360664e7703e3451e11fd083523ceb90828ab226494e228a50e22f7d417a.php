<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_799f0503cfd572e30a00327ce88ec1c7c53232957dc042c5a5f3862c50d528e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b7779ae392aa321aec8e0a86b506454598cdee3a309d09ecd4c083407a36d444 = $this->env->getExtension("native_profiler");
        $__internal_b7779ae392aa321aec8e0a86b506454598cdee3a309d09ecd4c083407a36d444->enter($__internal_b7779ae392aa321aec8e0a86b506454598cdee3a309d09ecd4c083407a36d444_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_b7779ae392aa321aec8e0a86b506454598cdee3a309d09ecd4c083407a36d444->leave($__internal_b7779ae392aa321aec8e0a86b506454598cdee3a309d09ecd4c083407a36d444_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->start($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* <?php echo $view['form']->end($form) ?>*/
/* */
