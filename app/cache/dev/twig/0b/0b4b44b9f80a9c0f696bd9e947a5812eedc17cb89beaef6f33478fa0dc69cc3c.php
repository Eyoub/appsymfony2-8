<?php

/* FOSUserBundle:Group:edit.html.twig */
class __TwigTemplate_9d4a483662fb79aa3fbe9c117629a1cd0926c3619466ed23c4369687560b4aa1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Group:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_019ba9bd05aea443bd309a7c21e63607287f683c692229b39bb19533a90c9ed7 = $this->env->getExtension("native_profiler");
        $__internal_019ba9bd05aea443bd309a7c21e63607287f683c692229b39bb19533a90c9ed7->enter($__internal_019ba9bd05aea443bd309a7c21e63607287f683c692229b39bb19533a90c9ed7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_019ba9bd05aea443bd309a7c21e63607287f683c692229b39bb19533a90c9ed7->leave($__internal_019ba9bd05aea443bd309a7c21e63607287f683c692229b39bb19533a90c9ed7_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_63ea6ef0268c1e060c29511d166571427eb18c02047cf495cbafd5b8610ace05 = $this->env->getExtension("native_profiler");
        $__internal_63ea6ef0268c1e060c29511d166571427eb18c02047cf495cbafd5b8610ace05->enter($__internal_63ea6ef0268c1e060c29511d166571427eb18c02047cf495cbafd5b8610ace05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:edit_content.html.twig", "FOSUserBundle:Group:edit.html.twig", 4)->display($context);
        
        $__internal_63ea6ef0268c1e060c29511d166571427eb18c02047cf495cbafd5b8610ace05->leave($__internal_63ea6ef0268c1e060c29511d166571427eb18c02047cf495cbafd5b8610ace05_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:edit_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
