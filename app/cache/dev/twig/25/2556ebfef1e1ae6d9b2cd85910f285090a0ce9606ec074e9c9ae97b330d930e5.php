<?php

/* @FOSUser/Resetting/check_email.html.twig */
class __TwigTemplate_f5549ae6c59d21e166612734db12a20dc6911aae3e28a36fd594cd452170c4a5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Resetting/check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0ca3eaa62f33bf24603e76e748454c7cabd0f79c09d144b5528f893deb9ca833 = $this->env->getExtension("native_profiler");
        $__internal_0ca3eaa62f33bf24603e76e748454c7cabd0f79c09d144b5528f893deb9ca833->enter($__internal_0ca3eaa62f33bf24603e76e748454c7cabd0f79c09d144b5528f893deb9ca833_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0ca3eaa62f33bf24603e76e748454c7cabd0f79c09d144b5528f893deb9ca833->leave($__internal_0ca3eaa62f33bf24603e76e748454c7cabd0f79c09d144b5528f893deb9ca833_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_c278c9cd81183bded916dbed811829e25f2f0c17ac415716a7c53c8bc9cc4830 = $this->env->getExtension("native_profiler");
        $__internal_c278c9cd81183bded916dbed811829e25f2f0c17ac415716a7c53c8bc9cc4830->enter($__internal_c278c9cd81183bded916dbed811829e25f2f0c17ac415716a7c53c8bc9cc4830_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.check_email", array("%email%" => (isset($context["email"]) ? $context["email"] : $this->getContext($context, "email"))), "FOSUserBundle"), "html", null, true);
        echo "
</p>
";
        
        $__internal_c278c9cd81183bded916dbed811829e25f2f0c17ac415716a7c53c8bc9cc4830->leave($__internal_c278c9cd81183bded916dbed811829e25f2f0c17ac415716a7c53c8bc9cc4830_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 7,  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/* <p>*/
/* {{ 'resetting.check_email'|trans({'%email%': email}) }}*/
/* </p>*/
/* {% endblock %}*/
/* */
