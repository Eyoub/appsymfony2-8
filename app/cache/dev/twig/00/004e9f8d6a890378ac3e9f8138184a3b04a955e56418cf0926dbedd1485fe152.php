<?php

/* @FOSUser/Resetting/password_already_requested.html.twig */
class __TwigTemplate_cf7581caee5b2016d8cc1d5fe70438c4349240282e50a64d5fd07d30a08ed7cd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Resetting/password_already_requested.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_87ee194711fcb669c408c6dba99ffbeaa1d4da339709b311f5e25404ac82aa97 = $this->env->getExtension("native_profiler");
        $__internal_87ee194711fcb669c408c6dba99ffbeaa1d4da339709b311f5e25404ac82aa97->enter($__internal_87ee194711fcb669c408c6dba99ffbeaa1d4da339709b311f5e25404ac82aa97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/password_already_requested.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_87ee194711fcb669c408c6dba99ffbeaa1d4da339709b311f5e25404ac82aa97->leave($__internal_87ee194711fcb669c408c6dba99ffbeaa1d4da339709b311f5e25404ac82aa97_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_36e718458f7b29524b9064e3c18b92ebf9a74e484d2a139413137609824cc030 = $this->env->getExtension("native_profiler");
        $__internal_36e718458f7b29524b9064e3c18b92ebf9a74e484d2a139413137609824cc030->enter($__internal_36e718458f7b29524b9064e3c18b92ebf9a74e484d2a139413137609824cc030_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.password_already_requested", array(), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_36e718458f7b29524b9064e3c18b92ebf9a74e484d2a139413137609824cc030->leave($__internal_36e718458f7b29524b9064e3c18b92ebf9a74e484d2a139413137609824cc030_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/password_already_requested.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/* <p>{{ 'resetting.password_already_requested'|trans }}</p>*/
/* {% endblock fos_user_content %}*/
/* */
