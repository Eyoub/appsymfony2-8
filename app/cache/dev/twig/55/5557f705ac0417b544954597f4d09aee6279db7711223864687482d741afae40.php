<?php

/* FOSUserBundle:Resetting:reset.html.twig */
class __TwigTemplate_ebe1ce3cffb3e6d8d20eb1f2e29f145c712b41c55f1bfbbcd327d12c57d5da51 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3afa55a385095c2a578c8c9f767f207dda2604c0b3f4a910a203ac8761394205 = $this->env->getExtension("native_profiler");
        $__internal_3afa55a385095c2a578c8c9f767f207dda2604c0b3f4a910a203ac8761394205->enter($__internal_3afa55a385095c2a578c8c9f767f207dda2604c0b3f4a910a203ac8761394205_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3afa55a385095c2a578c8c9f767f207dda2604c0b3f4a910a203ac8761394205->leave($__internal_3afa55a385095c2a578c8c9f767f207dda2604c0b3f4a910a203ac8761394205_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_91dc20654a3871fd684d6e8b323845e4ac7962709e6d18b8c2bade4e81a1b181 = $this->env->getExtension("native_profiler");
        $__internal_91dc20654a3871fd684d6e8b323845e4ac7962709e6d18b8c2bade4e81a1b181->enter($__internal_91dc20654a3871fd684d6e8b323845e4ac7962709e6d18b8c2bade4e81a1b181_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Resetting:reset_content.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 4)->display($context);
        
        $__internal_91dc20654a3871fd684d6e8b323845e4ac7962709e6d18b8c2bade4e81a1b181->leave($__internal_91dc20654a3871fd684d6e8b323845e4ac7962709e6d18b8c2bade4e81a1b181_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Resetting:reset_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
