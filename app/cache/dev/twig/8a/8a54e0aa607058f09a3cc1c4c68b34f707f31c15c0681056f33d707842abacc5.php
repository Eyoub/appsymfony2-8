<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_d157694d979a85af9530e094dd436b8154f11547b9cb0fd1c1893ec83ba3e984 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1443f3fc26fbfddfb96acf02a6e42465066c8b02d453951ad7f91f18e833fa70 = $this->env->getExtension("native_profiler");
        $__internal_1443f3fc26fbfddfb96acf02a6e42465066c8b02d453951ad7f91f18e833fa70->enter($__internal_1443f3fc26fbfddfb96acf02a6e42465066c8b02d453951ad7f91f18e833fa70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_1443f3fc26fbfddfb96acf02a6e42465066c8b02d453951ad7f91f18e833fa70->leave($__internal_1443f3fc26fbfddfb96acf02a6e42465066c8b02d453951ad7f91f18e833fa70_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($compound): ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_compound')?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_simple')?>*/
/* <?php endif ?>*/
/* */
