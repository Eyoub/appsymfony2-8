<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_bd4d91f4c9f015b35e5d442fbfec84dfd70b29f5fa22abea4662454426c245ec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5494c572789929a48618e2df1e3a23f369d3f88ad2451a038df5c9075de30cdb = $this->env->getExtension("native_profiler");
        $__internal_5494c572789929a48618e2df1e3a23f369d3f88ad2451a038df5c9075de30cdb->enter($__internal_5494c572789929a48618e2df1e3a23f369d3f88ad2451a038df5c9075de30cdb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5494c572789929a48618e2df1e3a23f369d3f88ad2451a038df5c9075de30cdb->leave($__internal_5494c572789929a48618e2df1e3a23f369d3f88ad2451a038df5c9075de30cdb_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_231ac7d8f4c520e30d6b275aae1e6395be2b1d0175bbd16f55f0e5a6f21ec9e6 = $this->env->getExtension("native_profiler");
        $__internal_231ac7d8f4c520e30d6b275aae1e6395be2b1d0175bbd16f55f0e5a6f21ec9e6->enter($__internal_231ac7d8f4c520e30d6b275aae1e6395be2b1d0175bbd16f55f0e5a6f21ec9e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_231ac7d8f4c520e30d6b275aae1e6395be2b1d0175bbd16f55f0e5a6f21ec9e6->leave($__internal_231ac7d8f4c520e30d6b275aae1e6395be2b1d0175bbd16f55f0e5a6f21ec9e6_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_110bdca1e580574f75ac983f7b9bd3cea86a6809d205f8f321dc1f8e83b3619b = $this->env->getExtension("native_profiler");
        $__internal_110bdca1e580574f75ac983f7b9bd3cea86a6809d205f8f321dc1f8e83b3619b->enter($__internal_110bdca1e580574f75ac983f7b9bd3cea86a6809d205f8f321dc1f8e83b3619b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_110bdca1e580574f75ac983f7b9bd3cea86a6809d205f8f321dc1f8e83b3619b->leave($__internal_110bdca1e580574f75ac983f7b9bd3cea86a6809d205f8f321dc1f8e83b3619b_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_9e34eb49b8686144b6860fdb0060f7a00cb69ab9a2cb608c22884e1c660dfb71 = $this->env->getExtension("native_profiler");
        $__internal_9e34eb49b8686144b6860fdb0060f7a00cb69ab9a2cb608c22884e1c660dfb71->enter($__internal_9e34eb49b8686144b6860fdb0060f7a00cb69ab9a2cb608c22884e1c660dfb71_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_9e34eb49b8686144b6860fdb0060f7a00cb69ab9a2cb608c22884e1c660dfb71->leave($__internal_9e34eb49b8686144b6860fdb0060f7a00cb69ab9a2cb608c22884e1c660dfb71_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
