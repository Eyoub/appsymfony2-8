<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_3a44cb4f7415b44c18a5e4c8cdbc3870a3a4e250223e016a1f8d1ff0179024ab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_91209eb2b861bc23c7098610a9ba4f2d67021ab49cedd34c4dfbacfe58806d7c = $this->env->getExtension("native_profiler");
        $__internal_91209eb2b861bc23c7098610a9ba4f2d67021ab49cedd34c4dfbacfe58806d7c->enter($__internal_91209eb2b861bc23c7098610a9ba4f2d67021ab49cedd34c4dfbacfe58806d7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_91209eb2b861bc23c7098610a9ba4f2d67021ab49cedd34c4dfbacfe58806d7c->leave($__internal_91209eb2b861bc23c7098610a9ba4f2d67021ab49cedd34c4dfbacfe58806d7c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td>*/
/*         <?php echo $view['form']->label($form) ?>*/
/*     </td>*/
/*     <td>*/
/*         <?php echo $view['form']->errors($form) ?>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
