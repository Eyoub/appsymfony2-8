<?php

/* categorie/edit.html.twig */
class __TwigTemplate_77419b477ba9e6e53867a109157d574aaba8e1539ab255ebe3bcb81176aac89b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "categorie/edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9138ff2900ad23f7a915838a4428df2437f18d36397acfc963f5aa9cec57c6e9 = $this->env->getExtension("native_profiler");
        $__internal_9138ff2900ad23f7a915838a4428df2437f18d36397acfc963f5aa9cec57c6e9->enter($__internal_9138ff2900ad23f7a915838a4428df2437f18d36397acfc963f5aa9cec57c6e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "categorie/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9138ff2900ad23f7a915838a4428df2437f18d36397acfc963f5aa9cec57c6e9->leave($__internal_9138ff2900ad23f7a915838a4428df2437f18d36397acfc963f5aa9cec57c6e9_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_6fe534648321378a4beef2f925a569fb7bca90d9ae5ca45a664d4694eb26e37a = $this->env->getExtension("native_profiler");
        $__internal_6fe534648321378a4beef2f925a569fb7bca90d9ae5ca45a664d4694eb26e37a->enter($__internal_6fe534648321378a4beef2f925a569fb7bca90d9ae5ca45a664d4694eb26e37a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Categorie edit</h1>

    ";
        // line 6
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Edit\" />
    ";
        // line 9
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("categorie_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            ";
        // line 16
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 18
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_6fe534648321378a4beef2f925a569fb7bca90d9ae5ca45a664d4694eb26e37a->leave($__internal_6fe534648321378a4beef2f925a569fb7bca90d9ae5ca45a664d4694eb26e37a_prof);

    }

    public function getTemplateName()
    {
        return "categorie/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 18,  66 => 16,  60 => 13,  53 => 9,  48 => 7,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/*     <h1>Categorie edit</h1>*/
/* */
/*     {{ form_start(edit_form) }}*/
/*         {{ form_widget(edit_form) }}*/
/*         <input type="submit" value="Edit" />*/
/*     {{ form_end(edit_form) }}*/
/* */
/*     <ul>*/
/*         <li>*/
/*             <a href="{{ path('categorie_index') }}">Back to the list</a>*/
/*         </li>*/
/*         <li>*/
/*             {{ form_start(delete_form) }}*/
/*                 <input type="submit" value="Delete">*/
/*             {{ form_end(delete_form) }}*/
/*         </li>*/
/*     </ul>*/
/* {% endblock %}*/
/* */
