<?php

/* @Twig/Exception/error.js.twig */
class __TwigTemplate_641a2837529451d09cbaff96a9f5051671101ac288cf8f4e6a616f0d0e7bbc23 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9783a8ee97254fadb4f43f835dbdccad5004373ecc9141aaf1ef665fec8321c3 = $this->env->getExtension("native_profiler");
        $__internal_9783a8ee97254fadb4f43f835dbdccad5004373ecc9141aaf1ef665fec8321c3->enter($__internal_9783a8ee97254fadb4f43f835dbdccad5004373ecc9141aaf1ef665fec8321c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_9783a8ee97254fadb4f43f835dbdccad5004373ecc9141aaf1ef665fec8321c3->leave($__internal_9783a8ee97254fadb4f43f835dbdccad5004373ecc9141aaf1ef665fec8321c3_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
