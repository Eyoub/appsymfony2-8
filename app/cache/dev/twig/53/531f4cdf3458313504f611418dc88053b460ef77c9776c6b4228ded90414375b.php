<?php

/* @FOSUser/Registration/confirmed.html.twig */
class __TwigTemplate_b66366cad9c4ffbeab24644e7cac4c0d23a5771c53af5087f4ba98e8b1a8819d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Registration/confirmed.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d7716e5e214e9db83c1c0ef2fad69b57d5e884ea257dc62aae4588f864a3abb3 = $this->env->getExtension("native_profiler");
        $__internal_d7716e5e214e9db83c1c0ef2fad69b57d5e884ea257dc62aae4588f864a3abb3->enter($__internal_d7716e5e214e9db83c1c0ef2fad69b57d5e884ea257dc62aae4588f864a3abb3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d7716e5e214e9db83c1c0ef2fad69b57d5e884ea257dc62aae4588f864a3abb3->leave($__internal_d7716e5e214e9db83c1c0ef2fad69b57d5e884ea257dc62aae4588f864a3abb3_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_e9aaa6717cc15e69874ce137655bfea4d7dee2c144f8931e560c8b187b4afcbc = $this->env->getExtension("native_profiler");
        $__internal_e9aaa6717cc15e69874ce137655bfea4d7dee2c144f8931e560c8b187b4afcbc->enter($__internal_e9aaa6717cc15e69874ce137655bfea4d7dee2c144f8931e560c8b187b4afcbc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.confirmed", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if ((isset($context["targetUrl"]) ? $context["targetUrl"] : $this->getContext($context, "targetUrl"))) {
            // line 8
            echo "    <p><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["targetUrl"]) ? $context["targetUrl"] : $this->getContext($context, "targetUrl")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        
        $__internal_e9aaa6717cc15e69874ce137655bfea4d7dee2c144f8931e560c8b187b4afcbc->leave($__internal_e9aaa6717cc15e69874ce137655bfea4d7dee2c144f8931e560c8b187b4afcbc_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 8,  45 => 7,  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/*     <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>*/
/*     {% if targetUrl %}*/
/*     <p><a href="{{ targetUrl }}">{{ 'registration.back'|trans }}</a></p>*/
/*     {% endif %}*/
/* {% endblock fos_user_content %}*/
/* */
