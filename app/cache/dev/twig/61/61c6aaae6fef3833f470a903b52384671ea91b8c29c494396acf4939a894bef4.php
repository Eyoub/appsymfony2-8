<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_ef12c67e5f51c110b64ee30dfab99cba8155a74ba022164d2bef6b58b92dce31 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b4ed7806185e1d5e1aa9b84c0b8e63f0980ce0bf516720015f17f31d03552d3b = $this->env->getExtension("native_profiler");
        $__internal_b4ed7806185e1d5e1aa9b84c0b8e63f0980ce0bf516720015f17f31d03552d3b->enter($__internal_b4ed7806185e1d5e1aa9b84c0b8e63f0980ce0bf516720015f17f31d03552d3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_b4ed7806185e1d5e1aa9b84c0b8e63f0980ce0bf516720015f17f31d03552d3b->leave($__internal_b4ed7806185e1d5e1aa9b84c0b8e63f0980ce0bf516720015f17f31d03552d3b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child): ?>*/
/*     <?php if (!$child->isRendered()): ?>*/
/*         <?php echo $view['form']->row($child) ?>*/
/*     <?php endif; ?>*/
/* <?php endforeach; ?>*/
/* */
