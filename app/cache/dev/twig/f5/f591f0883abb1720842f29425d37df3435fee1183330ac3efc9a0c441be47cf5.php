<?php

/* @Film/Acteur/affichActeur.html.twig */
class __TwigTemplate_031ae02ee5492b9537247f502d04602ca2236578965cfad23751ea59355075bf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("FilmBundle::index.html.twig", "@Film/Acteur/affichActeur.html.twig", 3);
        $this->blocks = array(
            'A' => array($this, 'block_A'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FilmBundle::index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6b12708a9194390ba492532338787fd5d06d49f276895438f5f6ff02d94ef363 = $this->env->getExtension("native_profiler");
        $__internal_6b12708a9194390ba492532338787fd5d06d49f276895438f5f6ff02d94ef363->enter($__internal_6b12708a9194390ba492532338787fd5d06d49f276895438f5f6ff02d94ef363_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Film/Acteur/affichActeur.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6b12708a9194390ba492532338787fd5d06d49f276895438f5f6ff02d94ef363->leave($__internal_6b12708a9194390ba492532338787fd5d06d49f276895438f5f6ff02d94ef363_prof);

    }

    // line 4
    public function block_A($context, array $blocks = array())
    {
        $__internal_af6f22bb3945be3e4c7cd6ba6898460a1737cce70b0033dea0bfbda7a06eccd6 = $this->env->getExtension("native_profiler");
        $__internal_af6f22bb3945be3e4c7cd6ba6898460a1737cce70b0033dea0bfbda7a06eccd6->enter($__internal_af6f22bb3945be3e4c7cd6ba6898460a1737cce70b0033dea0bfbda7a06eccd6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "A"));

        // line 5
        echo "<div class=\"row\">

            <div class=\"col-md-3\">
                <p class=\"lead\">Gestion des acteurs</p>
                <div class=\"list-group\">
                    <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("affich_act");
        echo "\" class=\"list-group-item\">Afficher</a>
                    <a href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("ajout_act");
        echo "\" class=\"list-group-item\">Ajout</a>
                    <a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("modif_act2");
        echo "\" class=\"list-group-item\">Editer</a>
                </div>
            </div>
<div class=\"col-md-9\">
<table class=\"table table-striped\">
    <thead>
<th>Id</th>
<th>nom</th>
<th>prenom</th>
<th>dateN</th>
<th>sexe</th>
<th>participé à</th>
</thead>
";
        // line 25
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["act"]) ? $context["act"] : $this->getContext($context, "act")));
        foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
            // line 26
            echo " <tr>
     <td>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "id", array()), "html", null, true);
            echo "</td>
     <td>";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "nom", array()), "html", null, true);
            echo "</td>
     <td>";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "prenom", array()), "html", null, true);
            echo "</td>
     <td>";
            // line 30
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["a"], "dateN", array()), "d-M-Y"), "html", null, true);
            echo "</td>
     <td>";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "sexe", array()), "html", null, true);
            echo "</td>
     <td>
        ";
            // line 33
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["a"], "films", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["film"]) {
                // line 34
                echo "     ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["film"], "titre", array()), "html", null, true);
                echo " ";
                if ( !$this->getAttribute($context["loop"], "last", array())) {
                    echo ", ";
                }
                // line 35
                echo "    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['film'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 36
            echo "    </td>

 </tr>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "</table>
</div>
</div>
";
        
        $__internal_af6f22bb3945be3e4c7cd6ba6898460a1737cce70b0033dea0bfbda7a06eccd6->leave($__internal_af6f22bb3945be3e4c7cd6ba6898460a1737cce70b0033dea0bfbda7a06eccd6_prof);

    }

    public function getTemplateName()
    {
        return "@Film/Acteur/affichActeur.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 40,  137 => 36,  123 => 35,  116 => 34,  99 => 33,  94 => 31,  90 => 30,  86 => 29,  82 => 28,  78 => 27,  75 => 26,  71 => 25,  55 => 12,  51 => 11,  47 => 10,  40 => 5,  34 => 4,  11 => 3,);
    }
}
/* {# empty Twig template #}*/
/* */
/* {% extends "FilmBundle::index.html.twig" %}*/
/* {% block A %}*/
/* <div class="row">*/
/* */
/*             <div class="col-md-3">*/
/*                 <p class="lead">Gestion des acteurs</p>*/
/*                 <div class="list-group">*/
/*                     <a href="{{path('affich_act')}}" class="list-group-item">Afficher</a>*/
/*                     <a href="{{path('ajout_act')}}" class="list-group-item">Ajout</a>*/
/*                     <a href="{{path('modif_act2')}}" class="list-group-item">Editer</a>*/
/*                 </div>*/
/*             </div>*/
/* <div class="col-md-9">*/
/* <table class="table table-striped">*/
/*     <thead>*/
/* <th>Id</th>*/
/* <th>nom</th>*/
/* <th>prenom</th>*/
/* <th>dateN</th>*/
/* <th>sexe</th>*/
/* <th>participé à</th>*/
/* </thead>*/
/* {% for a in act %}*/
/*  <tr>*/
/*      <td>{{a.id}}</td>*/
/*      <td>{{a.nom}}</td>*/
/*      <td>{{a.prenom}}</td>*/
/*      <td>{{a.dateN|date('d-M-Y')}}</td>*/
/*      <td>{{a.sexe}}</td>*/
/*      <td>*/
/*         {% for film in a.films %}*/
/*      {{film.titre}} {% if not loop.last %}, {% endif %}*/
/*     {% endfor %}*/
/*     </td>*/
/* */
/*  </tr>*/
/* {%endfor%}*/
/* </table>*/
/* </div>*/
/* </div>*/
/* {% endblock %}*/
/* */
/* */
