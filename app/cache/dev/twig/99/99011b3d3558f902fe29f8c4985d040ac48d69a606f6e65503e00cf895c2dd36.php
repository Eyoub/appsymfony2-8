<?php

/* categorie/new.html.twig */
class __TwigTemplate_1f8628de786f526c1f47919c55fb48a3d4517fd2fd3d5c36a5b41791a2d326c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FilmBundle::index.html.twig", "categorie/new.html.twig", 1);
        $this->blocks = array(
            'A' => array($this, 'block_A'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FilmBundle::index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0b5d9f7c76a098fd250d9dc4a583f66e2e381e277f6a4f3db33a82014d3337e5 = $this->env->getExtension("native_profiler");
        $__internal_0b5d9f7c76a098fd250d9dc4a583f66e2e381e277f6a4f3db33a82014d3337e5->enter($__internal_0b5d9f7c76a098fd250d9dc4a583f66e2e381e277f6a4f3db33a82014d3337e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "categorie/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0b5d9f7c76a098fd250d9dc4a583f66e2e381e277f6a4f3db33a82014d3337e5->leave($__internal_0b5d9f7c76a098fd250d9dc4a583f66e2e381e277f6a4f3db33a82014d3337e5_prof);

    }

    // line 2
    public function block_A($context, array $blocks = array())
    {
        $__internal_38f337022ba4892a3c4b72cef1c670d8efbdaea5ee3cb72af0feadcfc55c9ab7 = $this->env->getExtension("native_profiler");
        $__internal_38f337022ba4892a3c4b72cef1c670d8efbdaea5ee3cb72af0feadcfc55c9ab7->enter($__internal_38f337022ba4892a3c4b72cef1c670d8efbdaea5ee3cb72af0feadcfc55c9ab7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "A"));

        // line 3
        echo "    <div class=\"row\">

        <div class=\"col-md-3\">
            <p class=\"lead\">Gestion des categories</p>
            <div class=\"list-group\">
                <a href=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("category_index");
        echo "\" class=\"list-group-item\">lister</a>
                <a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("category_new");
        echo "\" class=\"list-group-item\">creer</a>
            </div>
        </div>
        <div class=\"col-md-9\">
            <form method=\"POST\">

                ";
        // line 15
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
                <input type=\"submit\" value=\"envoyer\"/>    
            </form>
        </div>
    </div>
";
        
        $__internal_38f337022ba4892a3c4b72cef1c670d8efbdaea5ee3cb72af0feadcfc55c9ab7->leave($__internal_38f337022ba4892a3c4b72cef1c670d8efbdaea5ee3cb72af0feadcfc55c9ab7_prof);

    }

    public function getTemplateName()
    {
        return "categorie/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 15,  51 => 9,  47 => 8,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends "FilmBundle::index.html.twig" %}*/
/* {% block A %}*/
/*     <div class="row">*/
/* */
/*         <div class="col-md-3">*/
/*             <p class="lead">Gestion des categories</p>*/
/*             <div class="list-group">*/
/*                 <a href="{{path('category_index')}}" class="list-group-item">lister</a>*/
/*                 <a href="{{path('category_new')}}" class="list-group-item">creer</a>*/
/*             </div>*/
/*         </div>*/
/*         <div class="col-md-9">*/
/*             <form method="POST">*/
/* */
/*                 {{form_widget(form)}}*/
/*                 <input type="submit" value="envoyer"/>    */
/*             </form>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
