<?php

/* @Film/index.html.twig */
class __TwigTemplate_b22f95073d19aa89f5dc2ebdda2bfc8cbe3374b776999d6f17cc0cff6b2af9ec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'A' => array($this, 'block_A'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d5170cf1dedd112effdcca36b525da3a009bcdac846fc8dc202ef668b2367e62 = $this->env->getExtension("native_profiler");
        $__internal_d5170cf1dedd112effdcca36b525da3a009bcdac846fc8dc202ef668b2367e62->enter($__internal_d5170cf1dedd112effdcca36b525da3a009bcdac846fc8dc202ef668b2367e62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Film/index.html.twig"));

        // line 2
        echo "
<!DOCTYPE html>
<html lang=\"en\">

<head>

    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"\">

    <title>Shop Homepage - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
\t<link href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/film/css/bootsrap-theme.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/film/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <link href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/film/css/jquery-ui.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- Custom CSS -->
    <link href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/film/css/shop-homepage.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <!--<link href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/film/css/normalize.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">-->


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
    <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
    <![endif]-->

</head>

<body>

<!-- Navigation -->
<nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
    <div class=\"container\">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\"
                    data-target=\"#bs-example-navbar-collapse-1\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a class=\"navbar-brand\" href=\"#\"><img src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/film/images/logo.png"), "html", null, true);
        echo "\" alt=\"Filmothèque\"
                                                  width=\"35\" height=\"35\" class=\"moimg\"/>
                <div class=\"moh1\"> Film</div>
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
            <ul class=\"nav navbar-nav\">
                <li>
                    <a href=\"";
        // line 58
        echo $this->env->getExtension('routing')->getPath("film_homepage");
        echo "\">Accueil</a>
                </li>
                <li>
                    <a href=\"";
        // line 61
        echo $this->env->getExtension('routing')->getPath("affich_act");
        echo "\">Acteurs</a>
                </li>
                <li>
                    <a href=\"";
        // line 64
        echo $this->env->getExtension('routing')->getPath("film_index");
        echo "\">Films</a>
                </li>
                <li>
                    <a href=\"";
        // line 67
        echo $this->env->getExtension('routing')->getPath("category_index");
        echo "\">Categories</a>
                </li>
            </ul>
            <ul class=\"nav navbar-nav navbar-right\">
                <li>
                    ";
        // line 72
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 73
            echo "                        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("layout.logged_in_as", array("%username%" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array())), "FOSUserBundle"), "html", null, true);
            echo "
                        <a href=\"";
            // line 74
            echo $this->env->getExtension('routing')->getPath("fos_user_security_logout");
            echo "\">
                            ";
            // line 75
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("layout.logout", array(), "FOSUserBundle"), "html", null, true);
            echo "
                        </a>
                    ";
        } else {
            // line 78
            echo "                        <a href=\"";
            echo $this->env->getExtension('routing')->getPath("fos_user_security_login");
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("layout.login", array(), "FOSUserBundle"), "html", null, true);
            echo "</a>
                    ";
        }
        // line 80
        echo "                </li>
                <li>
                    
                    <script>
                        
                        function subm() {
                            if (\$(\"#f input[class=sear]:checked\").val() == 1) {\$('#f').attr('action', \"";
        // line 86
        echo $this->env->getExtension('routing')->getPath("film_search");
        echo "\")};
                        }
                    </script>
                    <form id=\"f\" method=\"get\" onsubmit=\"subm()\">
                        <input type=\"text\" autocomplete=\"off\" placeholder=\"Search...\" name=\"motcle\" id=\"recherche\">
                        <input type=\"radio\" class=\"sear\" value=\"1\" checked/>Films 
                        <input type=\"radio\" class=\"sear\" value=\"2\"/>Acteurs  
                        <input type=\"radio\" class=\"sear\" value=\"3\"/>Categories  
                    </form>
                </li>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
                            
<!-- Page Content -->
<div class=\"container\">
    ";
        // line 104
        $this->displayBlock('A', $context, $blocks);
        // line 203
        echo "</div>
<!-- /.container -->

<div class=\"container\">

    <hr>

    <!-- Footer -->
    <footer>
        <div class=\"row\">
            <div class=\"col-lg-12\">
                <p>Copyright &copy; Your Website 2014</p>
            </div>
        </div>
    </footer>

</div>
<!-- /.container -->

<!-- jQuery -->
  <script src=\"";
        // line 223
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/film/js/jquery.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 224
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/film/js/jquery-ui.js"), "html", null, true);
        echo "\"></script>
  <script src=\"";
        // line 225
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/film/js/typeahead.bundle.js"), "html", null, true);
        echo "\"></script>
  
<!-- Bootstrap Core JavaScript -->
<script src=\"";
        // line 228
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/film/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 229
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/film/js/searchJSON.js"), "html", null, true);
        echo "\"></script>




</body>

</html>
";
        
        $__internal_d5170cf1dedd112effdcca36b525da3a009bcdac846fc8dc202ef668b2367e62->leave($__internal_d5170cf1dedd112effdcca36b525da3a009bcdac846fc8dc202ef668b2367e62_prof);

    }

    // line 104
    public function block_A($context, array $blocks = array())
    {
        $__internal_6c747fe5ac97d014c9049c7b7fe069681e30767130694d7623d1f9e25445d13f = $this->env->getExtension("native_profiler");
        $__internal_6c747fe5ac97d014c9049c7b7fe069681e30767130694d7623d1f9e25445d13f->enter($__internal_6c747fe5ac97d014c9049c7b7fe069681e30767130694d7623d1f9e25445d13f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "A"));

        // line 105
        echo "        <div class=\"row\">

            <div class=\"col-md-3\">
                <p class=\"lead\">Listes Des Categories</p>
                <div class=\"list-group\">
                    ";
        // line 110
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : $this->getContext($context, "categories")));
        foreach ($context['_seq'] as $context["_key"] => $context["cat"]) {
            // line 111
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("film_homepage", array("id" => $this->getAttribute($context["cat"], "id", array()))), "html", null, true);
            echo "\" class=\"list-group-item\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["cat"], "nom", array()), "html", null, true);
            echo "</a>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cat'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 113
        echo "                </div>
                
                    <div class=\"well\">
                        <form method=\"get\" action=\"";
        // line 116
        echo $this->env->getExtension('routing')->getPath("film_search");
        echo "\">
                            <fieldset>
                                <div class=\"form-group-sm\">
                                <label for=\"motcle\">Search:</label>
                                <input type=\"text\" placeholder=\"Search...\" name=\"motcle\" autocomplete=\"off\" id=\"typeahead\">
                                </div>
                                <button type=\"submit\" class=\"btn btn-primary btn-xs\" data>Search</button>
                            </fieldset>
                        </form>
                    </div>
            </div>

            <div class=\"col-md-9\">

                <!--<div class=\"row carousel-holder\">

                    <div class=\"col-md-12\">
                        <div id=\"carousel-example-generic\" class=\"carousel slide\" data-ride=\"carousel\">
                            <ol class=\"carousel-indicators\">
                                <li data-target=\"#carousel-example-generic\" data-slide-to=\"0\" class=\"active\"></li>
                                <li data-target=\"#carousel-example-generic\" data-slide-to=\"1\"></li>
                                <li data-target=\"#carousel-example-generic\" data-slide-to=\"2\"></li>
                            </ol>
                            <div class=\"carousel-inner\">
                                <div class=\"item active\">
                                    <img class=\"slide-image\"
                                         src=\"";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/film/images/CREATE-Film-Festival-800x300.jpg"), "html", null, true);
        echo "\"
                                         alt=\"\">
                                </div>
                                <div class=\"item\">
                                    <img class=\"slide-image\"
                                         src=\"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/film/images/horror_true_movies_1-800x300.jpg"), "html", null, true);
        echo "\"
                                         alt=\"\">
                                </div>
                                <div class=\"item\">
                                    <img class=\"slide-image\"
                                         src=\"";
        // line 152
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bundles/film/images/assassins-creed-810x300.jpg"), "html", null, true);
        echo "\" alt=\"\">
                                </div>
                            </div>
                            <a class=\"left carousel-control\" href=\"#carousel-example-generic\" data-slide=\"prev\">
                                <span class=\"glyphicon glyphicon-chevron-left\"></span>
                            </a>
                            <a class=\"right carousel-control\" href=\"#carousel-example-generic\" data-slide=\"next\">
                                <span class=\"glyphicon glyphicon-chevron-right\"></span>
                            </a>
                        </div>
                    </div>

                </div>-->
                <div></div>
                <div class=\"row\">

                    ";
        // line 168
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["films"]) ? $context["films"] : $this->getContext($context, "films")));
        foreach ($context['_seq'] as $context["_key"] => $context["film"]) {
            // line 169
            echo "                        <div class=\"col-sm-4 col-lg-4 col-md-4\">
                            <div class=\"thumbnail\">
                                <img src=\"";
            // line 171
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl(("uploads/" . $this->getAttribute($context["film"], "poster", array()))), "html", null, true);
            echo "\" alt=\"\">
                                <div class=\"caption\">
                                    <h4 class=\"pull-right\">64.99\$</h4>
                                    <h4><a href=\"#\">";
            // line 174
            echo twig_escape_filter($this->env, $this->getAttribute($context["film"], "titre", array()), "html", null, true);
            echo "</a>
                                    </h4>
                                    <p>";
            // line 176
            echo twig_escape_filter($this->env, $this->getAttribute($context["film"], "description", array()), "html", null, true);
            echo "</p>
                                    <p>";
            // line 177
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["film"], "categorie", array()), "nom", array()), "html", null, true);
            echo "</p>

                                </div>
                                <div class=\"ratings\">
                                    <p class=\"pull-right\">12 vues</p>
                                    <p>
                                        <span class=\"glyphicon glyphicon-star\"></span>
                                        <span class=\"glyphicon glyphicon-star\"></span>
                                        <span class=\"glyphicon glyphicon-star\"></span>
                                        <span class=\"glyphicon glyphicon-star\"></span>
                                        <span class=\"glyphicon glyphicon-star-empty\"></span>
                                    </p>
                                </div>
                            </div>
                        </div>

                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['film'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 194
        echo "                </div>
                <div class=\"navigation\">
                    ";
        // line 196
        echo $this->env->getExtension('knp_pagination')->render($this->env, (isset($context["films"]) ? $context["films"] : $this->getContext($context, "films")));
        echo "
                </div>

            </div>

        </div>
    ";
        
        $__internal_6c747fe5ac97d014c9049c7b7fe069681e30767130694d7623d1f9e25445d13f->leave($__internal_6c747fe5ac97d014c9049c7b7fe069681e30767130694d7623d1f9e25445d13f_prof);

    }

    public function getTemplateName()
    {
        return "@Film/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  384 => 196,  380 => 194,  357 => 177,  353 => 176,  348 => 174,  342 => 171,  338 => 169,  334 => 168,  315 => 152,  307 => 147,  299 => 142,  270 => 116,  265 => 113,  254 => 111,  250 => 110,  243 => 105,  237 => 104,  221 => 229,  217 => 228,  211 => 225,  207 => 224,  203 => 223,  181 => 203,  179 => 104,  158 => 86,  150 => 80,  142 => 78,  136 => 75,  132 => 74,  127 => 73,  125 => 72,  117 => 67,  111 => 64,  105 => 61,  99 => 58,  87 => 49,  58 => 23,  54 => 22,  48 => 19,  44 => 18,  40 => 17,  23 => 2,);
    }
}
/* {# empty Twig template #}*/
/* */
/* <!DOCTYPE html>*/
/* <html lang="en">*/
/* */
/* <head>*/
/* */
/*     <meta charset="utf-8">*/
/*     <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*     <meta name="description" content="">*/
/*     <meta name="author" content="">*/
/* */
/*     <title>Shop Homepage - Start Bootstrap Template</title>*/
/* */
/*     <!-- Bootstrap Core CSS -->*/
/* 	<link href="{{ asset('bundles/film/css/bootsrap-theme.css') }}" rel="stylesheet">*/
/*     <link href="{{ asset('bundles/film/css/bootstrap.min.css') }}" rel="stylesheet">*/
/*     <link href="{{asset('bundles/film/css/jquery-ui.css')}}" rel="stylesheet">*/
/* */
/*     <!-- Custom CSS -->*/
/*     <link href="{{ asset('bundles/film/css/shop-homepage.css') }}" rel="stylesheet">*/
/*     <!--<link href="{{ asset('bundles/film/css/normalize.css') }}" rel="stylesheet">-->*/
/* */
/* */
/*     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->*/
/*     <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->*/
/*     <!--[if lt IE 9]>*/
/*     <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>*/
/*     <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>*/
/*     <![endif]-->*/
/* */
/* </head>*/
/* */
/* <body>*/
/* */
/* <!-- Navigation -->*/
/* <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">*/
/*     <div class="container">*/
/*         <!-- Brand and toggle get grouped for better mobile display -->*/
/*         <div class="navbar-header">*/
/*             <button type="button" class="navbar-toggle" data-toggle="collapse"*/
/*                     data-target="#bs-example-navbar-collapse-1">*/
/*                 <span class="sr-only">Toggle navigation</span>*/
/*                 <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*             </button>*/
/*             <a class="navbar-brand" href="#"><img src="{{ asset('bundles/film/images/logo.png') }}" alt="Filmothèque"*/
/*                                                   width="35" height="35" class="moimg"/>*/
/*                 <div class="moh1"> Film</div>*/
/*             </a>*/
/*         </div>*/
/*         <!-- Collect the nav links, forms, and other content for toggling -->*/
/*         <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">*/
/*             <ul class="nav navbar-nav">*/
/*                 <li>*/
/*                     <a href="{{ path('film_homepage') }}">Accueil</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="{{ path('affich_act') }}">Acteurs</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="{{ path('film_index') }}">Films</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="{{ path('category_index') }}">Categories</a>*/
/*                 </li>*/
/*             </ul>*/
/*             <ul class="nav navbar-nav navbar-right">*/
/*                 <li>*/
/*                     {% if is_granted("IS_AUTHENTICATED_REMEMBERED") %}*/
/*                         {{ 'layout.logged_in_as'|trans({'%username%': app.user.username}, 'FOSUserBundle') }}*/
/*                         <a href="{{ path('fos_user_security_logout') }}">*/
/*                             {{ 'layout.logout'|trans({}, 'FOSUserBundle') }}*/
/*                         </a>*/
/*                     {% else %}*/
/*                         <a href="{{ path('fos_user_security_login') }}">{{ 'layout.login'|trans({}, 'FOSUserBundle') }}</a>*/
/*                     {% endif %}*/
/*                 </li>*/
/*                 <li>*/
/*                     */
/*                     <script>*/
/*                         */
/*                         function subm() {*/
/*                             if ($("#f input[class=sear]:checked").val() == 1) {$('#f').attr('action', "{{ path('film_search') }}")};*/
/*                         }*/
/*                     </script>*/
/*                     <form id="f" method="get" onsubmit="subm()">*/
/*                         <input type="text" autocomplete="off" placeholder="Search..." name="motcle" id="recherche">*/
/*                         <input type="radio" class="sear" value="1" checked/>Films */
/*                         <input type="radio" class="sear" value="2"/>Acteurs  */
/*                         <input type="radio" class="sear" value="3"/>Categories  */
/*                     </form>*/
/*                 </li>*/
/*         </div>*/
/*         <!-- /.navbar-collapse -->*/
/*     </div>*/
/*     <!-- /.container -->*/
/* </nav>*/
/*                             */
/* <!-- Page Content -->*/
/* <div class="container">*/
/*     {% block A %}*/
/*         <div class="row">*/
/* */
/*             <div class="col-md-3">*/
/*                 <p class="lead">Listes Des Categories</p>*/
/*                 <div class="list-group">*/
/*                     {% for cat in categories %}*/
/*                         <a href="{{ path('film_homepage', {'id':cat.id}) }}" class="list-group-item">{{ cat.nom }}</a>*/
/*                     {% endfor %}*/
/*                 </div>*/
/*                 */
/*                     <div class="well">*/
/*                         <form method="get" action="{{ path('film_search') }}">*/
/*                             <fieldset>*/
/*                                 <div class="form-group-sm">*/
/*                                 <label for="motcle">Search:</label>*/
/*                                 <input type="text" placeholder="Search..." name="motcle" autocomplete="off" id="typeahead">*/
/*                                 </div>*/
/*                                 <button type="submit" class="btn btn-primary btn-xs" data>Search</button>*/
/*                             </fieldset>*/
/*                         </form>*/
/*                     </div>*/
/*             </div>*/
/* */
/*             <div class="col-md-9">*/
/* */
/*                 <!--<div class="row carousel-holder">*/
/* */
/*                     <div class="col-md-12">*/
/*                         <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">*/
/*                             <ol class="carousel-indicators">*/
/*                                 <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>*/
/*                                 <li data-target="#carousel-example-generic" data-slide-to="1"></li>*/
/*                                 <li data-target="#carousel-example-generic" data-slide-to="2"></li>*/
/*                             </ol>*/
/*                             <div class="carousel-inner">*/
/*                                 <div class="item active">*/
/*                                     <img class="slide-image"*/
/*                                          src="{{ asset('bundles/film/images/CREATE-Film-Festival-800x300.jpg') }}"*/
/*                                          alt="">*/
/*                                 </div>*/
/*                                 <div class="item">*/
/*                                     <img class="slide-image"*/
/*                                          src="{{ asset('bundles/film/images/horror_true_movies_1-800x300.jpg') }}"*/
/*                                          alt="">*/
/*                                 </div>*/
/*                                 <div class="item">*/
/*                                     <img class="slide-image"*/
/*                                          src="{{ asset('bundles/film/images/assassins-creed-810x300.jpg') }}" alt="">*/
/*                                 </div>*/
/*                             </div>*/
/*                             <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">*/
/*                                 <span class="glyphicon glyphicon-chevron-left"></span>*/
/*                             </a>*/
/*                             <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">*/
/*                                 <span class="glyphicon glyphicon-chevron-right"></span>*/
/*                             </a>*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                 </div>-->*/
/*                 <div></div>*/
/*                 <div class="row">*/
/* */
/*                     {% for film in films %}*/
/*                         <div class="col-sm-4 col-lg-4 col-md-4">*/
/*                             <div class="thumbnail">*/
/*                                 <img src="{{ asset('uploads/'~film.poster) }}" alt="">*/
/*                                 <div class="caption">*/
/*                                     <h4 class="pull-right">64.99$</h4>*/
/*                                     <h4><a href="#">{{ film.titre }}</a>*/
/*                                     </h4>*/
/*                                     <p>{{ film.description }}</p>*/
/*                                     <p>{{ film.categorie.nom }}</p>*/
/* */
/*                                 </div>*/
/*                                 <div class="ratings">*/
/*                                     <p class="pull-right">12 vues</p>*/
/*                                     <p>*/
/*                                         <span class="glyphicon glyphicon-star"></span>*/
/*                                         <span class="glyphicon glyphicon-star"></span>*/
/*                                         <span class="glyphicon glyphicon-star"></span>*/
/*                                         <span class="glyphicon glyphicon-star"></span>*/
/*                                         <span class="glyphicon glyphicon-star-empty"></span>*/
/*                                     </p>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                     {% endfor %}*/
/*                 </div>*/
/*                 <div class="navigation">*/
/*                     {{ knp_pagination_render(films) }}*/
/*                 </div>*/
/* */
/*             </div>*/
/* */
/*         </div>*/
/*     {% endblock %}*/
/* </div>*/
/* <!-- /.container -->*/
/* */
/* <div class="container">*/
/* */
/*     <hr>*/
/* */
/*     <!-- Footer -->*/
/*     <footer>*/
/*         <div class="row">*/
/*             <div class="col-lg-12">*/
/*                 <p>Copyright &copy; Your Website 2014</p>*/
/*             </div>*/
/*         </div>*/
/*     </footer>*/
/* */
/* </div>*/
/* <!-- /.container -->*/
/* */
/* <!-- jQuery -->*/
/*   <script src="{{ asset('bundles/film/js/jquery.js') }}"></script>*/
/*   <script src="{{ asset('bundles/film/js/jquery-ui.js') }}"></script>*/
/*   <script src="{{ asset('bundles/film/js/typeahead.bundle.js') }}"></script>*/
/*   */
/* <!-- Bootstrap Core JavaScript -->*/
/* <script src="{{ asset('bundles/film/js/bootstrap.min.js') }}"></script>*/
/* <script src="{{ asset('bundles/film/js/searchJSON.js') }}"></script>*/
/* */
/* */
/* */
/* */
/* </body>*/
/* */
/* </html>*/
/* */
