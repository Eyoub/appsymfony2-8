<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_d1668ca60e78dd36cd210df67b12b5d21193c655287f1aa3343b4e7439f9b37f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9a682ca24c1e71967802514c59e1f57b23e490af17c30f4d715d1bf7584a4cfa = $this->env->getExtension("native_profiler");
        $__internal_9a682ca24c1e71967802514c59e1f57b23e490af17c30f4d715d1bf7584a4cfa->enter($__internal_9a682ca24c1e71967802514c59e1f57b23e490af17c30f4d715d1bf7584a4cfa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_9a682ca24c1e71967802514c59e1f57b23e490af17c30f4d715d1bf7584a4cfa->leave($__internal_9a682ca24c1e71967802514c59e1f57b23e490af17c30f4d715d1bf7584a4cfa_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'email')) ?>*/
/* */
