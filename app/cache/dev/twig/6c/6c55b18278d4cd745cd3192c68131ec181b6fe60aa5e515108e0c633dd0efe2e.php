<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_01f396ae3023b4ea6e3b4358827c57465c166ee6b02aeb6ca8049e6726271889 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aafb0855da10ae48169b2189ec0f58823b245e518d6717466ebf3074dbd53cd9 = $this->env->getExtension("native_profiler");
        $__internal_aafb0855da10ae48169b2189ec0f58823b245e518d6717466ebf3074dbd53cd9->enter($__internal_aafb0855da10ae48169b2189ec0f58823b245e518d6717466ebf3074dbd53cd9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_aafb0855da10ae48169b2189ec0f58823b245e518d6717466ebf3074dbd53cd9->leave($__internal_aafb0855da10ae48169b2189ec0f58823b245e518d6717466ebf3074dbd53cd9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'url')) ?>*/
/* */
