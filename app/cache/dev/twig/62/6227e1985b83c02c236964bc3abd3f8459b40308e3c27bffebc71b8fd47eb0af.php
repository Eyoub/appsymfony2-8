<?php

/* @Film/Acteur/modifActeur.html.twig */
class __TwigTemplate_ea07d724643d7ab38614c48fa61a0875577f00e67e7b71267f9ecd560aeef671 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("FilmBundle::index.html.twig", "@Film/Acteur/modifActeur.html.twig", 2);
        $this->blocks = array(
            'A' => array($this, 'block_A'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FilmBundle::index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_31466711e1855ce811c904470443873722498b4c2090be0592ea480bd4804a15 = $this->env->getExtension("native_profiler");
        $__internal_31466711e1855ce811c904470443873722498b4c2090be0592ea480bd4804a15->enter($__internal_31466711e1855ce811c904470443873722498b4c2090be0592ea480bd4804a15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Film/Acteur/modifActeur.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_31466711e1855ce811c904470443873722498b4c2090be0592ea480bd4804a15->leave($__internal_31466711e1855ce811c904470443873722498b4c2090be0592ea480bd4804a15_prof);

    }

    // line 3
    public function block_A($context, array $blocks = array())
    {
        $__internal_fe36b5e836e7b02a03f811425c484d3e5ce1f8a2f8610aedb38d6df12fe3fba4 = $this->env->getExtension("native_profiler");
        $__internal_fe36b5e836e7b02a03f811425c484d3e5ce1f8a2f8610aedb38d6df12fe3fba4->enter($__internal_fe36b5e836e7b02a03f811425c484d3e5ce1f8a2f8610aedb38d6df12fe3fba4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "A"));

        // line 4
        echo "    
    <div class=\"row\">

        <div class=\"col-md-3\">
            <p class=\"lead\">Gestion des acteurs</p>
            <div class=\"list-group\">
                <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("affich_act");
        echo "\" class=\"list-group-item\">Afficher</a>
                <a href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("ajout_act");
        echo "\" class=\"list-group-item\">Ajout</a>
                <a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("modif_act2");
        echo "\" class=\"list-group-item\">Editer</a>
            </div>
        </div>
            <div class=\"col-md-9\">
<form class=\"well\" method=\"POST\">
    
    ";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
    <input type=\"submit\" value=\"envoyer\"/>    
</form>
            </div>
";
        
        $__internal_fe36b5e836e7b02a03f811425c484d3e5ce1f8a2f8610aedb38d6df12fe3fba4->leave($__internal_fe36b5e836e7b02a03f811425c484d3e5ce1f8a2f8610aedb38d6df12fe3fba4_prof);

    }

    public function getTemplateName()
    {
        return "@Film/Acteur/modifActeur.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 18,  56 => 12,  52 => 11,  48 => 10,  40 => 4,  34 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends "FilmBundle::index.html.twig" %}*/
/* {% block A %}*/
/*     */
/*     <div class="row">*/
/* */
/*         <div class="col-md-3">*/
/*             <p class="lead">Gestion des acteurs</p>*/
/*             <div class="list-group">*/
/*                 <a href="{{path('affich_act')}}" class="list-group-item">Afficher</a>*/
/*                 <a href="{{path('ajout_act')}}" class="list-group-item">Ajout</a>*/
/*                 <a href="{{path('modif_act2')}}" class="list-group-item">Editer</a>*/
/*             </div>*/
/*         </div>*/
/*             <div class="col-md-9">*/
/* <form class="well" method="POST">*/
/*     */
/*     {{form_widget(form)}}*/
/*     <input type="submit" value="envoyer"/>    */
/* </form>*/
/*             </div>*/
/* {% endblock %}*/
