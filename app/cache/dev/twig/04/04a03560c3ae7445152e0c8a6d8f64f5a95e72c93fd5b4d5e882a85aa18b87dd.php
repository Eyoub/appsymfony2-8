<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_842f8db065886419165d1a13f67ef3a5a7d83bd31d73314afad66fa2df8e7ac0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7701b9918e03be9a9e4823d278d39b3d3edbfff025098f5af9ca898399db33b5 = $this->env->getExtension("native_profiler");
        $__internal_7701b9918e03be9a9e4823d278d39b3d3edbfff025098f5af9ca898399db33b5->enter($__internal_7701b9918e03be9a9e4823d278d39b3d3edbfff025098f5af9ca898399db33b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_7701b9918e03be9a9e4823d278d39b3d3edbfff025098f5af9ca898399db33b5->leave($__internal_7701b9918e03be9a9e4823d278d39b3d3edbfff025098f5af9ca898399db33b5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($form->vars['multipart']): ?>enctype="multipart/form-data"<?php endif ?>*/
/* */
