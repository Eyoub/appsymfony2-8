<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_4327e614b487025826512b5d1563fcf774aebb399dce8fddee3f309823d142db extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_03f46ccd41397064e82ab43c1073650e9833b10c8d19aaaf657659b512e9c8f8 = $this->env->getExtension("native_profiler");
        $__internal_03f46ccd41397064e82ab43c1073650e9833b10c8d19aaaf657659b512e9c8f8->enter($__internal_03f46ccd41397064e82ab43c1073650e9833b10c8d19aaaf657659b512e9c8f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_03f46ccd41397064e82ab43c1073650e9833b10c8d19aaaf657659b512e9c8f8->leave($__internal_03f46ccd41397064e82ab43c1073650e9833b10c8d19aaaf657659b512e9c8f8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_container_attributes') ?>*/
/* */
