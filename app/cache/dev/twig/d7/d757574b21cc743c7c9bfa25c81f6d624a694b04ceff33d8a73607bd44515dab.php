<?php

/* FOSUserBundle:ChangePassword:change_password.html.twig */
class __TwigTemplate_fec2e4832e25d2e19be0b3cc4912e632bc50db89a1ab23301286370db47c2687 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_639d06da086e4dac64103e815b04f9419d6b2e97f4b8e4e98241fad7c77c5026 = $this->env->getExtension("native_profiler");
        $__internal_639d06da086e4dac64103e815b04f9419d6b2e97f4b8e4e98241fad7c77c5026->enter($__internal_639d06da086e4dac64103e815b04f9419d6b2e97f4b8e4e98241fad7c77c5026_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_639d06da086e4dac64103e815b04f9419d6b2e97f4b8e4e98241fad7c77c5026->leave($__internal_639d06da086e4dac64103e815b04f9419d6b2e97f4b8e4e98241fad7c77c5026_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_3049eb0d2e28c84368210282e858d684f016fce62188d27af25185d9d810ed8b = $this->env->getExtension("native_profiler");
        $__internal_3049eb0d2e28c84368210282e858d684f016fce62188d27af25185d9d810ed8b->enter($__internal_3049eb0d2e28c84368210282e858d684f016fce62188d27af25185d9d810ed8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:ChangePassword:change_password_content.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 4)->display($context);
        
        $__internal_3049eb0d2e28c84368210282e858d684f016fce62188d27af25185d9d810ed8b->leave($__internal_3049eb0d2e28c84368210282e858d684f016fce62188d27af25185d9d810ed8b_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:ChangePassword:change_password_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
