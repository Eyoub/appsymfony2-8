<?php

/* :film:new.html.twig */
class __TwigTemplate_95974ebe25c0bac22cfc553eb59433bc2a7c7105b4bd4fd21e57a08bcf5e57ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FilmBundle::index.html.twig", ":film:new.html.twig", 1);
        $this->blocks = array(
            'A' => array($this, 'block_A'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FilmBundle::index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4a79bc80df1954ec69af9672d4e04bb8cbe10f18b35e3cfbeece997b9d742c1b = $this->env->getExtension("native_profiler");
        $__internal_4a79bc80df1954ec69af9672d4e04bb8cbe10f18b35e3cfbeece997b9d742c1b->enter($__internal_4a79bc80df1954ec69af9672d4e04bb8cbe10f18b35e3cfbeece997b9d742c1b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":film:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4a79bc80df1954ec69af9672d4e04bb8cbe10f18b35e3cfbeece997b9d742c1b->leave($__internal_4a79bc80df1954ec69af9672d4e04bb8cbe10f18b35e3cfbeece997b9d742c1b_prof);

    }

    // line 3
    public function block_A($context, array $blocks = array())
    {
        $__internal_9fe028d267395ce7775d604bab6fa317bc760e1ca7bfbdc31f9f123e406fb599 = $this->env->getExtension("native_profiler");
        $__internal_9fe028d267395ce7775d604bab6fa317bc760e1ca7bfbdc31f9f123e406fb599->enter($__internal_9fe028d267395ce7775d604bab6fa317bc760e1ca7bfbdc31f9f123e406fb599_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "A"));

        // line 4
        echo "
    <div class=\"row\">

            <div class=\"col-md-3\">
                <p class=\"lead\">Gestion des filmes</p>
                <div class=\"list-group\">
                    <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("film_index");
        echo "\" class=\"list-group-item\">lister</a>
                    <a href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("film_new");
        echo "\" class=\"list-group-item\">creer</a>
                    <a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("film_editer");
        echo "\" class=\"list-group-item\">editer</a>
                </div>
            </div>
<div class=\"col-md-9\">

    ";
        // line 17
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 20
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
</div>
</div>
";
        
        $__internal_9fe028d267395ce7775d604bab6fa317bc760e1ca7bfbdc31f9f123e406fb599->leave($__internal_9fe028d267395ce7775d604bab6fa317bc760e1ca7bfbdc31f9f123e406fb599_prof);

    }

    public function getTemplateName()
    {
        return ":film:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 20,  68 => 18,  64 => 17,  56 => 12,  52 => 11,  48 => 10,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'FilmBundle::index.html.twig' %}*/
/* */
/* {% block A %}*/
/* */
/*     <div class="row">*/
/* */
/*             <div class="col-md-3">*/
/*                 <p class="lead">Gestion des filmes</p>*/
/*                 <div class="list-group">*/
/*                     <a href="{{path('film_index')}}" class="list-group-item">lister</a>*/
/*                     <a href="{{path('film_new')}}" class="list-group-item">creer</a>*/
/*                     <a href="{{path('film_editer')}}" class="list-group-item">editer</a>*/
/*                 </div>*/
/*             </div>*/
/* <div class="col-md-9">*/
/* */
/*     {{ form_start(form) }}*/
/*         {{ form_widget(form) }}*/
/*         <input type="submit" value="Create" />*/
/*     {{ form_end(form) }}*/
/* </div>*/
/* </div>*/
/* {% endblock %}*/
/* */
