<?php

/* FOSUserBundle:Resetting:check_email.html.twig */
class __TwigTemplate_44fb1441e5ad5cf61e427e4e3aa051c22bfab564e08efde95390b80e40073355 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Resetting:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_75ab5de903337daf93d6ba257b03e251ca90d42e1ea57517bdc6020ee11fd037 = $this->env->getExtension("native_profiler");
        $__internal_75ab5de903337daf93d6ba257b03e251ca90d42e1ea57517bdc6020ee11fd037->enter($__internal_75ab5de903337daf93d6ba257b03e251ca90d42e1ea57517bdc6020ee11fd037_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_75ab5de903337daf93d6ba257b03e251ca90d42e1ea57517bdc6020ee11fd037->leave($__internal_75ab5de903337daf93d6ba257b03e251ca90d42e1ea57517bdc6020ee11fd037_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_79d7c984d80b2b7db529fe7d1a98639849fe641b2f79e5e81d9cdadbbb101c44 = $this->env->getExtension("native_profiler");
        $__internal_79d7c984d80b2b7db529fe7d1a98639849fe641b2f79e5e81d9cdadbbb101c44->enter($__internal_79d7c984d80b2b7db529fe7d1a98639849fe641b2f79e5e81d9cdadbbb101c44_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.check_email", array("%email%" => (isset($context["email"]) ? $context["email"] : $this->getContext($context, "email"))), "FOSUserBundle"), "html", null, true);
        echo "
</p>
";
        
        $__internal_79d7c984d80b2b7db529fe7d1a98639849fe641b2f79e5e81d9cdadbbb101c44->leave($__internal_79d7c984d80b2b7db529fe7d1a98639849fe641b2f79e5e81d9cdadbbb101c44_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 7,  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/* <p>*/
/* {{ 'resetting.check_email'|trans({'%email%': email}) }}*/
/* </p>*/
/* {% endblock %}*/
/* */
