<?php

/* film/new.html.twig */
class __TwigTemplate_214d71166ffda1636b750b032de7c5c6460035f6b16fe8437c44b017d4d5e6eb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FilmBundle::index.html.twig", "film/new.html.twig", 1);
        $this->blocks = array(
            'A' => array($this, 'block_A'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FilmBundle::index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6e17a4bcd66f12b56246c82d32082d7ea9eafd0acd0033a144a0854f5e661162 = $this->env->getExtension("native_profiler");
        $__internal_6e17a4bcd66f12b56246c82d32082d7ea9eafd0acd0033a144a0854f5e661162->enter($__internal_6e17a4bcd66f12b56246c82d32082d7ea9eafd0acd0033a144a0854f5e661162_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "film/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6e17a4bcd66f12b56246c82d32082d7ea9eafd0acd0033a144a0854f5e661162->leave($__internal_6e17a4bcd66f12b56246c82d32082d7ea9eafd0acd0033a144a0854f5e661162_prof);

    }

    // line 3
    public function block_A($context, array $blocks = array())
    {
        $__internal_3ed2a5b1061075f8f77eb12ddb6cf314757cf920b38ca8880f316bbb957bb195 = $this->env->getExtension("native_profiler");
        $__internal_3ed2a5b1061075f8f77eb12ddb6cf314757cf920b38ca8880f316bbb957bb195->enter($__internal_3ed2a5b1061075f8f77eb12ddb6cf314757cf920b38ca8880f316bbb957bb195_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "A"));

        // line 4
        echo "
    <div class=\"row\">

            <div class=\"col-md-3\">
                <p class=\"lead\">Gestion des filmes</p>
                <div class=\"list-group\">
                    <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("film_index");
        echo "\" class=\"list-group-item\">lister</a>
                    <a href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("film_new");
        echo "\" class=\"list-group-item\">creer</a>
                    <a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("film_editer");
        echo "\" class=\"list-group-item\">editer</a>
                </div>
            </div>
<div class=\"col-md-9\">

    ";
        // line 17
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 18
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 20
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
</div>
</div>
";
        
        $__internal_3ed2a5b1061075f8f77eb12ddb6cf314757cf920b38ca8880f316bbb957bb195->leave($__internal_3ed2a5b1061075f8f77eb12ddb6cf314757cf920b38ca8880f316bbb957bb195_prof);

    }

    public function getTemplateName()
    {
        return "film/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 20,  68 => 18,  64 => 17,  56 => 12,  52 => 11,  48 => 10,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'FilmBundle::index.html.twig' %}*/
/* */
/* {% block A %}*/
/* */
/*     <div class="row">*/
/* */
/*             <div class="col-md-3">*/
/*                 <p class="lead">Gestion des filmes</p>*/
/*                 <div class="list-group">*/
/*                     <a href="{{path('film_index')}}" class="list-group-item">lister</a>*/
/*                     <a href="{{path('film_new')}}" class="list-group-item">creer</a>*/
/*                     <a href="{{path('film_editer')}}" class="list-group-item">editer</a>*/
/*                 </div>*/
/*             </div>*/
/* <div class="col-md-9">*/
/* */
/*     {{ form_start(form) }}*/
/*         {{ form_widget(form) }}*/
/*         <input type="submit" value="Create" />*/
/*     {{ form_end(form) }}*/
/* </div>*/
/* </div>*/
/* {% endblock %}*/
/* */
