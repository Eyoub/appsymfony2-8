<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class __TwigTemplate_933185314e010dd60ad307bf4111cac934f23ecc8eca9829fd7f3e3a5f9dafb3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c73090940693d79b7d070aa3630f6eb7a24783742664658b2331daea966da01d = $this->env->getExtension("native_profiler");
        $__internal_c73090940693d79b7d070aa3630f6eb7a24783742664658b2331daea966da01d->enter($__internal_c73090940693d79b7d070aa3630f6eb7a24783742664658b2331daea966da01d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c73090940693d79b7d070aa3630f6eb7a24783742664658b2331daea966da01d->leave($__internal_c73090940693d79b7d070aa3630f6eb7a24783742664658b2331daea966da01d_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_fb5d5b04e5b88612c6772ca8c8aa34e3098bc36606e0bdc4e454d68d8a915173 = $this->env->getExtension("native_profiler");
        $__internal_fb5d5b04e5b88612c6772ca8c8aa34e3098bc36606e0bdc4e454d68d8a915173->enter($__internal_fb5d5b04e5b88612c6772ca8c8aa34e3098bc36606e0bdc4e454d68d8a915173_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.confirmed", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if ((isset($context["targetUrl"]) ? $context["targetUrl"] : $this->getContext($context, "targetUrl"))) {
            // line 8
            echo "    <p><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["targetUrl"]) ? $context["targetUrl"] : $this->getContext($context, "targetUrl")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        
        $__internal_fb5d5b04e5b88612c6772ca8c8aa34e3098bc36606e0bdc4e454d68d8a915173->leave($__internal_fb5d5b04e5b88612c6772ca8c8aa34e3098bc36606e0bdc4e454d68d8a915173_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 8,  45 => 7,  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/*     <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>*/
/*     {% if targetUrl %}*/
/*     <p><a href="{{ targetUrl }}">{{ 'registration.back'|trans }}</a></p>*/
/*     {% endif %}*/
/* {% endblock fos_user_content %}*/
/* */
