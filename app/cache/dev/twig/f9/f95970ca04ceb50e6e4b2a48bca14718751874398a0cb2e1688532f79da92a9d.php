<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_8455f7052d1245d88a0dd4c415cdf1189af1d98f8dba6402e49a8a81ba85abf4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b7f6b955a969ccb60b3f8992d8e741a1caf7442d845adbff18e8b0e455f7ad52 = $this->env->getExtension("native_profiler");
        $__internal_b7f6b955a969ccb60b3f8992d8e741a1caf7442d845adbff18e8b0e455f7ad52->enter($__internal_b7f6b955a969ccb60b3f8992d8e741a1caf7442d845adbff18e8b0e455f7ad52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_b7f6b955a969ccb60b3f8992d8e741a1caf7442d845adbff18e8b0e455f7ad52->leave($__internal_b7f6b955a969ccb60b3f8992d8e741a1caf7442d845adbff18e8b0e455f7ad52_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'hidden')) ?>*/
/* */
