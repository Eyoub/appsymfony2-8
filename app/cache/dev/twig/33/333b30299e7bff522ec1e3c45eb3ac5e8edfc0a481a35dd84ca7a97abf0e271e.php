<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_f39afe4d51b813a15415e60df7b25f43a5efa69d87ed234915914063dcc577ad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_175f034e14db49615dc70bebf5ac9542864ac4894977ae0e8b9975ee8ad68540 = $this->env->getExtension("native_profiler");
        $__internal_175f034e14db49615dc70bebf5ac9542864ac4894977ae0e8b9975ee8ad68540->enter($__internal_175f034e14db49615dc70bebf5ac9542864ac4894977ae0e8b9975ee8ad68540_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_175f034e14db49615dc70bebf5ac9542864ac4894977ae0e8b9975ee8ad68540->leave($__internal_175f034e14db49615dc70bebf5ac9542864ac4894977ae0e8b9975ee8ad68540_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_efbc991589326979a63e5bec7f5556da854c417d65e7d1c745e4f7a13d3ae9c3 = $this->env->getExtension("native_profiler");
        $__internal_efbc991589326979a63e5bec7f5556da854c417d65e7d1c745e4f7a13d3ae9c3->enter($__internal_efbc991589326979a63e5bec7f5556da854c417d65e7d1c745e4f7a13d3ae9c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Resetting:request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $__internal_efbc991589326979a63e5bec7f5556da854c417d65e7d1c745e4f7a13d3ae9c3->leave($__internal_efbc991589326979a63e5bec7f5556da854c417d65e7d1c745e4f7a13d3ae9c3_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Resetting:request_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
