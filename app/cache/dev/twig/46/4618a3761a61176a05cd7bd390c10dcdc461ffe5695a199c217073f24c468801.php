<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_32936bb9e46cd3c5f95e623912a7b34c8b0d4b69584f6b655c199d9e65ee11b2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e89b26d8b2fe231c8a5e6ae42ebf54db2bac0e0ec48945d22dcaf5699aedcb84 = $this->env->getExtension("native_profiler");
        $__internal_e89b26d8b2fe231c8a5e6ae42ebf54db2bac0e0ec48945d22dcaf5699aedcb84->enter($__internal_e89b26d8b2fe231c8a5e6ae42ebf54db2bac0e0ec48945d22dcaf5699aedcb84_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_e89b26d8b2fe231c8a5e6ae42ebf54db2bac0e0ec48945d22dcaf5699aedcb84->leave($__internal_e89b26d8b2fe231c8a5e6ae42ebf54db2bac0e0ec48945d22dcaf5699aedcb84_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($expanded): ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_expanded') ?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_collapsed') ?>*/
/* <?php endif ?>*/
/* */
