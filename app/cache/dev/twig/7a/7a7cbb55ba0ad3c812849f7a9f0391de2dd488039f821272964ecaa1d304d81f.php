<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_4c86ed95720d2250baa37a68b304d4c2fcedd1703af2eeb6c9561afc18b4b231 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cda0fcd34a860485b8818028ff7edf008b74038f029b330f0e1756af3ba7b76c = $this->env->getExtension("native_profiler");
        $__internal_cda0fcd34a860485b8818028ff7edf008b74038f029b330f0e1756af3ba7b76c->enter($__internal_cda0fcd34a860485b8818028ff7edf008b74038f029b330f0e1756af3ba7b76c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_cda0fcd34a860485b8818028ff7edf008b74038f029b330f0e1756af3ba7b76c->leave($__internal_cda0fcd34a860485b8818028ff7edf008b74038f029b330f0e1756af3ba7b76c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (isset($prototype)): ?>*/
/*     <?php $attr['data-prototype'] = $view->escape($view['form']->row($prototype)) ?>*/
/* <?php endif ?>*/
/* <?php echo $view['form']->widget($form, array('attr' => $attr)) ?>*/
/* */
