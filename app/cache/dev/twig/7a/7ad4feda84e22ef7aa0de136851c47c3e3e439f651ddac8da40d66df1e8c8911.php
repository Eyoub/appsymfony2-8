<?php

/* @Film/Categories/index.html.twig */
class __TwigTemplate_00db5f75dbc683dda2ec5ae421cc8dc1dc158dc24862d362dd223b1517a1d6b4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("FilmBundle::index.html.twig", "@Film/Categories/index.html.twig", 2);
        $this->blocks = array(
            'A' => array($this, 'block_A'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FilmBundle::index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a301803dae47e4969a1bab7876f6f6b0a01030b8d695df281b2a164a0a74663e = $this->env->getExtension("native_profiler");
        $__internal_a301803dae47e4969a1bab7876f6f6b0a01030b8d695df281b2a164a0a74663e->enter($__internal_a301803dae47e4969a1bab7876f6f6b0a01030b8d695df281b2a164a0a74663e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Film/Categories/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a301803dae47e4969a1bab7876f6f6b0a01030b8d695df281b2a164a0a74663e->leave($__internal_a301803dae47e4969a1bab7876f6f6b0a01030b8d695df281b2a164a0a74663e_prof);

    }

    // line 3
    public function block_A($context, array $blocks = array())
    {
        $__internal_d42be0d2832fb6546bd61577db1f0ab02495e758607c3fe75d0b51a80693bc3b = $this->env->getExtension("native_profiler");
        $__internal_d42be0d2832fb6546bd61577db1f0ab02495e758607c3fe75d0b51a80693bc3b->enter($__internal_d42be0d2832fb6546bd61577db1f0ab02495e758607c3fe75d0b51a80693bc3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "A"));

        // line 4
        echo "
<div class=\"row\">

            <div class=\"col-md-3\">
                <p class=\"lead\">Gestion des categories</p>
                <div class=\"list-group\">
                    <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("category_index");
        echo "\" class=\"list-group-item\">lister</a>
                    <a href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("category_new");
        echo "\" class=\"list-group-item\">creer</a>
                </div>
            </div>
<div class=\"col-md-9\">
<table class=\"table table-striped\">
<thead>
\t<tr><th>Id</th><th>Nom</th></tr>
</thead>

<tbody>

";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["cat"]) ? $context["cat"] : $this->getContext($context, "cat")));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 23
            echo "\t<tr>
 <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "id", array()), "html", null, true);
            echo "</td>
 <td>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "nom", array()), "html", null, true);
            echo "</td>
 </tr>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "
</tbody>
</table>
</div>
</div>
";
        
        $__internal_d42be0d2832fb6546bd61577db1f0ab02495e758607c3fe75d0b51a80693bc3b->leave($__internal_d42be0d2832fb6546bd61577db1f0ab02495e758607c3fe75d0b51a80693bc3b_prof);

    }

    public function getTemplateName()
    {
        return "@Film/Categories/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 28,  77 => 25,  73 => 24,  70 => 23,  66 => 22,  52 => 11,  48 => 10,  40 => 4,  34 => 3,  11 => 2,);
    }
}
/* {# empty Twig template #}*/
/* {% extends 'FilmBundle::index.html.twig' %}*/
/* {% block A %}*/
/* */
/* <div class="row">*/
/* */
/*             <div class="col-md-3">*/
/*                 <p class="lead">Gestion des categories</p>*/
/*                 <div class="list-group">*/
/*                     <a href="{{path('category_index')}}" class="list-group-item">lister</a>*/
/*                     <a href="{{path('category_new')}}" class="list-group-item">creer</a>*/
/*                 </div>*/
/*             </div>*/
/* <div class="col-md-9">*/
/* <table class="table table-striped">*/
/* <thead>*/
/* 	<tr><th>Id</th><th>Nom</th></tr>*/
/* </thead>*/
/* */
/* <tbody>*/
/* */
/* {% for c in cat %}*/
/* 	<tr>*/
/*  <td>{{c.id}}</td>*/
/*  <td>{{c.nom}}</td>*/
/*  </tr>*/
/* {% endfor %}*/
/* */
/* </tbody>*/
/* </table>*/
/* </div>*/
/* </div>*/
/* {% endblock %}*/
