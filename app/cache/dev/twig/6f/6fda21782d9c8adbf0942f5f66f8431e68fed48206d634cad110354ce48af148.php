<?php

/* @Twig/Exception/error.json.twig */
class __TwigTemplate_1e81ecdd0a4b70bc3beb37551482aa5482824ba3af074021657bd75040cd7b4a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0a9ab5be3568d171cd4cf2cda9d6dd09f1b666b9f95a8474ee67f41961aaab66 = $this->env->getExtension("native_profiler");
        $__internal_0a9ab5be3568d171cd4cf2cda9d6dd09f1b666b9f95a8474ee67f41961aaab66->enter($__internal_0a9ab5be3568d171cd4cf2cda9d6dd09f1b666b9f95a8474ee67f41961aaab66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_0a9ab5be3568d171cd4cf2cda9d6dd09f1b666b9f95a8474ee67f41961aaab66->leave($__internal_0a9ab5be3568d171cd4cf2cda9d6dd09f1b666b9f95a8474ee67f41961aaab66_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}*/
/* */
