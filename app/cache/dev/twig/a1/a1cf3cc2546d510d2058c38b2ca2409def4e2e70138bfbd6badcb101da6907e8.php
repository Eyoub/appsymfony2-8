<?php

/* @FOSUser/Group/show.html.twig */
class __TwigTemplate_8d3be6b3d8ba40aa4fbee63b7fd1772f32d91011d9b3f78fc3c487f6ba40c0be extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Group/show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_73b2cb94deeb74983122f7ea405742c7ecad311bc2bb06a3d7b337ed9e0d8e93 = $this->env->getExtension("native_profiler");
        $__internal_73b2cb94deeb74983122f7ea405742c7ecad311bc2bb06a3d7b337ed9e0d8e93->enter($__internal_73b2cb94deeb74983122f7ea405742c7ecad311bc2bb06a3d7b337ed9e0d8e93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_73b2cb94deeb74983122f7ea405742c7ecad311bc2bb06a3d7b337ed9e0d8e93->leave($__internal_73b2cb94deeb74983122f7ea405742c7ecad311bc2bb06a3d7b337ed9e0d8e93_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_5668832099c1766b3c358a5d59a1a8376b45e0e0309211c77af9d9b07df45072 = $this->env->getExtension("native_profiler");
        $__internal_5668832099c1766b3c358a5d59a1a8376b45e0e0309211c77af9d9b07df45072->enter($__internal_5668832099c1766b3c358a5d59a1a8376b45e0e0309211c77af9d9b07df45072_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:show_content.html.twig", "@FOSUser/Group/show.html.twig", 4)->display($context);
        
        $__internal_5668832099c1766b3c358a5d59a1a8376b45e0e0309211c77af9d9b07df45072->leave($__internal_5668832099c1766b3c358a5d59a1a8376b45e0e0309211c77af9d9b07df45072_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:show_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
