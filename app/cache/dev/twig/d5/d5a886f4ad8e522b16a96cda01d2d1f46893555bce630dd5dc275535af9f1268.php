<?php

/* UserBundle:Security:login.html.twig */
class __TwigTemplate_159ee2f62e51d8fad5ae8ee1f33451c640a80cd30191eece72b74510616684c7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("UserBundle::layout.html.twig", "UserBundle:Security:login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "UserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cb4624c48382a68f41f7ddd5ba13576ab09944332ea3dce073967bad305b8513 = $this->env->getExtension("native_profiler");
        $__internal_cb4624c48382a68f41f7ddd5ba13576ab09944332ea3dce073967bad305b8513->enter($__internal_cb4624c48382a68f41f7ddd5ba13576ab09944332ea3dce073967bad305b8513_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "UserBundle:Security:login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cb4624c48382a68f41f7ddd5ba13576ab09944332ea3dce073967bad305b8513->leave($__internal_cb4624c48382a68f41f7ddd5ba13576ab09944332ea3dce073967bad305b8513_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_1a49aaa7a87e9aa5f51fdb8040c3f2dad357efcb332df57c30832b202423ccbb = $this->env->getExtension("native_profiler");
        $__internal_1a49aaa7a87e9aa5f51fdb8040c3f2dad357efcb332df57c30832b202423ccbb->enter($__internal_1a49aaa7a87e9aa5f51fdb8040c3f2dad357efcb332df57c30832b202423ccbb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "FOSUserBundle:Security:login_content.html.twig");
        echo "
";
        
        $__internal_1a49aaa7a87e9aa5f51fdb8040c3f2dad357efcb332df57c30832b202423ccbb->leave($__internal_1a49aaa7a87e9aa5f51fdb8040c3f2dad357efcb332df57c30832b202423ccbb_prof);

    }

    public function getTemplateName()
    {
        return "UserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "UserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/*     {{ include('FOSUserBundle:Security:login_content.html.twig') }}*/
/* {% endblock fos_user_content %}*/
/* */
