<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_cc511a06189f97941e3e0e669b07186784417b6d9302a77036fbcf0c4b753fa8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_48ba97e9bef6e83c8cd4f65805d99f12d29dd2956def3a9589b74c569a5061f9 = $this->env->getExtension("native_profiler");
        $__internal_48ba97e9bef6e83c8cd4f65805d99f12d29dd2956def3a9589b74c569a5061f9->enter($__internal_48ba97e9bef6e83c8cd4f65805d99f12d29dd2956def3a9589b74c569a5061f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, (isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_48ba97e9bef6e83c8cd4f65805d99f12d29dd2956def3a9589b74c569a5061f9->leave($__internal_48ba97e9bef6e83c8cd4f65805d99f12d29dd2956def3a9589b74c569a5061f9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo str_replace('{{ widget }}', $view['form']->block($form, 'form_widget_simple'), $money_pattern) ?>*/
/* */
