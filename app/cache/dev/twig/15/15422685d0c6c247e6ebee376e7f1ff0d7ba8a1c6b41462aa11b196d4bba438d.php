<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_45e420463610e39270a3309c3e79d2fa472fff15d164445cdef1b9b98aca9421 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e518f6ffc5837857d8435126d530018da2a8b4f11b9bc173281482ac056cee39 = $this->env->getExtension("native_profiler");
        $__internal_e518f6ffc5837857d8435126d530018da2a8b4f11b9bc173281482ac056cee39->enter($__internal_e518f6ffc5837857d8435126d530018da2a8b4f11b9bc173281482ac056cee39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_e518f6ffc5837857d8435126d530018da2a8b4f11b9bc173281482ac056cee39->leave($__internal_e518f6ffc5837857d8435126d530018da2a8b4f11b9bc173281482ac056cee39_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'reset')) ?>*/
/* */
