<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_c5ff047c9556b3fd830d08999fa502f7a1c7320f22fdcc636f33434a815ae20b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_96a1c3e65b98d2199c8f6f5e8cff073434fc94259b75bf0df589a3c067cb96d4 = $this->env->getExtension("native_profiler");
        $__internal_96a1c3e65b98d2199c8f6f5e8cff073434fc94259b75bf0df589a3c067cb96d4->enter($__internal_96a1c3e65b98d2199c8f6f5e8cff073434fc94259b75bf0df589a3c067cb96d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_96a1c3e65b98d2199c8f6f5e8cff073434fc94259b75bf0df589a3c067cb96d4->leave($__internal_96a1c3e65b98d2199c8f6f5e8cff073434fc94259b75bf0df589a3c067cb96d4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child) : ?>*/
/*     <?php echo $view['form']->row($child) ?>*/
/* <?php endforeach; ?>*/
/* */
