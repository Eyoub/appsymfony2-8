<?php

/* film/index.html.twig */
class __TwigTemplate_6b708c19366f1b7a249eeb1a1bfa3fa939560dfc21bfb25f347301cdf14125a5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FilmBundle::index.html.twig", "film/index.html.twig", 1);
        $this->blocks = array(
            'A' => array($this, 'block_A'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FilmBundle::index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2badfd85e50c4f842a9306807d5134105455c91bd9829731e1337a74a9877a76 = $this->env->getExtension("native_profiler");
        $__internal_2badfd85e50c4f842a9306807d5134105455c91bd9829731e1337a74a9877a76->enter($__internal_2badfd85e50c4f842a9306807d5134105455c91bd9829731e1337a74a9877a76_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "film/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2badfd85e50c4f842a9306807d5134105455c91bd9829731e1337a74a9877a76->leave($__internal_2badfd85e50c4f842a9306807d5134105455c91bd9829731e1337a74a9877a76_prof);

    }

    // line 3
    public function block_A($context, array $blocks = array())
    {
        $__internal_cebb3b3858e3a6e34140d96ef339e4c7db2411ff3663bcdd21dc25658c44d360 = $this->env->getExtension("native_profiler");
        $__internal_cebb3b3858e3a6e34140d96ef339e4c7db2411ff3663bcdd21dc25658c44d360->enter($__internal_cebb3b3858e3a6e34140d96ef339e4c7db2411ff3663bcdd21dc25658c44d360_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "A"));

        // line 4
        echo "

        <div class=\"row\">

            <div class=\"col-md-3\">
                <p class=\"lead\">Gestion des filmes </p>
                <div class=\"list-group\">
                    <a href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("film_index");
        echo "\" class=\"list-group-item\">lister</a>
                    <a href=\"";
        // line 12
        echo $this->env->getExtension('routing')->getPath("film_new");
        echo "\" class=\"list-group-item\">creer</a>
                    <a href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("film_editer");
        echo "\" class=\"list-group-item\">editer</a>
                </div>
            </div>
        <div class=\"col-md-9\">
        <table class=\"table table-striped\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Titre</th>
                <th>Description</th>
                <th>acteurs</th>
                <th>categorie</th>
                <th>poster</th>

            </tr>
        </thead>
        <tbody>
        ";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["films"]) ? $context["films"] : $this->getContext($context, "films")));
        foreach ($context['_seq'] as $context["_key"] => $context["film"]) {
            // line 31
            echo "            <tr>
                <td><a href=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("film_show", array("id" => $this->getAttribute($context["film"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["film"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["film"], "titre", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["film"], "description", array()), "html", null, true);
            echo "</td>
                <td>
                ";
            // line 36
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["film"], "acteurs", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["act"]) {
                // line 37
                echo "
                ";
                // line 38
                echo twig_escape_filter($this->env, $this->getAttribute($context["act"], "nom", array()), "html", null, true);
                if ( !$this->getAttribute($context["loop"], "last", array())) {
                    echo ", ";
                }
                // line 39
                echo "                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['act'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 40
            echo "            </td>
                <td>";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["film"], "categorie", array()), "nom", array()), "html", null, true);
            echo "</td>
                <td><img src=\"";
            // line 42
            echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl(("uploads/" . $this->getAttribute($context["film"], "poster", array()))), "html", null, true);
            echo "\" alt=\"\" width=\"100\" height=\"50\"/></td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['film'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "        </tbody>
    </table>
        <div class=\"navigation\">
    ";
        // line 48
        echo $this->env->getExtension('knp_pagination')->render($this->env, (isset($context["films"]) ? $context["films"] : $this->getContext($context, "films")));
        echo "
</div>
    </div>
</div>
";
        
        $__internal_cebb3b3858e3a6e34140d96ef339e4c7db2411ff3663bcdd21dc25658c44d360->leave($__internal_cebb3b3858e3a6e34140d96ef339e4c7db2411ff3663bcdd21dc25658c44d360_prof);

    }

    public function getTemplateName()
    {
        return "film/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 48,  154 => 45,  145 => 42,  141 => 41,  138 => 40,  124 => 39,  119 => 38,  116 => 37,  99 => 36,  94 => 34,  90 => 33,  84 => 32,  81 => 31,  77 => 30,  57 => 13,  53 => 12,  49 => 11,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'FilmBundle::index.html.twig' %}*/
/* */
/* {% block A %}*/
/* */
/* */
/*         <div class="row">*/
/* */
/*             <div class="col-md-3">*/
/*                 <p class="lead">Gestion des filmes </p>*/
/*                 <div class="list-group">*/
/*                     <a href="{{path('film_index')}}" class="list-group-item">lister</a>*/
/*                     <a href="{{path('film_new')}}" class="list-group-item">creer</a>*/
/*                     <a href="{{path('film_editer')}}" class="list-group-item">editer</a>*/
/*                 </div>*/
/*             </div>*/
/*         <div class="col-md-9">*/
/*         <table class="table table-striped">*/
/*         <thead>*/
/*             <tr>*/
/*                 <th>Id</th>*/
/*                 <th>Titre</th>*/
/*                 <th>Description</th>*/
/*                 <th>acteurs</th>*/
/*                 <th>categorie</th>*/
/*                 <th>poster</th>*/
/* */
/*             </tr>*/
/*         </thead>*/
/*         <tbody>*/
/*         {% for film in films %}*/
/*             <tr>*/
/*                 <td><a href="{{ path('film_show', { 'id': film.id }) }}">{{ film.id }}</a></td>*/
/*                 <td>{{ film.titre }}</td>*/
/*                 <td>{{ film.description }}</td>*/
/*                 <td>*/
/*                 {% for act in film.acteurs %}*/
/* */
/*                 {{ act.nom }}{% if not loop.last %}, {% endif %}*/
/*                 {% endfor %}*/
/*             </td>*/
/*                 <td>{{ film.categorie.nom }}</td>*/
/*                 <td><img src="{{ asset('uploads/' ~ film.poster)}}" alt="" width="100" height="50"/></td>*/
/*             </tr>*/
/*         {% endfor %}*/
/*         </tbody>*/
/*     </table>*/
/*         <div class="navigation">*/
/*     {{ knp_pagination_render(films) }}*/
/* </div>*/
/*     </div>*/
/* </div>*/
/* {% endblock %}*/
/* */
