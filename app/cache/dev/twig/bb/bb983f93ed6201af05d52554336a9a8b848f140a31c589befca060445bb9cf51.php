<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_048b36299488a44402c1109ebcedb4708db94196a38a4ea1b01628990a3660aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_68b2fa1049ea76aa611f9711e738dea924b48e153a851c0ca651e5b757ef623f = $this->env->getExtension("native_profiler");
        $__internal_68b2fa1049ea76aa611f9711e738dea924b48e153a851c0ca651e5b757ef623f->enter($__internal_68b2fa1049ea76aa611f9711e738dea924b48e153a851c0ca651e5b757ef623f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.js.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_68b2fa1049ea76aa611f9711e738dea924b48e153a851c0ca651e5b757ef623f->leave($__internal_68b2fa1049ea76aa611f9711e738dea924b48e153a851c0ca651e5b757ef623f_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
