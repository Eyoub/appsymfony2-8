<?php

/* @FOSUser/Group/edit.html.twig */
class __TwigTemplate_19ec7f26ca4bf93e5622329512ba828b8c603116d069bc430b445eaab43dbfeb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Group/edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_171363cb7d47669cff11e0206b8c0ea882e6ceda4f8523b0d2ae741a6a34937d = $this->env->getExtension("native_profiler");
        $__internal_171363cb7d47669cff11e0206b8c0ea882e6ceda4f8523b0d2ae741a6a34937d->enter($__internal_171363cb7d47669cff11e0206b8c0ea882e6ceda4f8523b0d2ae741a6a34937d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_171363cb7d47669cff11e0206b8c0ea882e6ceda4f8523b0d2ae741a6a34937d->leave($__internal_171363cb7d47669cff11e0206b8c0ea882e6ceda4f8523b0d2ae741a6a34937d_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_5c5245eae0c1ce17e5a9da29144318be801e7f31340120bea94dce527285db9f = $this->env->getExtension("native_profiler");
        $__internal_5c5245eae0c1ce17e5a9da29144318be801e7f31340120bea94dce527285db9f->enter($__internal_5c5245eae0c1ce17e5a9da29144318be801e7f31340120bea94dce527285db9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:edit_content.html.twig", "@FOSUser/Group/edit.html.twig", 4)->display($context);
        
        $__internal_5c5245eae0c1ce17e5a9da29144318be801e7f31340120bea94dce527285db9f->leave($__internal_5c5245eae0c1ce17e5a9da29144318be801e7f31340120bea94dce527285db9f_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:edit_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
