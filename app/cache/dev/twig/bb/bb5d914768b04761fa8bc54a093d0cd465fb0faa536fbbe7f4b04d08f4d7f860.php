<?php

/* TwigBundle:Exception:error.txt.twig */
class __TwigTemplate_a6b54d172dda02d8b3d993219c90f3486d26d745d45517827124870d7d21580a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1654bfc71d8a6d1f49f58387acc02c5ff74b699a86fd17494fe8648e46e3f707 = $this->env->getExtension("native_profiler");
        $__internal_1654bfc71d8a6d1f49f58387acc02c5ff74b699a86fd17494fe8648e46e3f707->enter($__internal_1654bfc71d8a6d1f49f58387acc02c5ff74b699a86fd17494fe8648e46e3f707_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code"));
        echo " ";
        echo (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_1654bfc71d8a6d1f49f58387acc02c5ff74b699a86fd17494fe8648e46e3f707->leave($__internal_1654bfc71d8a6d1f49f58387acc02c5ff74b699a86fd17494fe8648e46e3f707_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 4,  22 => 1,);
    }
}
/* Oops! An Error Occurred*/
/* =======================*/
/* */
/* The server returned a "{{ status_code }} {{ status_text }}".*/
/* */
/* Something is broken. Please let us know what you were doing when this error occurred.*/
/* We will fix it as soon as possible. Sorry for any inconvenience caused.*/
/* */
