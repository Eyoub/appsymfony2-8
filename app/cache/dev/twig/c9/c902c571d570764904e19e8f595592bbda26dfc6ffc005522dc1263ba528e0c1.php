<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_c7dc5635e1b0661e738261482029d4770e3adb5b70a05123f31b99b2f7a0facf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8decee3d06b129d51d1e92fa0829f7b1f8b279d8880b5ebbe57ca74e0f3f24ad = $this->env->getExtension("native_profiler");
        $__internal_8decee3d06b129d51d1e92fa0829f7b1f8b279d8880b5ebbe57ca74e0f3f24ad->enter($__internal_8decee3d06b129d51d1e92fa0829f7b1f8b279d8880b5ebbe57ca74e0f3f24ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $__internal_8decee3d06b129d51d1e92fa0829f7b1f8b279d8880b5ebbe57ca74e0f3f24ad->leave($__internal_8decee3d06b129d51d1e92fa0829f7b1f8b279d8880b5ebbe57ca74e0f3f24ad_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <table <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <tr>*/
/*         <td colspan="2">*/
/*             <?php echo $view['form']->errors($form) ?>*/
/*         </td>*/
/*     </tr>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </table>*/
/* */
