<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_f4ee3be42c0cc0ebaa132e5ee1b5722570f67ed96d29f67d0746f63bb80f946b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7b449011f9ad28e6c987129789a02ac23106dbf0972d603d124d812e153d7129 = $this->env->getExtension("native_profiler");
        $__internal_7b449011f9ad28e6c987129789a02ac23106dbf0972d603d124d812e153d7129->enter($__internal_7b449011f9ad28e6c987129789a02ac23106dbf0972d603d124d812e153d7129_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_7b449011f9ad28e6c987129789a02ac23106dbf0972d603d124d812e153d7129->leave($__internal_7b449011f9ad28e6c987129789a02ac23106dbf0972d603d124d812e153d7129_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!isset($render_rest) || $render_rest): ?>*/
/* <?php echo $view['form']->rest($form) ?>*/
/* <?php endif ?>*/
/* </form>*/
/* */
