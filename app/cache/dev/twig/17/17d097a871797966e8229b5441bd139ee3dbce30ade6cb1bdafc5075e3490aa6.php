<?php

/* FOSUserBundle:Group:show_content.html.twig */
class __TwigTemplate_42520bff364d408d547b1bf9846e127b1e5d8f5aee914a99fd73a1fca03727a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_841d47783bb01a1cb1262a349fd13e5fefc5c5dade353922c554a76f0b68ee99 = $this->env->getExtension("native_profiler");
        $__internal_841d47783bb01a1cb1262a349fd13e5fefc5c5dade353922c554a76f0b68ee99->enter($__internal_841d47783bb01a1cb1262a349fd13e5fefc5c5dade353922c554a76f0b68ee99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_group_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("group.show.name", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["group"]) ? $context["group"] : $this->getContext($context, "group")), "getName", array(), "method"), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_841d47783bb01a1cb1262a349fd13e5fefc5c5dade353922c554a76f0b68ee99->leave($__internal_841d47783bb01a1cb1262a349fd13e5fefc5c5dade353922c554a76f0b68ee99_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 4,  22 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* <div class="fos_user_group_show">*/
/*     <p>{{ 'group.show.name'|trans }}: {{ group.getName() }}</p>*/
/* </div>*/
/* */
