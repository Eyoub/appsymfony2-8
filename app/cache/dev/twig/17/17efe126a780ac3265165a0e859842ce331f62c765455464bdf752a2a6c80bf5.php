<?php

/* @FOSUser/Registration/register.html.twig */
class __TwigTemplate_b8d7608c96819f249080b1e4e99f6d21a5a0bd675d806bed5eced2bc7ee7ece3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Registration/register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_56118976a07d808a1f2c7ea05620775466c04d9855a87508ac851bf80b2bfc37 = $this->env->getExtension("native_profiler");
        $__internal_56118976a07d808a1f2c7ea05620775466c04d9855a87508ac851bf80b2bfc37->enter($__internal_56118976a07d808a1f2c7ea05620775466c04d9855a87508ac851bf80b2bfc37_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_56118976a07d808a1f2c7ea05620775466c04d9855a87508ac851bf80b2bfc37->leave($__internal_56118976a07d808a1f2c7ea05620775466c04d9855a87508ac851bf80b2bfc37_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_a6234496f0fc0e56481b9496eba817206d273e30bf26f38d1ce461e6e65ec80d = $this->env->getExtension("native_profiler");
        $__internal_a6234496f0fc0e56481b9496eba817206d273e30bf26f38d1ce461e6e65ec80d->enter($__internal_a6234496f0fc0e56481b9496eba817206d273e30bf26f38d1ce461e6e65ec80d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Registration:register_content.html.twig", "@FOSUser/Registration/register.html.twig", 4)->display($context);
        
        $__internal_a6234496f0fc0e56481b9496eba817206d273e30bf26f38d1ce461e6e65ec80d->leave($__internal_a6234496f0fc0e56481b9496eba817206d273e30bf26f38d1ce461e6e65ec80d_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Registration:register_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
