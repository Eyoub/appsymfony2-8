<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_c41d67a6de5efa818c5f24851bf9e2f53b5b2f18326b220a876ed23da65934b8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Security/login.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_452b29bd458cedea0f9c2b6dfe8e309239c5604015fc772a1c88ff804c7f13ca = $this->env->getExtension("native_profiler");
        $__internal_452b29bd458cedea0f9c2b6dfe8e309239c5604015fc772a1c88ff804c7f13ca->enter($__internal_452b29bd458cedea0f9c2b6dfe8e309239c5604015fc772a1c88ff804c7f13ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_452b29bd458cedea0f9c2b6dfe8e309239c5604015fc772a1c88ff804c7f13ca->leave($__internal_452b29bd458cedea0f9c2b6dfe8e309239c5604015fc772a1c88ff804c7f13ca_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_a5781dd9b913bdf2802ec134302671b36c1a1c72c7462f928c125742f0c86f30 = $this->env->getExtension("native_profiler");
        $__internal_a5781dd9b913bdf2802ec134302671b36c1a1c72c7462f928c125742f0c86f30->enter($__internal_a5781dd9b913bdf2802ec134302671b36c1a1c72c7462f928c125742f0c86f30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "FOSUserBundle:Security:login_content.html.twig");
        echo "
";
        
        $__internal_a5781dd9b913bdf2802ec134302671b36c1a1c72c7462f928c125742f0c86f30->leave($__internal_a5781dd9b913bdf2802ec134302671b36c1a1c72c7462f928c125742f0c86f30_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/*     {{ include('FOSUserBundle:Security:login_content.html.twig') }}*/
/* {% endblock fos_user_content %}*/
/* */
