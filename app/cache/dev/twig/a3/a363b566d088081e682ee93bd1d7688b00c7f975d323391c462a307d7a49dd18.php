<?php

/* FilmBundle:Default:rectangle.html.twig */
class __TwigTemplate_8f2acbc696a59fc03b082c1abc360c775899b6766c57ca9dd758aeef841f9f11 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d4e04ce745556c31274a6f8ef0e1bcf92d1d8eb932bc5400c1c547ef71b5131e = $this->env->getExtension("native_profiler");
        $__internal_d4e04ce745556c31274a6f8ef0e1bcf92d1d8eb932bc5400c1c547ef71b5131e->enter($__internal_d4e04ce745556c31274a6f8ef0e1bcf92d1d8eb932bc5400c1c547ef71b5131e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FilmBundle:Default:rectangle.html.twig"));

        // line 2
        echo "perimetre =";
        echo twig_escape_filter($this->env, (isset($context["perim"]) ? $context["perim"] : $this->getContext($context, "perim")), "html", null, true);
        echo "
surface=";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["surf"]) ? $context["surf"] : $this->getContext($context, "surf")), "html", null, true);
        echo "

";
        
        $__internal_d4e04ce745556c31274a6f8ef0e1bcf92d1d8eb932bc5400c1c547ef71b5131e->leave($__internal_d4e04ce745556c31274a6f8ef0e1bcf92d1d8eb932bc5400c1c547ef71b5131e_prof);

    }

    public function getTemplateName()
    {
        return "FilmBundle:Default:rectangle.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  22 => 2,);
    }
}
/* {# empty Twig template #}*/
/* perimetre ={{perim}}*/
/* surface={{surf}}*/
/* */
/* */
