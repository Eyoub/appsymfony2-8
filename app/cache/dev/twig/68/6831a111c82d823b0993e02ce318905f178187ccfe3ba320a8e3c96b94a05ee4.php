<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_6e6fea68a87732c2364559b370a6b62b14ab8195950c9c85bf6d3288ae38d4bb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6b74b9b4b674e7cccd15dcba5e0d6c0effb495e1df97fb6f7e1b43da9d6fc25d = $this->env->getExtension("native_profiler");
        $__internal_6b74b9b4b674e7cccd15dcba5e0d6c0effb495e1df97fb6f7e1b43da9d6fc25d->enter($__internal_6b74b9b4b674e7cccd15dcba5e0d6c0effb495e1df97fb6f7e1b43da9d6fc25d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_6b74b9b4b674e7cccd15dcba5e0d6c0effb495e1df97fb6f7e1b43da9d6fc25d->leave($__internal_6b74b9b4b674e7cccd15dcba5e0d6c0effb495e1df97fb6f7e1b43da9d6fc25d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td></td>*/
/*     <td>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
