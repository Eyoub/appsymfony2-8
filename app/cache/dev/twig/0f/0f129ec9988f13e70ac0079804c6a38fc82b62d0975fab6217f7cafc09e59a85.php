<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_5d28c38270635ac0b3b650642b2f6b79edfa16f23a14b4893ddb8c01727e1be8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4e1a22d8ac45cc6b901465a34298bf34e6113dc8c674402faaae72b0d72f982a = $this->env->getExtension("native_profiler");
        $__internal_4e1a22d8ac45cc6b901465a34298bf34e6113dc8c674402faaae72b0d72f982a->enter($__internal_4e1a22d8ac45cc6b901465a34298bf34e6113dc8c674402faaae72b0d72f982a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_4e1a22d8ac45cc6b901465a34298bf34e6113dc8c674402faaae72b0d72f982a->leave($__internal_4e1a22d8ac45cc6b901465a34298bf34e6113dc8c674402faaae72b0d72f982a_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
