<?php

/* @Film/Categories/new.html.twig */
class __TwigTemplate_184564b1c24fbc593d4007bfa19a96e6ef1a74acae6da2aa6259a5f62e0fa8f1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FilmBundle::index.html.twig", "@Film/Categories/new.html.twig", 1);
        $this->blocks = array(
            'A' => array($this, 'block_A'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FilmBundle::index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c1c08c4f94684bd08780ab721d3f8a6ed9edba6fa3179f125780426a58f025da = $this->env->getExtension("native_profiler");
        $__internal_c1c08c4f94684bd08780ab721d3f8a6ed9edba6fa3179f125780426a58f025da->enter($__internal_c1c08c4f94684bd08780ab721d3f8a6ed9edba6fa3179f125780426a58f025da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Film/Categories/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c1c08c4f94684bd08780ab721d3f8a6ed9edba6fa3179f125780426a58f025da->leave($__internal_c1c08c4f94684bd08780ab721d3f8a6ed9edba6fa3179f125780426a58f025da_prof);

    }

    // line 2
    public function block_A($context, array $blocks = array())
    {
        $__internal_618d5ee10af0bdd684d380646668bdebeaed67ae9500138e85bf9c3bac7ac020 = $this->env->getExtension("native_profiler");
        $__internal_618d5ee10af0bdd684d380646668bdebeaed67ae9500138e85bf9c3bac7ac020->enter($__internal_618d5ee10af0bdd684d380646668bdebeaed67ae9500138e85bf9c3bac7ac020_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "A"));

        // line 3
        echo "    <div class=\"row\">

        <div class=\"col-md-3\">
            <p class=\"lead\">Gestion des categories</p>
            <div class=\"list-group\">
                <a href=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("category_index");
        echo "\" class=\"list-group-item\">lister</a>
                <a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("category_new");
        echo "\" class=\"list-group-item\">creer</a>
            </div>
        </div>
        <div class=\"col-md-9\">
            <form method=\"POST\">

                ";
        // line 15
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
                <input type=\"submit\" value=\"envoyer\"/>    
            </form>
        </div>
    </div>
";
        
        $__internal_618d5ee10af0bdd684d380646668bdebeaed67ae9500138e85bf9c3bac7ac020->leave($__internal_618d5ee10af0bdd684d380646668bdebeaed67ae9500138e85bf9c3bac7ac020_prof);

    }

    public function getTemplateName()
    {
        return "@Film/Categories/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 15,  51 => 9,  47 => 8,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends "FilmBundle::index.html.twig" %}*/
/* {% block A %}*/
/*     <div class="row">*/
/* */
/*         <div class="col-md-3">*/
/*             <p class="lead">Gestion des categories</p>*/
/*             <div class="list-group">*/
/*                 <a href="{{path('category_index')}}" class="list-group-item">lister</a>*/
/*                 <a href="{{path('category_new')}}" class="list-group-item">creer</a>*/
/*             </div>*/
/*         </div>*/
/*         <div class="col-md-9">*/
/*             <form method="POST">*/
/* */
/*                 {{form_widget(form)}}*/
/*                 <input type="submit" value="envoyer"/>    */
/*             </form>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
