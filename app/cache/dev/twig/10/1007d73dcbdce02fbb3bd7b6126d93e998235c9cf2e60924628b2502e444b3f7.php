<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_d624157f69ebfbac7b95261407da69be2a79c11d0dc8efc07734c29196f0a13b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8c44a86f3bf9660f459c72063fde4562268d52c1a70aecb49f1eaca4c5f95b78 = $this->env->getExtension("native_profiler");
        $__internal_8c44a86f3bf9660f459c72063fde4562268d52c1a70aecb49f1eaca4c5f95b78->enter($__internal_8c44a86f3bf9660f459c72063fde4562268d52c1a70aecb49f1eaca4c5f95b78_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_8c44a86f3bf9660f459c72063fde4562268d52c1a70aecb49f1eaca4c5f95b78->leave($__internal_8c44a86f3bf9660f459c72063fde4562268d52c1a70aecb49f1eaca4c5f95b78_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'password')) ?>*/
/* */
