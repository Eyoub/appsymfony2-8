<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_25b2718f8db115319201cf586012f2c18923738b5c8fd47b3b4390ba0970995c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1addebc173cc3328723091636593d7daa387fb790338239ba7d8b9194ed01c77 = $this->env->getExtension("native_profiler");
        $__internal_1addebc173cc3328723091636593d7daa387fb790338239ba7d8b9194ed01c77->enter($__internal_1addebc173cc3328723091636593d7daa387fb790338239ba7d8b9194ed01c77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_1addebc173cc3328723091636593d7daa387fb790338239ba7d8b9194ed01c77->leave($__internal_1addebc173cc3328723091636593d7daa387fb790338239ba7d8b9194ed01c77_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_922d0859b98ef7271ff8e7cf441986bf87c3fa64aab6e418781a06b98eaa73b6 = $this->env->getExtension("native_profiler");
        $__internal_922d0859b98ef7271ff8e7cf441986bf87c3fa64aab6e418781a06b98eaa73b6->enter($__internal_922d0859b98ef7271ff8e7cf441986bf87c3fa64aab6e418781a06b98eaa73b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_922d0859b98ef7271ff8e7cf441986bf87c3fa64aab6e418781a06b98eaa73b6->leave($__internal_922d0859b98ef7271ff8e7cf441986bf87c3fa64aab6e418781a06b98eaa73b6_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
