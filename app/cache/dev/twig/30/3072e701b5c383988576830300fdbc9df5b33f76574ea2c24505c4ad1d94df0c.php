<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_2df8900ee0405cdb3bfbaf1ef051263c6afcaabe46244adb796b286471eec6a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d2d5c1e980f1cc30b9d45788c0bdcb05f4b7f359c2574a25c83e787db5037eb7 = $this->env->getExtension("native_profiler");
        $__internal_d2d5c1e980f1cc30b9d45788c0bdcb05f4b7f359c2574a25c83e787db5037eb7->enter($__internal_d2d5c1e980f1cc30b9d45788c0bdcb05f4b7f359c2574a25c83e787db5037eb7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_d2d5c1e980f1cc30b9d45788c0bdcb05f4b7f359c2574a25c83e787db5037eb7->leave($__internal_d2d5c1e980f1cc30b9d45788c0bdcb05f4b7f359c2574a25c83e787db5037eb7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (count($errors) > 0): ?>*/
/*     <ul>*/
/*         <?php foreach ($errors as $error): ?>*/
/*             <li><?php echo $error->getMessage() ?></li>*/
/*         <?php endforeach; ?>*/
/*     </ul>*/
/* <?php endif ?>*/
/* */
