<?php

/* FOSUserBundle:Registration:check_email.html.twig */
class __TwigTemplate_551078b6144ff83e2a942165ca450462367d4516afa7e4ecabe054b8886ea16c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Registration:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3ba3b90cf9f236b5e6cf03273014bc2fdfc9664a54f89f67721f6c2b010b0a0b = $this->env->getExtension("native_profiler");
        $__internal_3ba3b90cf9f236b5e6cf03273014bc2fdfc9664a54f89f67721f6c2b010b0a0b->enter($__internal_3ba3b90cf9f236b5e6cf03273014bc2fdfc9664a54f89f67721f6c2b010b0a0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3ba3b90cf9f236b5e6cf03273014bc2fdfc9664a54f89f67721f6c2b010b0a0b->leave($__internal_3ba3b90cf9f236b5e6cf03273014bc2fdfc9664a54f89f67721f6c2b010b0a0b_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_b4e05ab48eba075dbb03884158c11952039c3c28ce2bf880523b0431e6e618f5 = $this->env->getExtension("native_profiler");
        $__internal_b4e05ab48eba075dbb03884158c11952039c3c28ce2bf880523b0431e6e618f5->enter($__internal_b4e05ab48eba075dbb03884158c11952039c3c28ce2bf880523b0431e6e618f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.check_email", array("%email%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_b4e05ab48eba075dbb03884158c11952039c3c28ce2bf880523b0431e6e618f5->leave($__internal_b4e05ab48eba075dbb03884158c11952039c3c28ce2bf880523b0431e6e618f5_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/*     <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>*/
/* {% endblock fos_user_content %}*/
/* */
