<?php

/* FOSUserBundle:Profile:edit.html.twig */
class __TwigTemplate_3715d955f4eba9b46515ee515bcd863ab5f8bb8481b8515e758c70ac67a6d669 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "FOSUserBundle:Profile:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2b9f0b42407077638a02010a7d5a1f50693f39ff182ffaf7f00513ac97055d07 = $this->env->getExtension("native_profiler");
        $__internal_2b9f0b42407077638a02010a7d5a1f50693f39ff182ffaf7f00513ac97055d07->enter($__internal_2b9f0b42407077638a02010a7d5a1f50693f39ff182ffaf7f00513ac97055d07_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2b9f0b42407077638a02010a7d5a1f50693f39ff182ffaf7f00513ac97055d07->leave($__internal_2b9f0b42407077638a02010a7d5a1f50693f39ff182ffaf7f00513ac97055d07_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_4902f26876727c54fdb238e914ae7b0153b20486268983b1befb8b90f61763ab = $this->env->getExtension("native_profiler");
        $__internal_4902f26876727c54fdb238e914ae7b0153b20486268983b1befb8b90f61763ab->enter($__internal_4902f26876727c54fdb238e914ae7b0153b20486268983b1befb8b90f61763ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Profile:edit_content.html.twig", "FOSUserBundle:Profile:edit.html.twig", 4)->display($context);
        
        $__internal_4902f26876727c54fdb238e914ae7b0153b20486268983b1befb8b90f61763ab->leave($__internal_4902f26876727c54fdb238e914ae7b0153b20486268983b1befb8b90f61763ab_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Profile:edit_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
