<?php

/* @FOSUser/Registration/check_email.html.twig */
class __TwigTemplate_99b68654f41127b61c5b9123a749072553ccd3e7e657185e9f12b1e097adaf80 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Registration/check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c22fdbf57740d63cfa75395595454a092c26810da11da27c8a94b71aef37ace3 = $this->env->getExtension("native_profiler");
        $__internal_c22fdbf57740d63cfa75395595454a092c26810da11da27c8a94b71aef37ace3->enter($__internal_c22fdbf57740d63cfa75395595454a092c26810da11da27c8a94b71aef37ace3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c22fdbf57740d63cfa75395595454a092c26810da11da27c8a94b71aef37ace3->leave($__internal_c22fdbf57740d63cfa75395595454a092c26810da11da27c8a94b71aef37ace3_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_3b66f00bf63e55e071996790016fd776c32a4286b1d938822a490b76f0c19c82 = $this->env->getExtension("native_profiler");
        $__internal_3b66f00bf63e55e071996790016fd776c32a4286b1d938822a490b76f0c19c82->enter($__internal_3b66f00bf63e55e071996790016fd776c32a4286b1d938822a490b76f0c19c82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("registration.check_email", array("%email%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_3b66f00bf63e55e071996790016fd776c32a4286b1d938822a490b76f0c19c82->leave($__internal_3b66f00bf63e55e071996790016fd776c32a4286b1d938822a490b76f0c19c82_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 6,  34 => 5,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* {% block fos_user_content %}*/
/*     <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>*/
/* {% endblock fos_user_content %}*/
/* */
