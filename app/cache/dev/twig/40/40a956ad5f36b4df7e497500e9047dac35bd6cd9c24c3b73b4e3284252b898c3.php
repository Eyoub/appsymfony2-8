<?php

/* :film:edit.html.twig */
class __TwigTemplate_4cdd35480d630b5196e760390d3064114b7b6ce07a06c6c97248f17f83a05bd2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FilmBundle::index.html.twig", ":film:edit.html.twig", 1);
        $this->blocks = array(
            'A' => array($this, 'block_A'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FilmBundle::index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_adc29ab4adaccb0b568e9b1c575f99dac3f1e0a1a1f74cc64543c3e066f8016e = $this->env->getExtension("native_profiler");
        $__internal_adc29ab4adaccb0b568e9b1c575f99dac3f1e0a1a1f74cc64543c3e066f8016e->enter($__internal_adc29ab4adaccb0b568e9b1c575f99dac3f1e0a1a1f74cc64543c3e066f8016e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":film:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_adc29ab4adaccb0b568e9b1c575f99dac3f1e0a1a1f74cc64543c3e066f8016e->leave($__internal_adc29ab4adaccb0b568e9b1c575f99dac3f1e0a1a1f74cc64543c3e066f8016e_prof);

    }

    // line 3
    public function block_A($context, array $blocks = array())
    {
        $__internal_7e1fef2af69a8e436a812741618e47b7de856c186bbada1071ac02a0b1247ccb = $this->env->getExtension("native_profiler");
        $__internal_7e1fef2af69a8e436a812741618e47b7de856c186bbada1071ac02a0b1247ccb->enter($__internal_7e1fef2af69a8e436a812741618e47b7de856c186bbada1071ac02a0b1247ccb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "A"));

        // line 4
        echo "    <div class=\"row\">

            <div class=\"col-md-3\">
                <p class=\"lead\">Gestion des filmes</p>
                <div class=\"list-group\">
                    <a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("film_index");
        echo "\" class=\"list-group-item\">lister</a>
                    <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("film_new");
        echo "\" class=\"list-group-item\">creer</a>
                    <a href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("film_editer");
        echo "\" class=\"list-group-item\">editer</a>
                </div>
            </div>
<div class=\"col-md-9\">

    ";
        // line 16
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_start');
        echo "
        ";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'widget');
        echo "
        <p>
            <img src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["film"]) ? $context["film"] : $this->getContext($context, "film")), "poster", array()), "pathName", array())), "html", null, true);
        echo "\" alt=\"\" width=\"100\" height=\"50\"/>
        </p>
        <input type=\"submit\" value=\"Edit\" />
    ";
        // line 22
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["edit_form"]) ? $context["edit_form"] : $this->getContext($context, "edit_form")), 'form_end');
        echo "
</div>
</div>
";
        
        $__internal_7e1fef2af69a8e436a812741618e47b7de856c186bbada1071ac02a0b1247ccb->leave($__internal_7e1fef2af69a8e436a812741618e47b7de856c186bbada1071ac02a0b1247ccb_prof);

    }

    public function getTemplateName()
    {
        return ":film:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 22,  72 => 19,  67 => 17,  63 => 16,  55 => 11,  51 => 10,  47 => 9,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'FilmBundle::index.html.twig' %}*/
/* */
/* {% block A %}*/
/*     <div class="row">*/
/* */
/*             <div class="col-md-3">*/
/*                 <p class="lead">Gestion des filmes</p>*/
/*                 <div class="list-group">*/
/*                     <a href="{{path('film_index')}}" class="list-group-item">lister</a>*/
/*                     <a href="{{path('film_new')}}" class="list-group-item">creer</a>*/
/*                     <a href="{{path('film_editer')}}" class="list-group-item">editer</a>*/
/*                 </div>*/
/*             </div>*/
/* <div class="col-md-9">*/
/* */
/*     {{ form_start(edit_form) }}*/
/*         {{ form_widget(edit_form) }}*/
/*         <p>*/
/*             <img src="{{ asset(film.poster.pathName)}}" alt="" width="100" height="50"/>*/
/*         </p>*/
/*         <input type="submit" value="Edit" />*/
/*     {{ form_end(edit_form) }}*/
/* </div>*/
/* </div>*/
/* {% endblock %}*/
/* */
