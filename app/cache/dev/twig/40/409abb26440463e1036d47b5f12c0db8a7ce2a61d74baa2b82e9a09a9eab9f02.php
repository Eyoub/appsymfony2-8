<?php

/* FilmBundle:Categories:new.html.twig */
class __TwigTemplate_5971efcb4ad131c092408965d7d03c9a1509c6dd076053a7d06c87baacd2d0d2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FilmBundle::index.html.twig", "FilmBundle:Categories:new.html.twig", 1);
        $this->blocks = array(
            'A' => array($this, 'block_A'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FilmBundle::index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_052029dc253f7f524b8ae358734cd8dd0dd36944549dfd99987282d304f91bfa = $this->env->getExtension("native_profiler");
        $__internal_052029dc253f7f524b8ae358734cd8dd0dd36944549dfd99987282d304f91bfa->enter($__internal_052029dc253f7f524b8ae358734cd8dd0dd36944549dfd99987282d304f91bfa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FilmBundle:Categories:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_052029dc253f7f524b8ae358734cd8dd0dd36944549dfd99987282d304f91bfa->leave($__internal_052029dc253f7f524b8ae358734cd8dd0dd36944549dfd99987282d304f91bfa_prof);

    }

    // line 2
    public function block_A($context, array $blocks = array())
    {
        $__internal_1d25e3b5d98f3450964aa2df276daa1137d15ba158831cb5032849e298d4b5f6 = $this->env->getExtension("native_profiler");
        $__internal_1d25e3b5d98f3450964aa2df276daa1137d15ba158831cb5032849e298d4b5f6->enter($__internal_1d25e3b5d98f3450964aa2df276daa1137d15ba158831cb5032849e298d4b5f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "A"));

        // line 3
        echo "    <div class=\"row\">

        <div class=\"col-md-3\">
            <p class=\"lead\">Gestion des categories</p>
            <div class=\"list-group\">
                <a href=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("category_index");
        echo "\" class=\"list-group-item\">lister</a>
                <a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("category_new");
        echo "\" class=\"list-group-item\">creer</a>
            </div>
        </div>
        <div class=\"col-md-9\">
            <form method=\"POST\">

                ";
        // line 15
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
                <input type=\"submit\" value=\"envoyer\"/>    
            </form>
        </div>
    </div>
";
        
        $__internal_1d25e3b5d98f3450964aa2df276daa1137d15ba158831cb5032849e298d4b5f6->leave($__internal_1d25e3b5d98f3450964aa2df276daa1137d15ba158831cb5032849e298d4b5f6_prof);

    }

    public function getTemplateName()
    {
        return "FilmBundle:Categories:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 15,  51 => 9,  47 => 8,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends "FilmBundle::index.html.twig" %}*/
/* {% block A %}*/
/*     <div class="row">*/
/* */
/*         <div class="col-md-3">*/
/*             <p class="lead">Gestion des categories</p>*/
/*             <div class="list-group">*/
/*                 <a href="{{path('category_index')}}" class="list-group-item">lister</a>*/
/*                 <a href="{{path('category_new')}}" class="list-group-item">creer</a>*/
/*             </div>*/
/*         </div>*/
/*         <div class="col-md-9">*/
/*             <form method="POST">*/
/* */
/*                 {{form_widget(form)}}*/
/*                 <input type="submit" value="envoyer"/>    */
/*             </form>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
