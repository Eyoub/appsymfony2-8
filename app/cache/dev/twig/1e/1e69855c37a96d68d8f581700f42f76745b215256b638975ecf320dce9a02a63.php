<?php

/* @WebProfiler/Profiler/ajax_layout.html.twig */
class __TwigTemplate_d385f203da7c8ee7c2ea85821bdafd6bea6d4ab6e10fec5a271565d208c4848a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_eead0e3db272556a379e0d1009c3d2e7097d39c74062bb7a3cb5e6f5906aa3b7 = $this->env->getExtension("native_profiler");
        $__internal_eead0e3db272556a379e0d1009c3d2e7097d39c74062bb7a3cb5e6f5906aa3b7->enter($__internal_eead0e3db272556a379e0d1009c3d2e7097d39c74062bb7a3cb5e6f5906aa3b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_eead0e3db272556a379e0d1009c3d2e7097d39c74062bb7a3cb5e6f5906aa3b7->leave($__internal_eead0e3db272556a379e0d1009c3d2e7097d39c74062bb7a3cb5e6f5906aa3b7_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_9cc3699f6a478e21a795362beafd77a43f259780f60c7d32d560ec3c3d990d17 = $this->env->getExtension("native_profiler");
        $__internal_9cc3699f6a478e21a795362beafd77a43f259780f60c7d32d560ec3c3d990d17->enter($__internal_9cc3699f6a478e21a795362beafd77a43f259780f60c7d32d560ec3c3d990d17_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_9cc3699f6a478e21a795362beafd77a43f259780f60c7d32d560ec3c3d990d17->leave($__internal_9cc3699f6a478e21a795362beafd77a43f259780f60c7d32d560ec3c3d990d17_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
