<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_1f77fcb181d6b0512f6979fc13c634840d2b8ecb4abd9b41d810af37422e23a5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_30a82a3282b66cfcadb5ac5118a55fc37f081a5eb6ca29410311bf1fca9d5aca = $this->env->getExtension("native_profiler");
        $__internal_30a82a3282b66cfcadb5ac5118a55fc37f081a5eb6ca29410311bf1fca9d5aca->enter($__internal_30a82a3282b66cfcadb5ac5118a55fc37f081a5eb6ca29410311bf1fca9d5aca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_30a82a3282b66cfcadb5ac5118a55fc37f081a5eb6ca29410311bf1fca9d5aca->leave($__internal_30a82a3282b66cfcadb5ac5118a55fc37f081a5eb6ca29410311bf1fca9d5aca_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </div>*/
/* */
