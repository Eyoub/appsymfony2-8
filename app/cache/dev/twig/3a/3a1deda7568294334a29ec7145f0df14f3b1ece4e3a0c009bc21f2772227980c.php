<?php

/* @FOSUser/Resetting/request.html.twig */
class __TwigTemplate_d9a90d37caa37ac34d100e506faa7d17696533765eb82f9a86f6a2cc3c9cc927 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Resetting/request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a6bbb2898d56d6a7965ffdd41cdf67c74f969ec3da4100a0ca66d9118d339f09 = $this->env->getExtension("native_profiler");
        $__internal_a6bbb2898d56d6a7965ffdd41cdf67c74f969ec3da4100a0ca66d9118d339f09->enter($__internal_a6bbb2898d56d6a7965ffdd41cdf67c74f969ec3da4100a0ca66d9118d339f09_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a6bbb2898d56d6a7965ffdd41cdf67c74f969ec3da4100a0ca66d9118d339f09->leave($__internal_a6bbb2898d56d6a7965ffdd41cdf67c74f969ec3da4100a0ca66d9118d339f09_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_10762957cd766902b7a44be0ce3fa657ffb9c0a291afc6a2e8b087b8bb5debd6 = $this->env->getExtension("native_profiler");
        $__internal_10762957cd766902b7a44be0ce3fa657ffb9c0a291afc6a2e8b087b8bb5debd6->enter($__internal_10762957cd766902b7a44be0ce3fa657ffb9c0a291afc6a2e8b087b8bb5debd6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Resetting:request_content.html.twig", "@FOSUser/Resetting/request.html.twig", 4)->display($context);
        
        $__internal_10762957cd766902b7a44be0ce3fa657ffb9c0a291afc6a2e8b087b8bb5debd6->leave($__internal_10762957cd766902b7a44be0ce3fa657ffb9c0a291afc6a2e8b087b8bb5debd6_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Resetting:request_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
