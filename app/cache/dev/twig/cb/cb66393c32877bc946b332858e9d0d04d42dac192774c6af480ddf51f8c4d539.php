<?php

/* FOSUserBundle:Registration:email.txt.twig */
class __TwigTemplate_a1050b7ae8dee46b9d3b2be115ec14f3e767d0c3e240f3b71bec237b8820b4d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6235cf5c6de8f6e91ff52260fb210bb432cb01808b58beac320df131a03de129 = $this->env->getExtension("native_profiler");
        $__internal_6235cf5c6de8f6e91ff52260fb210bb432cb01808b58beac320df131a03de129->enter($__internal_6235cf5c6de8f6e91ff52260fb210bb432cb01808b58beac320df131a03de129_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        $this->displayBlock('body_text', $context, $blocks);
        // line 12
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_6235cf5c6de8f6e91ff52260fb210bb432cb01808b58beac320df131a03de129->leave($__internal_6235cf5c6de8f6e91ff52260fb210bb432cb01808b58beac320df131a03de129_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_e17e2b50b6099ff2990e0c5f4a446a193468372bb449eb7ed0b4d874d9643637 = $this->env->getExtension("native_profiler");
        $__internal_e17e2b50b6099ff2990e0c5f4a446a193468372bb449eb7ed0b4d874d9643637->enter($__internal_e17e2b50b6099ff2990e0c5f4a446a193468372bb449eb7ed0b4d874d9643637_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('translator')->trans("registration.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_e17e2b50b6099ff2990e0c5f4a446a193468372bb449eb7ed0b4d874d9643637->leave($__internal_e17e2b50b6099ff2990e0c5f4a446a193468372bb449eb7ed0b4d874d9643637_prof);

    }

    // line 7
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_11660f48b53e309c8e91a188882b62732dc2c2c6432fcdebff510bd810aa947c = $this->env->getExtension("native_profiler");
        $__internal_11660f48b53e309c8e91a188882b62732dc2c2c6432fcdebff510bd810aa947c->enter($__internal_11660f48b53e309c8e91a188882b62732dc2c2c6432fcdebff510bd810aa947c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 9
        echo $this->env->getExtension('translator')->trans("registration.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_11660f48b53e309c8e91a188882b62732dc2c2c6432fcdebff510bd810aa947c->leave($__internal_11660f48b53e309c8e91a188882b62732dc2c2c6432fcdebff510bd810aa947c_prof);

    }

    // line 12
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_02bc0b2721aed63a60c2b895dfdd9199cd00abea2e69a3135f99b85166a8c813 = $this->env->getExtension("native_profiler");
        $__internal_02bc0b2721aed63a60c2b895dfdd9199cd00abea2e69a3135f99b85166a8c813->enter($__internal_02bc0b2721aed63a60c2b895dfdd9199cd00abea2e69a3135f99b85166a8c813_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_02bc0b2721aed63a60c2b895dfdd9199cd00abea2e69a3135f99b85166a8c813->leave($__internal_02bc0b2721aed63a60c2b895dfdd9199cd00abea2e69a3135f99b85166a8c813_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  66 => 12,  57 => 9,  51 => 7,  42 => 4,  36 => 2,  29 => 12,  27 => 7,  25 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* {% block subject %}*/
/* {% autoescape false %}*/
/* {{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_text %}*/
/* {% autoescape false %}*/
/* {{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_html %}{% endblock %}*/
/* */
