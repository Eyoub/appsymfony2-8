<?php

/* @Framework/Form/range_widget.html.php */
class __TwigTemplate_d7f8a46e827eecbe81a6cc2cb3a22d5fb203e4c574bac1717d48bc48b6ca215e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1db6cbedf89fd0aa54b9428e54bdac261cd66b46ee6ff166279d48bd7ebe796e = $this->env->getExtension("native_profiler");
        $__internal_1db6cbedf89fd0aa54b9428e54bdac261cd66b46ee6ff166279d48bd7ebe796e->enter($__internal_1db6cbedf89fd0aa54b9428e54bdac261cd66b46ee6ff166279d48bd7ebe796e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/range_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'range'));
";
        
        $__internal_1db6cbedf89fd0aa54b9428e54bdac261cd66b46ee6ff166279d48bd7ebe796e->leave($__internal_1db6cbedf89fd0aa54b9428e54bdac261cd66b46ee6ff166279d48bd7ebe796e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/range_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'range'));*/
/* */
