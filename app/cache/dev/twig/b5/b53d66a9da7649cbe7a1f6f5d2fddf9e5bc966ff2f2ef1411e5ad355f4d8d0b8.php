<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_6ad95005b88478c413e6c42916a5ed5c0a4dba0b85aaf5dce7d2097271543462 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_41f504911b4a7627081c4030bd9b6ede13064c5c977b8bfad50870287a2d6729 = $this->env->getExtension("native_profiler");
        $__internal_41f504911b4a7627081c4030bd9b6ede13064c5c977b8bfad50870287a2d6729->enter($__internal_41f504911b4a7627081c4030bd9b6ede13064c5c977b8bfad50870287a2d6729_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_41f504911b4a7627081c4030bd9b6ede13064c5c977b8bfad50870287a2d6729->leave($__internal_41f504911b4a7627081c4030bd9b6ede13064c5c977b8bfad50870287a2d6729_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_c6a528dd6b651ff8d9b7f303c70932fb75f66f1022fc33f5a3c8a081c0ed9eb7 = $this->env->getExtension("native_profiler");
        $__internal_c6a528dd6b651ff8d9b7f303c70932fb75f66f1022fc33f5a3c8a081c0ed9eb7->enter($__internal_c6a528dd6b651ff8d9b7f303c70932fb75f66f1022fc33f5a3c8a081c0ed9eb7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_c6a528dd6b651ff8d9b7f303c70932fb75f66f1022fc33f5a3c8a081c0ed9eb7->leave($__internal_c6a528dd6b651ff8d9b7f303c70932fb75f66f1022fc33f5a3c8a081c0ed9eb7_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_7f54ce5dc55f54aa7a97eeaa3f7f4750554d0165e21d62bd7ced8fbdd1027e50 = $this->env->getExtension("native_profiler");
        $__internal_7f54ce5dc55f54aa7a97eeaa3f7f4750554d0165e21d62bd7ced8fbdd1027e50->enter($__internal_7f54ce5dc55f54aa7a97eeaa3f7f4750554d0165e21d62bd7ced8fbdd1027e50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_7f54ce5dc55f54aa7a97eeaa3f7f4750554d0165e21d62bd7ced8fbdd1027e50->leave($__internal_7f54ce5dc55f54aa7a97eeaa3f7f4750554d0165e21d62bd7ced8fbdd1027e50_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_0e1d2217cbe8451f93d2eefa5a1726482bfc9692d74cb56847062b88d0022109 = $this->env->getExtension("native_profiler");
        $__internal_0e1d2217cbe8451f93d2eefa5a1726482bfc9692d74cb56847062b88d0022109->enter($__internal_0e1d2217cbe8451f93d2eefa5a1726482bfc9692d74cb56847062b88d0022109_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_0e1d2217cbe8451f93d2eefa5a1726482bfc9692d74cb56847062b88d0022109->leave($__internal_0e1d2217cbe8451f93d2eefa5a1726482bfc9692d74cb56847062b88d0022109_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
