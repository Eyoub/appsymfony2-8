<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_436f624bbc54371d92040ad593548275f5873bd74b742b05cfe388c141bbe0f9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a61cbb25cc2aed8f7ca456b0d58bc31e9e2d1b27e6f82ef0168deb6a02282dfe = $this->env->getExtension("native_profiler");
        $__internal_a61cbb25cc2aed8f7ca456b0d58bc31e9e2d1b27e6f82ef0168deb6a02282dfe->enter($__internal_a61cbb25cc2aed8f7ca456b0d58bc31e9e2d1b27e6f82ef0168deb6a02282dfe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_a61cbb25cc2aed8f7ca456b0d58bc31e9e2d1b27e6f82ef0168deb6a02282dfe->leave($__internal_a61cbb25cc2aed8f7ca456b0d58bc31e9e2d1b27e6f82ef0168deb6a02282dfe_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_rows') ?>*/
/* */
