<?php

/* @Film/Acteur/modifActeur2.html.twig */
class __TwigTemplate_5888427301b5e1d91c34be61cd498ca96310cf386b44c62d5be1600bbf1316b4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FilmBundle::index.html.twig", "@Film/Acteur/modifActeur2.html.twig", 1);
        $this->blocks = array(
            'A' => array($this, 'block_A'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FilmBundle::index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b6f728afe65594d58ffa5c4ccaa2faabe36c28080ec1e86f823784daff24d46b = $this->env->getExtension("native_profiler");
        $__internal_b6f728afe65594d58ffa5c4ccaa2faabe36c28080ec1e86f823784daff24d46b->enter($__internal_b6f728afe65594d58ffa5c4ccaa2faabe36c28080ec1e86f823784daff24d46b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Film/Acteur/modifActeur2.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b6f728afe65594d58ffa5c4ccaa2faabe36c28080ec1e86f823784daff24d46b->leave($__internal_b6f728afe65594d58ffa5c4ccaa2faabe36c28080ec1e86f823784daff24d46b_prof);

    }

    // line 2
    public function block_A($context, array $blocks = array())
    {
        $__internal_32e3d8edd70301e800262841edab4a07ffdfcb4ded8c1d6868f55ad614dd121e = $this->env->getExtension("native_profiler");
        $__internal_32e3d8edd70301e800262841edab4a07ffdfcb4ded8c1d6868f55ad614dd121e->enter($__internal_32e3d8edd70301e800262841edab4a07ffdfcb4ded8c1d6868f55ad614dd121e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "A"));

        // line 3
        echo "    <div class=\"row\">

            <div class=\"col-md-3\">
                <p class=\"lead\">Gestion des acteurs</p>
                <div class=\"list-group\">
                    <a href=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("affich_act");
        echo "\" class=\"list-group-item\">Afficher</a>
                    <a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("ajout_act");
        echo "\" class=\"list-group-item\">Ajout</a>
                    <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("modif_act2");
        echo "\" class=\"list-group-item\">Editer</a>
                </div>
            </div>
<div class=\"col-md-9\">
<table class=\"table table-striped\">
";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["act"]) ? $context["act"] : $this->getContext($context, "act")));
        foreach ($context['_seq'] as $context["_key"] => $context["a"]) {
            // line 16
            echo " <tr>
     <td>";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "id", array()), "html", null, true);
            echo "</td><td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "nom", array()), "html", null, true);
            echo "</td><td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "prenom", array()), "html", null, true);
            echo "</td><td>";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["a"], "dateN", array()), "d-M-Y"), "html", null, true);
            echo "</td><td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["a"], "sexe", array()), "html", null, true);
            echo "</td>
     <td><a href=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("modif_act", array("id" => $this->getAttribute($context["a"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-edit\"></span> modifier</a></td>
     <td><a href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("supp_act", array("id" => $this->getAttribute($context["a"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-danger\"><span class=\"glyphicon glyphicon-trash\"></span> supprimer</a></td>
  </tr>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['a'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "</table>
</div>
    </div>
";
        
        $__internal_32e3d8edd70301e800262841edab4a07ffdfcb4ded8c1d6868f55ad614dd121e->leave($__internal_32e3d8edd70301e800262841edab4a07ffdfcb4ded8c1d6868f55ad614dd121e_prof);

    }

    public function getTemplateName()
    {
        return "@Film/Acteur/modifActeur2.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 22,  86 => 19,  82 => 18,  70 => 17,  67 => 16,  63 => 15,  55 => 10,  51 => 9,  47 => 8,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends "FilmBundle::index.html.twig" %}*/
/* {% block A %}*/
/*     <div class="row">*/
/* */
/*             <div class="col-md-3">*/
/*                 <p class="lead">Gestion des acteurs</p>*/
/*                 <div class="list-group">*/
/*                     <a href="{{path('affich_act')}}" class="list-group-item">Afficher</a>*/
/*                     <a href="{{path('ajout_act')}}" class="list-group-item">Ajout</a>*/
/*                     <a href="{{path('modif_act2')}}" class="list-group-item">Editer</a>*/
/*                 </div>*/
/*             </div>*/
/* <div class="col-md-9">*/
/* <table class="table table-striped">*/
/* {% for a in act %}*/
/*  <tr>*/
/*      <td>{{a.id}}</td><td>{{a.nom}}</td><td>{{a.prenom}}</td><td>{{a.dateN|date('d-M-Y')}}</td><td>{{a.sexe}}</td>*/
/*      <td><a href="{{path('modif_act', {'id':a.id})}}" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span> modifier</a></td>*/
/*      <td><a href="{{path('supp_act', {'id':a.id})}}" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> supprimer</a></td>*/
/*   </tr>*/
/* {%endfor%}*/
/* </table>*/
/* </div>*/
/*     </div>*/
/* {% endblock %}*/
