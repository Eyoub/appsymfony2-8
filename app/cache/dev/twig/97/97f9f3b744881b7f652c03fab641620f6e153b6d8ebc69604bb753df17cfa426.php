<?php

/* @User/layout.html.twig */
class __TwigTemplate_35ed37b18d9c8f13d0eac0063bcce7dfd40bf30cf4b7295d08fd560cc10fcf61 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FilmBundle::index.html.twig", "@User/layout.html.twig", 1);
        $this->blocks = array(
            'A' => array($this, 'block_A'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FilmBundle::index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c2a3da26121d0c2fd958711e9e0dda6d0a41c9f3baa4ae5cbfee91dcb61cfb7e = $this->env->getExtension("native_profiler");
        $__internal_c2a3da26121d0c2fd958711e9e0dda6d0a41c9f3baa4ae5cbfee91dcb61cfb7e->enter($__internal_c2a3da26121d0c2fd958711e9e0dda6d0a41c9f3baa4ae5cbfee91dcb61cfb7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@User/layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c2a3da26121d0c2fd958711e9e0dda6d0a41c9f3baa4ae5cbfee91dcb61cfb7e->leave($__internal_c2a3da26121d0c2fd958711e9e0dda6d0a41c9f3baa4ae5cbfee91dcb61cfb7e_prof);

    }

    // line 4
    public function block_A($context, array $blocks = array())
    {
        $__internal_1eaee929a8c395ee930a36f3f8cf236d379265fac703f758fb16f45e2829da7b = $this->env->getExtension("native_profiler");
        $__internal_1eaee929a8c395ee930a36f3f8cf236d379265fac703f758fb16f45e2829da7b->enter($__internal_1eaee929a8c395ee930a36f3f8cf236d379265fac703f758fb16f45e2829da7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "A"));

        // line 5
        echo "
    <div class=\"row\">    
        <div class=\"col-md-3\">
            <h1> Connexion </h1>
        </div>
        <div class=\"col-md-3\">
            ";
        // line 11
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "hasPreviousSession", array())) {
            // line 12
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "all", array(), "method"));
            foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                // line 13
                echo "                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 14
                    echo "                        <div class=\"flash-";
                    echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                    echo "\">
                            ";
                    // line 15
                    echo twig_escape_filter($this->env, $context["message"], "html", null, true);
                    echo "
                        </div>
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 18
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 19
            echo "            ";
        }
        // line 20
        echo "            <div>
                ";
        // line 21
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 23
        echo "            </div>
        </div> 
    </div>



";
        
        $__internal_1eaee929a8c395ee930a36f3f8cf236d379265fac703f758fb16f45e2829da7b->leave($__internal_1eaee929a8c395ee930a36f3f8cf236d379265fac703f758fb16f45e2829da7b_prof);

    }

    // line 21
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_17747e166ec8614afbcaf81b5afe0e26599983cc581add95a2b41a59ba78ffe8 = $this->env->getExtension("native_profiler");
        $__internal_17747e166ec8614afbcaf81b5afe0e26599983cc581add95a2b41a59ba78ffe8->enter($__internal_17747e166ec8614afbcaf81b5afe0e26599983cc581add95a2b41a59ba78ffe8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 22
        echo "                ";
        
        $__internal_17747e166ec8614afbcaf81b5afe0e26599983cc581add95a2b41a59ba78ffe8->leave($__internal_17747e166ec8614afbcaf81b5afe0e26599983cc581add95a2b41a59ba78ffe8_prof);

    }

    public function getTemplateName()
    {
        return "@User/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 22,  102 => 21,  89 => 23,  87 => 21,  84 => 20,  81 => 19,  75 => 18,  66 => 15,  61 => 14,  56 => 13,  51 => 12,  49 => 11,  41 => 5,  35 => 4,  11 => 1,);
    }
}
/* {% extends "FilmBundle::index.html.twig" %}*/
/*  */
/* */
/* {% block A %}*/
/* */
/*     <div class="row">    */
/*         <div class="col-md-3">*/
/*             <h1> Connexion </h1>*/
/*         </div>*/
/*         <div class="col-md-3">*/
/*             {% if app.request.hasPreviousSession %}*/
/*                 {% for type, messages in app.session.flashbag.all() %}*/
/*                     {% for message in messages %}*/
/*                         <div class="flash-{{ type }}">*/
/*                             {{ message }}*/
/*                         </div>*/
/*                     {% endfor %}*/
/*                 {% endfor %}*/
/*             {% endif %}*/
/*             <div>*/
/*                 {% block fos_user_content %}*/
/*                 {% endblock fos_user_content %}*/
/*             </div>*/
/*         </div> */
/*     </div>*/
/* */
/* */
/* */
/* {% endblock %}*/
