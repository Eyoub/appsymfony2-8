<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_c4500ce56602e109700f329305b7b22ce65904c0c447b23c016afb534acb9b8b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3016f6e0f9f1ee78dcf50d62ab4aa51ee23945e2f5919dd477214e5e7be02a9e = $this->env->getExtension("native_profiler");
        $__internal_3016f6e0f9f1ee78dcf50d62ab4aa51ee23945e2f5919dd477214e5e7be02a9e->enter($__internal_3016f6e0f9f1ee78dcf50d62ab4aa51ee23945e2f5919dd477214e5e7be02a9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "TwigBundle:Exception:error.atom.twig", 1)->display($context);
        
        $__internal_3016f6e0f9f1ee78dcf50d62ab4aa51ee23945e2f5919dd477214e5e7be02a9e->leave($__internal_3016f6e0f9f1ee78dcf50d62ab4aa51ee23945e2f5919dd477214e5e7be02a9e_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/error.xml.twig' %}*/
/* */
