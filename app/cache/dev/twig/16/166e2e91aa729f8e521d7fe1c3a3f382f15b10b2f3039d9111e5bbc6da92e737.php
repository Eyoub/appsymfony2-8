<?php

/* @Twig/Exception/error.xml.twig */
class __TwigTemplate_10bb0f661f09ad092c0c8f5008918a03f87b39f26c9a32599f8d51a1b0c8674b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8029eb0340805bdca344079476d63801557b700d8288f5724b580094e72715ee = $this->env->getExtension("native_profiler");
        $__internal_8029eb0340805bdca344079476d63801557b700d8288f5724b580094e72715ee->enter($__internal_8029eb0340805bdca344079476d63801557b700d8288f5724b580094e72715ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo "\" />
";
        
        $__internal_8029eb0340805bdca344079476d63801557b700d8288f5724b580094e72715ee->leave($__internal_8029eb0340805bdca344079476d63801557b700d8288f5724b580094e72715ee_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  22 => 1,);
    }
}
/* <?xml version="1.0" encoding="{{ _charset }}" ?>*/
/* */
/* <error code="{{ status_code }}" message="{{ status_text }}" />*/
/* */
