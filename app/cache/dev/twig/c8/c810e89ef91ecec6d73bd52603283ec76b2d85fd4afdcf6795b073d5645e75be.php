<?php

/* @WebProfiler/Profiler/toolbar_redirect.html.twig */
class __TwigTemplate_a3505af36f1910a4fbc0ad8ecc8533263ab89ae1c0237e9d4bf921385675929c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@WebProfiler/Profiler/toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fa6af7e442470d62dee576640153ed4bab28d0a35b20ad75c8bf0d85978612b1 = $this->env->getExtension("native_profiler");
        $__internal_fa6af7e442470d62dee576640153ed4bab28d0a35b20ad75c8bf0d85978612b1->enter($__internal_fa6af7e442470d62dee576640153ed4bab28d0a35b20ad75c8bf0d85978612b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fa6af7e442470d62dee576640153ed4bab28d0a35b20ad75c8bf0d85978612b1->leave($__internal_fa6af7e442470d62dee576640153ed4bab28d0a35b20ad75c8bf0d85978612b1_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_91bbce2721e0662c15ee40084eadc7e28ef95dbc150873aba3a1095ec0a1b409 = $this->env->getExtension("native_profiler");
        $__internal_91bbce2721e0662c15ee40084eadc7e28ef95dbc150873aba3a1095ec0a1b409->enter($__internal_91bbce2721e0662c15ee40084eadc7e28ef95dbc150873aba3a1095ec0a1b409_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_91bbce2721e0662c15ee40084eadc7e28ef95dbc150873aba3a1095ec0a1b409->leave($__internal_91bbce2721e0662c15ee40084eadc7e28ef95dbc150873aba3a1095ec0a1b409_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_02f2ee731512fa7c8c6ddc7ec2e9f729eefd1fa27a3eedb3df8d649e1266acaa = $this->env->getExtension("native_profiler");
        $__internal_02f2ee731512fa7c8c6ddc7ec2e9f729eefd1fa27a3eedb3df8d649e1266acaa->enter($__internal_02f2ee731512fa7c8c6ddc7ec2e9f729eefd1fa27a3eedb3df8d649e1266acaa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_02f2ee731512fa7c8c6ddc7ec2e9f729eefd1fa27a3eedb3df8d649e1266acaa->leave($__internal_02f2ee731512fa7c8c6ddc7ec2e9f729eefd1fa27a3eedb3df8d649e1266acaa_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block title 'Redirection Intercepted' %}*/
/* */
/* {% block body %}*/
/*     <div class="sf-reset">*/
/*         <div class="block-exception">*/
/*             <h1>This request redirects to <a href="{{ location }}">{{ location }}</a>.</h1>*/
/* */
/*             <p>*/
/*                 <small>*/
/*                     The redirect was intercepted by the web debug toolbar to help debugging.*/
/*                     For more information, see the "intercept-redirects" option of the Profiler.*/
/*                 </small>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
