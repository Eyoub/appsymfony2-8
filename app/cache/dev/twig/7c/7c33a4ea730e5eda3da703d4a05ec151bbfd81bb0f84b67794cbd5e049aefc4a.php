<?php

/* @Twig/Exception/exception.atom.twig */
class __TwigTemplate_0c6f40c1055d936c7259ca56d4ca84bcbc6e91eb76ddd66b0b62b540339925f9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7cc5dcbcf08870343c1ee72fbbfa57777c2aaade25191bde84b190f4f45bdeea = $this->env->getExtension("native_profiler");
        $__internal_7cc5dcbcf08870343c1ee72fbbfa57777c2aaade25191bde84b190f4f45bdeea->enter($__internal_7cc5dcbcf08870343c1ee72fbbfa57777c2aaade25191bde84b190f4f45bdeea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "@Twig/Exception/exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_7cc5dcbcf08870343c1ee72fbbfa57777c2aaade25191bde84b190f4f45bdeea->leave($__internal_7cc5dcbcf08870343c1ee72fbbfa57777c2aaade25191bde84b190f4f45bdeea_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
