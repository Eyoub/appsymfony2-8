<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_9b0592b53dab54c1903b74d00d07673f1d69f2124b084b58a09eb486c8ee51aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a7ff0886463af0d8ebe160fe031844daf1d9ed2ee72bb690409faf1bec9b30c4 = $this->env->getExtension("native_profiler");
        $__internal_a7ff0886463af0d8ebe160fe031844daf1d9ed2ee72bb690409faf1bec9b30c4->enter($__internal_a7ff0886463af0d8ebe160fe031844daf1d9ed2ee72bb690409faf1bec9b30c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_a7ff0886463af0d8ebe160fe031844daf1d9ed2ee72bb690409faf1bec9b30c4->leave($__internal_a7ff0886463af0d8ebe160fe031844daf1d9ed2ee72bb690409faf1bec9b30c4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/* */
