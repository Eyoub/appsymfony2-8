<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_0764ec879aff1cb890c3550ff94b2378ccf49bb30904cb32dae5fedd420a0197 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5503d7518f3456bc8de9762bcefad673021cff3f044f788d4f61d567af09eb8a = $this->env->getExtension("native_profiler");
        $__internal_5503d7518f3456bc8de9762bcefad673021cff3f044f788d4f61d567af09eb8a->enter($__internal_5503d7518f3456bc8de9762bcefad673021cff3f044f788d4f61d567af09eb8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_5503d7518f3456bc8de9762bcefad673021cff3f044f788d4f61d567af09eb8a->leave($__internal_5503d7518f3456bc8de9762bcefad673021cff3f044f788d4f61d567af09eb8a_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
