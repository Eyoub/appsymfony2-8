<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_3fcebb0143932bd0d0e5301273e6c63e79fbd65dd623c587ce3d4c9e7fbada92 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7942e50f2c34722a0e457d5cfd3f5477303fc0d27ab1512ed08580e14f580b9d = $this->env->getExtension("native_profiler");
        $__internal_7942e50f2c34722a0e457d5cfd3f5477303fc0d27ab1512ed08580e14f580b9d->enter($__internal_7942e50f2c34722a0e457d5cfd3f5477303fc0d27ab1512ed08580e14f580b9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7942e50f2c34722a0e457d5cfd3f5477303fc0d27ab1512ed08580e14f580b9d->leave($__internal_7942e50f2c34722a0e457d5cfd3f5477303fc0d27ab1512ed08580e14f580b9d_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_ce5703cd294e747259acb95f5dddf4de931ab31812e58b01f13be6752eb49e90 = $this->env->getExtension("native_profiler");
        $__internal_ce5703cd294e747259acb95f5dddf4de931ab31812e58b01f13be6752eb49e90->enter($__internal_ce5703cd294e747259acb95f5dddf4de931ab31812e58b01f13be6752eb49e90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_ce5703cd294e747259acb95f5dddf4de931ab31812e58b01f13be6752eb49e90->leave($__internal_ce5703cd294e747259acb95f5dddf4de931ab31812e58b01f13be6752eb49e90_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_6c19f37a4e165460d2abe35db4aaee66d606b5c27cd02a0089876b0c82355e91 = $this->env->getExtension("native_profiler");
        $__internal_6c19f37a4e165460d2abe35db4aaee66d606b5c27cd02a0089876b0c82355e91->enter($__internal_6c19f37a4e165460d2abe35db4aaee66d606b5c27cd02a0089876b0c82355e91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_6c19f37a4e165460d2abe35db4aaee66d606b5c27cd02a0089876b0c82355e91->leave($__internal_6c19f37a4e165460d2abe35db4aaee66d606b5c27cd02a0089876b0c82355e91_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_4b70eb771b56f4a58cd2d3a6a38562d501452c6860dfadca8cc963fb79c2d086 = $this->env->getExtension("native_profiler");
        $__internal_4b70eb771b56f4a58cd2d3a6a38562d501452c6860dfadca8cc963fb79c2d086->enter($__internal_4b70eb771b56f4a58cd2d3a6a38562d501452c6860dfadca8cc963fb79c2d086_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_4b70eb771b56f4a58cd2d3a6a38562d501452c6860dfadca8cc963fb79c2d086->leave($__internal_4b70eb771b56f4a58cd2d3a6a38562d501452c6860dfadca8cc963fb79c2d086_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
