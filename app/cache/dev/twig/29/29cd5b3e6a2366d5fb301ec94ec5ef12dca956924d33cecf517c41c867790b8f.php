<?php

/* @FOSUser/Group/new.html.twig */
class __TwigTemplate_d95483146bc64dff45b6e1c68502c14869773ad127a6cbcbc2fa2ed0fe6a7842 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Group/new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bdfc6e5f1291e07b0f4c2b96a822b00c3699a1621d04ff5b9b69e7e2ffdcd075 = $this->env->getExtension("native_profiler");
        $__internal_bdfc6e5f1291e07b0f4c2b96a822b00c3699a1621d04ff5b9b69e7e2ffdcd075->enter($__internal_bdfc6e5f1291e07b0f4c2b96a822b00c3699a1621d04ff5b9b69e7e2ffdcd075_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bdfc6e5f1291e07b0f4c2b96a822b00c3699a1621d04ff5b9b69e7e2ffdcd075->leave($__internal_bdfc6e5f1291e07b0f4c2b96a822b00c3699a1621d04ff5b9b69e7e2ffdcd075_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_012e33f2c0e1baea9b3e14ea5e7595bc5a8c488dce547333fe6fa081eda5eda3 = $this->env->getExtension("native_profiler");
        $__internal_012e33f2c0e1baea9b3e14ea5e7595bc5a8c488dce547333fe6fa081eda5eda3->enter($__internal_012e33f2c0e1baea9b3e14ea5e7595bc5a8c488dce547333fe6fa081eda5eda3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:new_content.html.twig", "@FOSUser/Group/new.html.twig", 4)->display($context);
        
        $__internal_012e33f2c0e1baea9b3e14ea5e7595bc5a8c488dce547333fe6fa081eda5eda3->leave($__internal_012e33f2c0e1baea9b3e14ea5e7595bc5a8c488dce547333fe6fa081eda5eda3_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:new_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
