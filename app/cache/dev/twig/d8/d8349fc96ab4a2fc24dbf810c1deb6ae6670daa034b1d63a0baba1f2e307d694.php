<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_06b0bf6c0077c5a1c6a6928c70764551477b379c546c6b1159872913b57e4e58 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1017adbe690c74ed04f39af3fe4a9fe33ffb578c8f82bc228139790e5da5efa1 = $this->env->getExtension("native_profiler");
        $__internal_1017adbe690c74ed04f39af3fe4a9fe33ffb578c8f82bc228139790e5da5efa1->enter($__internal_1017adbe690c74ed04f39af3fe4a9fe33ffb578c8f82bc228139790e5da5efa1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_1017adbe690c74ed04f39af3fe4a9fe33ffb578c8f82bc228139790e5da5efa1->leave($__internal_1017adbe690c74ed04f39af3fe4a9fe33ffb578c8f82bc228139790e5da5efa1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'number')) ?>*/
/* */
