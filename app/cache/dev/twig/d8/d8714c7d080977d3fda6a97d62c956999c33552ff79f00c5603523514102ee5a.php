<?php

/* @Film/Default/rectangle.html.twig */
class __TwigTemplate_60629db65442ceabaaff75497b274e473a3db54206f0a567ba3bf7fa194f4c77 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1503cc85a98dbfa109c9d621c9680c22566aeda865815967362e1a743c3e808f = $this->env->getExtension("native_profiler");
        $__internal_1503cc85a98dbfa109c9d621c9680c22566aeda865815967362e1a743c3e808f->enter($__internal_1503cc85a98dbfa109c9d621c9680c22566aeda865815967362e1a743c3e808f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Film/Default/rectangle.html.twig"));

        // line 2
        echo "perimetre =";
        echo twig_escape_filter($this->env, (isset($context["perim"]) ? $context["perim"] : $this->getContext($context, "perim")), "html", null, true);
        echo "
surface=";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["surf"]) ? $context["surf"] : $this->getContext($context, "surf")), "html", null, true);
        echo "

";
        
        $__internal_1503cc85a98dbfa109c9d621c9680c22566aeda865815967362e1a743c3e808f->leave($__internal_1503cc85a98dbfa109c9d621c9680c22566aeda865815967362e1a743c3e808f_prof);

    }

    public function getTemplateName()
    {
        return "@Film/Default/rectangle.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  22 => 2,);
    }
}
/* {# empty Twig template #}*/
/* perimetre ={{perim}}*/
/* surface={{surf}}*/
/* */
/* */
