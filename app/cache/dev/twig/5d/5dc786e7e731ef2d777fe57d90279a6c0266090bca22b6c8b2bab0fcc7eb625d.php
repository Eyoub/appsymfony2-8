<?php

/* @FOSUser/Profile/show.html.twig */
class __TwigTemplate_62c750137222a20004b9e425a81f4f53dee7f2e81dd74d3c302021d0f527c910 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Profile/show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1031afee8f494e70055c53bc845dda0047d68466401d80a509aebbac602d2e3c = $this->env->getExtension("native_profiler");
        $__internal_1031afee8f494e70055c53bc845dda0047d68466401d80a509aebbac602d2e3c->enter($__internal_1031afee8f494e70055c53bc845dda0047d68466401d80a509aebbac602d2e3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1031afee8f494e70055c53bc845dda0047d68466401d80a509aebbac602d2e3c->leave($__internal_1031afee8f494e70055c53bc845dda0047d68466401d80a509aebbac602d2e3c_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_f954e9fd75ea135b36c256071329054dd70872f64b4c7f485dd155f94d996d69 = $this->env->getExtension("native_profiler");
        $__internal_f954e9fd75ea135b36c256071329054dd70872f64b4c7f485dd155f94d996d69->enter($__internal_f954e9fd75ea135b36c256071329054dd70872f64b4c7f485dd155f94d996d69_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Profile:show_content.html.twig", "@FOSUser/Profile/show.html.twig", 4)->display($context);
        
        $__internal_f954e9fd75ea135b36c256071329054dd70872f64b4c7f485dd155f94d996d69->leave($__internal_f954e9fd75ea135b36c256071329054dd70872f64b4c7f485dd155f94d996d69_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Profile:show_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
