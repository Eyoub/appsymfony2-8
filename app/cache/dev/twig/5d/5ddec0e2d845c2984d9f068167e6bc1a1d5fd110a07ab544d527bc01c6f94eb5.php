<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_1feaeb704627486be2be0e8e7096cf425386dfad32a41721f4f5755e2a14ea94 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2007dacf536e0fb0f594ec6368fb8f5e57e34cd87aa423ca02385711f2846c50 = $this->env->getExtension("native_profiler");
        $__internal_2007dacf536e0fb0f594ec6368fb8f5e57e34cd87aa423ca02385711f2846c50->enter($__internal_2007dacf536e0fb0f594ec6368fb8f5e57e34cd87aa423ca02385711f2846c50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_2007dacf536e0fb0f594ec6368fb8f5e57e34cd87aa423ca02385711f2846c50->leave($__internal_2007dacf536e0fb0f594ec6368fb8f5e57e34cd87aa423ca02385711f2846c50_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?>*/
/* */
