<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_d6dfe808b5f2b5f8e3a14089bff5c06e9479916470c7b10cad83e7806f3e28c2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7e759b065f4dda4f7013b0548234df95235bf04bf3c3c80c695c33f36da09bf5 = $this->env->getExtension("native_profiler");
        $__internal_7e759b065f4dda4f7013b0548234df95235bf04bf3c3c80c695c33f36da09bf5->enter($__internal_7e759b065f4dda4f7013b0548234df95235bf04bf3c3c80c695c33f36da09bf5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_7e759b065f4dda4f7013b0548234df95235bf04bf3c3c80c695c33f36da09bf5->leave($__internal_7e759b065f4dda4f7013b0548234df95235bf04bf3c3c80c695c33f36da09bf5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->label($form) ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
