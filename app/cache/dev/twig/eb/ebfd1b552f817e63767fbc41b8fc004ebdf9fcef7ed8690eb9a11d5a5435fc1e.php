<?php

/* @Framework/Form/choice_attributes.html.php */
class __TwigTemplate_e6942e1d02e7120dd591a15e5feb912e17d868c6649b3f9cb54397ee70168b3e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_546fe4d784bbf62401010b6cec7703ecbde56ed4a69a4e43d2996ed4a3c4ef86 = $this->env->getExtension("native_profiler");
        $__internal_546fe4d784bbf62401010b6cec7703ecbde56ed4a69a4e43d2996ed4a3c4ef86->enter($__internal_546fe4d784bbf62401010b6cec7703ecbde56ed4a69a4e43d2996ed4a3c4ef86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        // line 1
        echo "id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"
<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
";
        
        $__internal_546fe4d784bbf62401010b6cec7703ecbde56ed4a69a4e43d2996ed4a3c4ef86->leave($__internal_546fe4d784bbf62401010b6cec7703ecbde56ed4a69a4e43d2996ed4a3c4ef86_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* id="<?php echo $view->escape($id) ?>" name="<?php echo $view->escape($full_name) ?>"*/
/* <?php if ($disabled): ?>disabled="disabled" <?php endif ?>*/
/* <?php foreach ($choice_attr as $k => $v): ?>*/
/* <?php if ($v === true): ?>*/
/* <?php printf('%s="%s" ', $view->escape($k), $view->escape($k)) ?>*/
/* <?php elseif ($v !== false): ?>*/
/* <?php printf('%s="%s" ', $view->escape($k), $view->escape($v)) ?>*/
/* <?php endif ?>*/
/* <?php endforeach ?>*/
/* */
