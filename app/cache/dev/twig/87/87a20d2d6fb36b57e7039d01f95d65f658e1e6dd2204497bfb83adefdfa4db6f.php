<?php

/* @FOSUser/Profile/edit.html.twig */
class __TwigTemplate_688c5cf33e13d844d46eb7c483573bb6ac961f2d5437cbd84b073b342e9de785 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Profile/edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a49a9ed3fea1846e5cae48c96830ba0bc8bb8a746e7fc994f31691d69a16f87c = $this->env->getExtension("native_profiler");
        $__internal_a49a9ed3fea1846e5cae48c96830ba0bc8bb8a746e7fc994f31691d69a16f87c->enter($__internal_a49a9ed3fea1846e5cae48c96830ba0bc8bb8a746e7fc994f31691d69a16f87c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a49a9ed3fea1846e5cae48c96830ba0bc8bb8a746e7fc994f31691d69a16f87c->leave($__internal_a49a9ed3fea1846e5cae48c96830ba0bc8bb8a746e7fc994f31691d69a16f87c_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_99e04012c76998afbe83a30909686a470f053141a520592541c3968a0f6780dd = $this->env->getExtension("native_profiler");
        $__internal_99e04012c76998afbe83a30909686a470f053141a520592541c3968a0f6780dd->enter($__internal_99e04012c76998afbe83a30909686a470f053141a520592541c3968a0f6780dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Profile:edit_content.html.twig", "@FOSUser/Profile/edit.html.twig", 4)->display($context);
        
        $__internal_99e04012c76998afbe83a30909686a470f053141a520592541c3968a0f6780dd->leave($__internal_99e04012c76998afbe83a30909686a470f053141a520592541c3968a0f6780dd_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Profile:edit_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
