<?php

/* @FOSUser/Group/show_content.html.twig */
class __TwigTemplate_3f8d7de365c93536b54fc7a3f2c1ae4c1f6caf13d908e19aa6fd50e5578dd627 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1937ddca97073b88b63b5a06abee75434462546ede5d4d560dc315f94dc85b87 = $this->env->getExtension("native_profiler");
        $__internal_1937ddca97073b88b63b5a06abee75434462546ede5d4d560dc315f94dc85b87->enter($__internal_1937ddca97073b88b63b5a06abee75434462546ede5d4d560dc315f94dc85b87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_group_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("group.show.name", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["group"]) ? $context["group"] : $this->getContext($context, "group")), "getName", array(), "method"), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_1937ddca97073b88b63b5a06abee75434462546ede5d4d560dc315f94dc85b87->leave($__internal_1937ddca97073b88b63b5a06abee75434462546ede5d4d560dc315f94dc85b87_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 4,  22 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* */
/* <div class="fos_user_group_show">*/
/*     <p>{{ 'group.show.name'|trans }}: {{ group.getName() }}</p>*/
/* </div>*/
/* */
