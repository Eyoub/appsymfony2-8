<?php

/* categorie/show.html.twig */
class __TwigTemplate_42b8ae555c5234bc7ce1e19234983476c9d3badbbdc0e0161d761a9f4ca47bdb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "categorie/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4b1409c2d83d7acf5d1f279ef2ac879d8af3d30996c2377e1fd6419f4a78f43e = $this->env->getExtension("native_profiler");
        $__internal_4b1409c2d83d7acf5d1f279ef2ac879d8af3d30996c2377e1fd6419f4a78f43e->enter($__internal_4b1409c2d83d7acf5d1f279ef2ac879d8af3d30996c2377e1fd6419f4a78f43e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "categorie/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4b1409c2d83d7acf5d1f279ef2ac879d8af3d30996c2377e1fd6419f4a78f43e->leave($__internal_4b1409c2d83d7acf5d1f279ef2ac879d8af3d30996c2377e1fd6419f4a78f43e_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_5e904edb9f18de2c2a4e2f8b903e7d1f6c9d64dbc9768ce57832d774e30bc937 = $this->env->getExtension("native_profiler");
        $__internal_5e904edb9f18de2c2a4e2f8b903e7d1f6c9d64dbc9768ce57832d774e30bc937->enter($__internal_5e904edb9f18de2c2a4e2f8b903e7d1f6c9d64dbc9768ce57832d774e30bc937_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Categorie</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["categorie"]) ? $context["categorie"] : $this->getContext($context, "categorie")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Nom</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["categorie"]) ? $context["categorie"] : $this->getContext($context, "categorie")), "nom", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("categorie_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("categorie_edit", array("id" => $this->getAttribute((isset($context["categorie"]) ? $context["categorie"] : $this->getContext($context, "categorie")), "id", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 27
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 29
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_5e904edb9f18de2c2a4e2f8b903e7d1f6c9d64dbc9768ce57832d774e30bc937->leave($__internal_5e904edb9f18de2c2a4e2f8b903e7d1f6c9d64dbc9768ce57832d774e30bc937_prof);

    }

    public function getTemplateName()
    {
        return "categorie/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 29,  77 => 27,  71 => 24,  65 => 21,  55 => 14,  48 => 10,  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/*     <h1>Categorie</h1>*/
/* */
/*     <table>*/
/*         <tbody>*/
/*             <tr>*/
/*                 <th>Id</th>*/
/*                 <td>{{ categorie.id }}</td>*/
/*             </tr>*/
/*             <tr>*/
/*                 <th>Nom</th>*/
/*                 <td>{{ categorie.nom }}</td>*/
/*             </tr>*/
/*         </tbody>*/
/*     </table>*/
/* */
/*     <ul>*/
/*         <li>*/
/*             <a href="{{ path('categorie_index') }}">Back to the list</a>*/
/*         </li>*/
/*         <li>*/
/*             <a href="{{ path('categorie_edit', { 'id': categorie.id }) }}">Edit</a>*/
/*         </li>*/
/*         <li>*/
/*             {{ form_start(delete_form) }}*/
/*                 <input type="submit" value="Delete">*/
/*             {{ form_end(delete_form) }}*/
/*         </li>*/
/*     </ul>*/
/* {% endblock %}*/
/* */
