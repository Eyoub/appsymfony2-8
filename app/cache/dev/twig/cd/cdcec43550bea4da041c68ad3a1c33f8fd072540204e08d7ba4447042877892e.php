<?php

/* @Twig/Exception/error.css.twig */
class __TwigTemplate_c70127037da09965b8363a7b3c0c2295ad576e3bc8dd660fb50e044d80f5f61b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_85e559746f833fe672a0306a08b5a1f5abd1e567a6ec55f09f5cb6e340a6bf72 = $this->env->getExtension("native_profiler");
        $__internal_85e559746f833fe672a0306a08b5a1f5abd1e567a6ec55f09f5cb6e340a6bf72->enter($__internal_85e559746f833fe672a0306a08b5a1f5abd1e567a6ec55f09f5cb6e340a6bf72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_85e559746f833fe672a0306a08b5a1f5abd1e567a6ec55f09f5cb6e340a6bf72->leave($__internal_85e559746f833fe672a0306a08b5a1f5abd1e567a6ec55f09f5cb6e340a6bf72_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
