<?php

/* @Film/Acteur/ajoutActeur.html.twig */
class __TwigTemplate_60555277c41109ffe3c79223452709e3a335f054c5a8d917c6cf90242e23485c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FilmBundle::index.html.twig", "@Film/Acteur/ajoutActeur.html.twig", 1);
        $this->blocks = array(
            'A' => array($this, 'block_A'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FilmBundle::index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f48c484b602b92119725ff4a2205578a96bc85b1f24ba06f3d8fbe616fdd00af = $this->env->getExtension("native_profiler");
        $__internal_f48c484b602b92119725ff4a2205578a96bc85b1f24ba06f3d8fbe616fdd00af->enter($__internal_f48c484b602b92119725ff4a2205578a96bc85b1f24ba06f3d8fbe616fdd00af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Film/Acteur/ajoutActeur.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f48c484b602b92119725ff4a2205578a96bc85b1f24ba06f3d8fbe616fdd00af->leave($__internal_f48c484b602b92119725ff4a2205578a96bc85b1f24ba06f3d8fbe616fdd00af_prof);

    }

    // line 2
    public function block_A($context, array $blocks = array())
    {
        $__internal_1beb4fce8b6ead84f90bfa7e31455c00ab38ed5a94d66c5033b03fdf280144b8 = $this->env->getExtension("native_profiler");
        $__internal_1beb4fce8b6ead84f90bfa7e31455c00ab38ed5a94d66c5033b03fdf280144b8->enter($__internal_1beb4fce8b6ead84f90bfa7e31455c00ab38ed5a94d66c5033b03fdf280144b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "A"));

        // line 3
        echo "    <div class=\"row\">

        <div class=\"col-md-3\">
            <p class=\"lead\">Gestion des acteurs</p>
            <div class=\"list-group\">
                <a href=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("affich_act");
        echo "\" class=\"list-group-item\">Afficher</a>
                <a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("ajout_act");
        echo "\" class=\"list-group-item\">Ajout</a>
                <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("modif_act2");
        echo "\" class=\"list-group-item\">Editer</a>
            </div>
        </div>
        <div class=\"col-md-9\">
            <form method=\"POST\">

                ";
        // line 16
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
                <input type=\"submit\" value=\"envoyer\"/>    
            </form>
        </div>
    </div>
    ";
        
        $__internal_1beb4fce8b6ead84f90bfa7e31455c00ab38ed5a94d66c5033b03fdf280144b8->leave($__internal_1beb4fce8b6ead84f90bfa7e31455c00ab38ed5a94d66c5033b03fdf280144b8_prof);

    }

    public function getTemplateName()
    {
        return "@Film/Acteur/ajoutActeur.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 16,  55 => 10,  51 => 9,  47 => 8,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends "FilmBundle::index.html.twig" %}*/
/* {% block A %}*/
/*     <div class="row">*/
/* */
/*         <div class="col-md-3">*/
/*             <p class="lead">Gestion des acteurs</p>*/
/*             <div class="list-group">*/
/*                 <a href="{{path('affich_act')}}" class="list-group-item">Afficher</a>*/
/*                 <a href="{{path('ajout_act')}}" class="list-group-item">Ajout</a>*/
/*                 <a href="{{path('modif_act2')}}" class="list-group-item">Editer</a>*/
/*             </div>*/
/*         </div>*/
/*         <div class="col-md-9">*/
/*             <form method="POST">*/
/* */
/*                 {{form_widget(form)}}*/
/*                 <input type="submit" value="envoyer"/>    */
/*             </form>*/
/*         </div>*/
/*     </div>*/
/*     {% endblock %}*/
