<?php

/* @FOSUser/Group/list.html.twig */
class __TwigTemplate_eb4fd45c3eec0d58b3fe7852c0413233ad4cb5772ab4615b4277e4895d9ea5c8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FOSUserBundle::layout.html.twig", "@FOSUser/Group/list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cce885ae6568db3a3c4bb7031bbaac31f18b65a342ca9acaad9c06737701cc1a = $this->env->getExtension("native_profiler");
        $__internal_cce885ae6568db3a3c4bb7031bbaac31f18b65a342ca9acaad9c06737701cc1a->enter($__internal_cce885ae6568db3a3c4bb7031bbaac31f18b65a342ca9acaad9c06737701cc1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cce885ae6568db3a3c4bb7031bbaac31f18b65a342ca9acaad9c06737701cc1a->leave($__internal_cce885ae6568db3a3c4bb7031bbaac31f18b65a342ca9acaad9c06737701cc1a_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_dac95d0f4c1c71e51a2ff90ac0e452039952114f05a3bc2ddd53816d410a7e15 = $this->env->getExtension("native_profiler");
        $__internal_dac95d0f4c1c71e51a2ff90ac0e452039952114f05a3bc2ddd53816d410a7e15->enter($__internal_dac95d0f4c1c71e51a2ff90ac0e452039952114f05a3bc2ddd53816d410a7e15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("FOSUserBundle:Group:list_content.html.twig", "@FOSUser/Group/list.html.twig", 4)->display($context);
        
        $__internal_dac95d0f4c1c71e51a2ff90ac0e452039952114f05a3bc2ddd53816d410a7e15->leave($__internal_dac95d0f4c1c71e51a2ff90ac0e452039952114f05a3bc2ddd53816d410a7e15_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends "FOSUserBundle::layout.html.twig" %}*/
/* */
/* {% block fos_user_content %}*/
/* {% include "FOSUserBundle:Group:list_content.html.twig" %}*/
/* {% endblock fos_user_content %}*/
/* */
