<?php

/* @FOSUser/Resetting/email.txt.twig */
class __TwigTemplate_ceeea565d5ccaee9598cc2718b151f237a8f9b8e4fb90bf49b9f8b307237837f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a6c995104e060daa8ccdb832e58e55040c63b6dadb3a415a7b2ed663d4f85a3c = $this->env->getExtension("native_profiler");
        $__internal_a6c995104e060daa8ccdb832e58e55040c63b6dadb3a415a7b2ed663d4f85a3c->enter($__internal_a6c995104e060daa8ccdb832e58e55040c63b6dadb3a415a7b2ed663d4f85a3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        $this->displayBlock('body_text', $context, $blocks);
        // line 12
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_a6c995104e060daa8ccdb832e58e55040c63b6dadb3a415a7b2ed663d4f85a3c->leave($__internal_a6c995104e060daa8ccdb832e58e55040c63b6dadb3a415a7b2ed663d4f85a3c_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_7f96945f16c5e160aaa9271ee5b65f78b114e3f34e77b69f0f5b1bbc675c776d = $this->env->getExtension("native_profiler");
        $__internal_7f96945f16c5e160aaa9271ee5b65f78b114e3f34e77b69f0f5b1bbc675c776d->enter($__internal_7f96945f16c5e160aaa9271ee5b65f78b114e3f34e77b69f0f5b1bbc675c776d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('translator')->trans("resetting.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle");
        echo "
";
        
        $__internal_7f96945f16c5e160aaa9271ee5b65f78b114e3f34e77b69f0f5b1bbc675c776d->leave($__internal_7f96945f16c5e160aaa9271ee5b65f78b114e3f34e77b69f0f5b1bbc675c776d_prof);

    }

    // line 7
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_76e51096727ca1e4df73d0a8f19773ce0ea1652b2c4f05eb16310625bcd71b80 = $this->env->getExtension("native_profiler");
        $__internal_76e51096727ca1e4df73d0a8f19773ce0ea1652b2c4f05eb16310625bcd71b80->enter($__internal_76e51096727ca1e4df73d0a8f19773ce0ea1652b2c4f05eb16310625bcd71b80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 9
        echo $this->env->getExtension('translator')->trans("resetting.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_76e51096727ca1e4df73d0a8f19773ce0ea1652b2c4f05eb16310625bcd71b80->leave($__internal_76e51096727ca1e4df73d0a8f19773ce0ea1652b2c4f05eb16310625bcd71b80_prof);

    }

    // line 12
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_515d968d4724afad4caa4347e85749dbbec99e7302c8bb1dd3ab41ada5e9ce76 = $this->env->getExtension("native_profiler");
        $__internal_515d968d4724afad4caa4347e85749dbbec99e7302c8bb1dd3ab41ada5e9ce76->enter($__internal_515d968d4724afad4caa4347e85749dbbec99e7302c8bb1dd3ab41ada5e9ce76_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_515d968d4724afad4caa4347e85749dbbec99e7302c8bb1dd3ab41ada5e9ce76->leave($__internal_515d968d4724afad4caa4347e85749dbbec99e7302c8bb1dd3ab41ada5e9ce76_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  66 => 12,  57 => 9,  51 => 7,  42 => 4,  36 => 2,  29 => 12,  27 => 7,  25 => 2,);
    }
}
/* {% trans_default_domain 'FOSUserBundle' %}*/
/* {% block subject %}*/
/* {% autoescape false %}*/
/* {{ 'resetting.email.subject'|trans({'%username%': user.username}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_text %}*/
/* {% autoescape false %}*/
/* {{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}*/
/* {% endautoescape %}*/
/* {% endblock %}*/
/* {% block body_html %}{% endblock %}*/
/* */
