var cache = {};
 
$(document).ready(function(){
	//$("input[id=recherche]").autocomplete({
            //$("#recherche").autocomplete("widget");
            $("#recherche").autocomplete({
                minLength: 1,
                delay:300,
                html:true,
                autoFocus: true,
                position: {
                    my: "left top",
                    at: "left bottom"
                },
                source: function (request, response)
		{
			//Si la réponse est dans le cache
			if (request.term in cache)
			{
				response($.map(cache[request.term], function (item)
				{
					return {
						label: item.titre,
						value: function ()
						{
							
								//$('input[id=recherche]').val(item.titre);
								return item.titre;
							
						}
					};
				}));
			}
			//Sinon -> Requete Ajax
			else
			{
 
		            var objData = {};
		            //var url = $(this.element).attr('action');

		            //console.log(url);
		            	objData = { motcle: request.term };
		            
				$.ajax({
					url: "/web/app_dev.php/film/search",
                                        contentType: "application/json; charset=utf-8",
					dataType: "json",
					data : objData,
					type: 'GET',
//                                        select : function(event, ui){
//					alert(ui.item.label);
//                                        },
					success: function (data)
					{
                                            
//                                                console.debug('ok');
//						//Ajout de reponse dans le cache
//						cache[request.term] = data;
                                               //console.debug($.parseJSON(response));
						response($.map(data, function (item)
						{
                                                    
							return {
                                                            label: item.titre,
                                                            value: function ()
                                                            {

                                                            //$('input[id=recherche]').val(item.titre);
                                                            return item.titre;
                                                             }

						}; 
						}));
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
                                                console.log('ko');
						console.log(textStatus, errorThrown);
					}
				});
			}
		}
		
	});
});


/* ----------------------------------search autocomplete twitter typeahead------------*/
$(document).ready(function(){
       
       $("#typeahead").typeahead({
        minLength : 1,
        hint: true,
        highlight: true
        //
        },
{
    limit:25, //par defaut cest 5
  async: true,
  source: function (query, processSync, processAsync) {
    //processSync(['This suggestion appears immediately', 'This one too']);
    return $.ajax({
      url: "/web/app_dev.php/film/search", 
      type: 'GET',
      data: {motcle: query},
      dataType: 'json',
      success: function (jsonString) {
      //console.log('ok');
        
 //var jsonString = '[{"label":"System Administrator","value":"1"},{"label":"Software Tester","value":"3"},{"label":" Software Developer","value":"4"},{"label":"Senior Developer","value":"5"},{"label":"Cloud Developer","value":"6"},{"label":"Wordpress Designer","value":"7"}]';
 
var jsonObj = $.parseJSON(JSON.stringify(jsonString));
var sourceArr = [];
 
for (var i = 0; i < jsonObj.length; i++) {
   sourceArr.push(jsonObj[i].titre);
}
        console.log(sourceArr);
        return processAsync(sourceArr);
        //return processAsync(jsonString); // dans le cas de array of strings cest aussi simple que ca
      }
    });
  }
});
   });
