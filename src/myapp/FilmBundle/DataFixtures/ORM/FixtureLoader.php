<?php


namespace myapp\FilmBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use myapp\FilmBundle\Entity\Categorie;
use myapp\FilmBundle\Entity\Acteur;
use myapp\FilmBundle\Entity\Film;

class FixtureLoader implements FixtureInterface
{
  // Dans l'argument de la méthode load, l'objet $manager est l'EntityManager
  public function load(ObjectManager $manager)
  {
    
    $act = new Acteur();
    $act->setNom("barbe");
    $act->setPrenom("sol");
    $act->setDateN(new \Datetime());
    $act->setSexe("F");

    $manager->persist($act);

    $act2 = new Acteur();
    $act2->setNom("fsdsdgg");
    $act2->setPrenom("dsf");
    $act2->setDateN(new \Datetime());
    $act2->setSexe("M");
    $manager->persist($act2);

    $manager->persist($act);
        
    $film = new Film();
    $film->setTitre("blackhawk down");
    $film->setDescription("war in somali");
    $cat=new Categorie();
    $cat->setNom("amusement");
    //$manager->persist($cat);
    $film->setCategorie($cat);
    $film->setTitre("blackhawk down");
    $film->addActeur($act);

    $manager->persist($film);
    // Liste des noms de catégorie à ajouter
    $noms = array(
      'Fiction',
      'Action',
      'Social'
    );

    foreach ($noms as $nom) {
      // On crée la catégorie
      $categorie = new Categorie();
      $categorie->setNom($nom);

      // On la persiste
      $manager->persist($categorie);
    }

    // On déclenche l'enregistrement de toutes les catégories
    $manager->flush();
  }
}
?>
