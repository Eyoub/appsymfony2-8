<?php

namespace myapp\FilmBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;


/**
 * Film
 *
 * @ORM\Table(name="film")
 * @ORM\Entity(repositoryClass="myapp\FilmBundle\Repository\FilmRepository")
 */
class Film
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
        private $description;

    /**
     * @ORM\Column(type="string")
     *
     * 
     * @Assert\File(
     * maxSize = "4096k", 
     * mimeTypes={"image/jpeg", "image/gif", "image/png"}
     * 
     * )
     */
    
    private $poster;

/*
     // @Assert\NotBlank(message="Please, blank field.") assert not blank a obligé doctrine a generer un champ not nullmimeTypesMessage="please upload a valid jpg/png/gif file"

Ce sujet m'a été très utile je me permet donc de le remonter malgré ça date avancé.

Pour être clair,

cascade={remove}  est utile quand tu supprimes une entité père et que tous ces fils doivent être supprimés avec lui. Il faut alors faire attention également aux fils des fils, etc ...

Si tu souhaites supprimer une entité mais ne pas supprimer les entités liées à celle-ci, par exemple tu souhaites supprimer un utilisateur qui quitte ton site mais tu veux garder les niveaux qu'il a créé. Tu vas définir que lorsque le créateur d'un niveau supprime son compte, tu gardes la map avec un créateur null.

@ORM\joinColumn(onDelete="SET NULL")

J'espère que ce résumé pourra être utile.
 */
 /**
 * @ORM\ManyToOne(targetEntity="Categorie", cascade={"persist"}, inversedBy="films")
 * @ORM\JoinColumn(name="Categorie_id", referencedColumnName="id", onDelete="SET NULL")   
 * 
   */
    
    //apres on excecute doctrine:generate:entities FilmBundle 
    //apres on fait update de la base de données doctrine:schema:update --force
    private $categorie;
    
    /**
     * @ORM\ManyToMany(targetEntity="Acteur", inversedBy="films")
     * @ORM\JoinTable(name="Film_Acteur")
     */
    private $acteurs;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Film
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Film
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->acteurs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set categorie
     *
     * @param \myapp\FilmBundle\Entity\Categorie $categorie
     *
     * @return Film
     */
    public function setCategorie(\myapp\FilmBundle\Entity\Categorie $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \myapp\FilmBundle\Entity\Categorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Add acteur
     *
     * @param \myapp\FilmBundle\Entity\Acteur $acteur
     *
     * @return Film
     */
    public function addActeur(\myapp\FilmBundle\Entity\Acteur $acteur)
    {
        $acteur->addFilm($this); //synchronously updating inverse side
        $this->acteurs[] = $acteur;

        return $this;
    }

    /**
     * Remove acteur
     *
     * @param \myapp\FilmBundle\Entity\Acteur $acteur
     */
    public function removeActeur(\myapp\FilmBundle\Entity\Acteur $acteur)
    {
        $this->acteurs->removeElement($acteur);
    }

    /**
     * Get acteurs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActeurs()
    {
        return $this->acteurs;
    }

    /**
     * Set poster
     *
     * @param string $poster
     *
     * @return Film
     */
    public function setPoster($poster)
    {
        $this->poster = $poster;

        return $this;
    }

    /**
     * Get poster
     *
     * @return string
     */
    public function getPoster()
    {
        return $this->poster;
    }
}
