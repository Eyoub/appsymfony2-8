<?php

namespace myapp\FilmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categorie
 *
 * @ORM\Table(name="categorie")
 * @ORM\Entity(repositoryClass="myapp\FilmBundle\Repository\CategorieRepository")
 */
class Categorie
{


    /**
     * @ORM\OneToMany(targetEntity="Film", mappedBy="categorie")  
     * 
     */
  
  private $films;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Categorie
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->films = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add film
     *
     * @param \myapp\FilmBundle\Entity\Film $film
     *
     * @return Categorie
     */
    public function addFilm(\myapp\FilmBundle\Entity\Film $film)
    {
        $this->films[] = $film;
        $film->setCategorie($this);

        return $this;
    }

    /**
     * Remove film
     *
     * @param \myapp\FilmBundle\Entity\Film $film
     */
    public function removeFilm(\myapp\FilmBundle\Entity\Film $film)
    {
        $this->films->removeElement($film);
    }

    /**
     * Get films
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFilms()
    {
        return $this->films;
    }
}
