<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace myapp\FilmBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use myapp\FilmBundle\Entity\Categorie;

/**
 * Description of CategoryController
 *
 * @author chumchum
 */
class CategoryController extends Controller{

    public function new1Action($nom) {
                $em=$this->getDoctrine()->getManager();
                $cat=new Categorie();
                $cat->setNom($nom);
                $em->persist($cat); // remplace prepare dans pdo 3 persistes et un seul flush
                $em->flush(); //persist dans hibernate
                $success= 'success';  
          //retourner toujours render meme vide sinon erreur              
                return $this->render('FilmBundle:Categories:new.html.twig', array('succes'=>$success));
                             //return $this->redirectToRoute('film_index');
    
    }

public function indexAction() {
                    $em=$this->getDoctrine()->getManager();
                $cat=$em->getRepository("FilmBundle:Categorie")->findAll();
                  return $this->render('FilmBundle:Categories:index.html.twig', array('cat'=>$cat));

}

public function newAction(Request $request)
    {
        $categorie = new Categorie();
        $form = $this->createForm('myapp\FilmBundle\Form\CategorieType', $categorie);
        //$request=$this->get('request');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($categorie);
            $em->flush();

            return $this->redirectToRoute('category_index');
        }

        //return $this->render('categorie/new.html.twig', array(
          return $this->render('FilmBundle:Categories:new.html.twig', array(
            'categorie' => $categorie,
            'form' => $form->createView(),
        ));
    }

}