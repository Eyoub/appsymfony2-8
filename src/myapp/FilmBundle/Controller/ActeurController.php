<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace myapp\FilmBundle\Controller;
use myapp\FilmBundle\Entity\Acteur;
use myapp\FilmBundle\Form\ActeurType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
/**
 * Description of ActeurController
 *
 * @author chumchum
 */
class ActeurController extends Controller{
    
    /*
    {{a.date|date('d-M-Y')}}
     *      */
    public function ajoutActAction() {
    	
        $message='Ajouter un acteur';
        $em=$this->getDoctrine()->getManager();
        $acteur=new Acteur();
        $form=$this->createForm(new ActeurType(),$acteur);//synchronisation entre form et entity
        $request=$this->get('request'); //request est celui qui capture les donnees du form
        if($request->getMethod()=='POST'){ //verifier si la methode existe ou non sinon n fait rien
            $form->handleRequest($request);
            if($form->isValid()){ //comme submit en php natif
                $em->persist($acteur);
                $em->flush();
                $message="insertion valide";
                
                return $this->redirect($this->generateUrl("affich_act"));
            }
        }
        return $this->get('templating')->renderResponse('FilmBundle:Acteur:ajoutActeur.html.twig',array('message'=>$message,'form'=>$form->createView()));
    }
    
    public function affichActAction() {
        
        $em=$this->getDoctrine()->getManager();
        $act=$em->getRepository('FilmBundle:Acteur')->findAll();

        
        return $this->get('templating')->renderResponse('FilmBundle:Acteur:affichActeur.html.twig',array('act'=>$act));
        
    }
    
    public function modifActAction($id) {
        $act=new Acteur();
        $em=$this->getDoctrine()->getManager();
        $act=$em->getRepository("FilmBundle:Acteur")->find($id);
        $form=  $this->createForm(new ActeurType(),$act);
        $request=$this->get('request'); //request est celui qui capture les donnees du form
        if($request->getMethod()=='POST'){ //verifier si la methode existe ou non sinon n fait rien
            $form->handleRequest($request);
            if($form->isValid()){ //comme submit en php natif
                $em->persist($act);
                $em->flush();
                $message="insertion valide";
                
             return $this->redirect($this->generateUrl("affich_act"));
            }
        }
             return $this->get('templating')->renderResponse('FilmBundle:Acteur:modifActeur.html.twig',array('form'=>$form->createView()));;

    }
    
    
    
    public function suppActAction($id) {
        
        $em=$this->getDoctrine()->getManager();
        $act=$em->getRepository("FilmBundle:Acteur")->find($id); //c'est une selection et non pas un objet
        $em->remove($act);
        $em->flush();
        return $this->redirect($this->generateUrl("affich_act"));

        
    }
    
    public function modifAct2Action() {
                $em=$this->getDoctrine()->getManager();
        $act=$em->getRepository('FilmBundle:Acteur')->findAll();

        
         return $this->get('templating')->renderResponse('FilmBundle:Acteur:modifActeur2.html.twig',array('act'=>$act));

        
    }
}
