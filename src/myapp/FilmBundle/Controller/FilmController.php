<?php

namespace myapp\FilmBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use myapp\FilmBundle\Entity\Film;

/**
 * Film controller.
 *
 */
class FilmController extends Controller
{

    /**
     * Lists all Film entities.
     *
     */

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $listfilm = $em->getRepository('FilmBundle:Film')->findAll();
        
        $paginator = $this->get('knp_paginator');
        $films = $paginator->paginate(
            $listfilm,
            $request->query->get('page', 1)/*page number*/,
            6/*limit per page*/
        );
        return $this->render('film/index.html.twig', array(
            'films' => $films,
        ));

            }

    /**
     * Creates a new Film entity.
     *
     */
    public function newAction(Request $request)
    {
        $film = new Film();
        $form = $this->createForm('myapp\FilmBundle\Form\FilmType', $film);
        // on peut utiliser FilmType::class au lieu du namespace
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            //meth 3 doctrine event listener
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            /*$file = $film->getPoster();
            $fileName=$this->get('myapp.file_uploader').upload($file);
            $film->setPoster($fileName);*/
            $em->persist($film);
            $em->flush();

            //return $this->redirectToRoute('film_show', array('id' => $film->getId()));
            return $this->redirectToRoute('film_index');
        }

        return $this->render('film/new.html.twig', array(
            'film' => $film,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Film entity.
     *
     */
    public function showAction(Film $film)
    {
        $deleteForm = $this->createDeleteForm($film);

        return $this->render('film/show.html.twig', array(
            'film' => $film,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Film entity.
     *
     */
    public function editAction(Request $request, Film $film)
    {

        $film->setPoster(new File('uploads/' . $film->getPoster(), false));
        //print_r($film->getPoster());

        /*si on passe string en param et non pas l'objet comme dans FilmType exception suivante
        The form's view data is expected to be an instance of class Symfony\Component\HttpFoundation\File\File, but is a(n) string. You can avoid this error by setting the "data_class" option to null or by adding a view transformer that transforms a(n) string to an instance of Symfony\Component\HttpFoundation\File\File.
        $deleteForm = $this->createDeleteForm($film);*/
        $editForm = $this->createForm('myapp\FilmBundle\Form\FilmType', $film);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
//            upload file methode 1 directement dans le controlleur
//            $file = $film->getPoster();
//            // Generate a unique name for the file before saving it
//            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
//
//            // Move the file to the directory where brochures are stored
//            $file->move(
//            $this->getParameter('uploads'),
//            $fileName
//            );
            //methode 3 doctrine event listener qui consommera lui 'myapp.file_uploader'
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */ 
            /*$file=$film->getPoster();
            $fileName=$this->get('myapp.file_uploader')->upload($file);
            //Ne persister que le nom et non pas l contenu du fichier
            $film->setPoster($fileName);*/

            $em->persist($film);
            $em->flush();

            //return $this->redirectToRoute('film_edit', array('id' => $film->getId()));
            return $this->redirectToRoute('film_editer');
        }

        return $this->render('film/edit.html.twig', array(
            'film' => $film,
            'edit_form' => $editForm->createView()
            //'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Film entity.
     *
     */
    public function deleteAction(Request $request, Film $film)
    {
        /* on ne travaille pas avec les delete_form 
        $form = $this->createDeleteForm($film);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($film);
            $em->flush();
        }*/
        $em = $this->getDoctrine()->getManager();
        $em->remove($film);
        $em->flush();


        return $this->redirectToRoute('film_index');
    }

    /**
     * Creates a form to delete a Film entity.
     *
     * @param Film $film The Film entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Film $film)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('film_delete', array('id' => $film->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function editerAction()
    {
        $em = $this->getDoctrine()->getManager();
        $films = $em->getRepository('FilmBundle:Film')->findAll();
        return $this->render('film/editer.html.twig', array(
            'films' => $films
        ));
    }


    public function searchAction(Request $request)
    {

        /*$em = $this->getDoctrine()->getManager();
        $listfilm = $em->getRepository('FilmBundle:Film')->findBy( array('titre'=>$motcle) );*/

        //other alternative when it comes to query by criteria DQL ot DQB(doctrine query builder)
        $motcle = $request->get('motcle');
        
        if($request->isXmlHttpRequest())
        {
            
            //$motcle = $request->get('item');
            $res= $this->getDoctrine()
                ->getEntityManager()
                ->getRepository('FilmBundle:Film')
                ->getFilmsByTitreJSON($motcle);
            $response = new Response(json_encode($res));
            //$response -> headers -> set('Content-Type', 'application/json');
            return $response;
            

            
        }
        $repository = $this->getDoctrine()->getRepository('FilmBundle:Film');
        $listfilm=$repository->getFilmsByTitre($motcle);
        //print_r($listfilm);
        $paginator = $this->get('knp_paginator');
        $films = $paginator->paginate(
            $listfilm,
            $request->query->get('page', 1)/*page number*/,
            6/*limit per page*/
        );
        return $this->render('film/index.html.twig', array(
            'films' => $films,
               
        ));

    }

}
