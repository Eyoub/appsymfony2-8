<?php

namespace myapp\FilmBundle\Controller;

use myapp\FilmBundle\Entity\Categorie;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction(Categorie $cat =null)
    {
        //$this->getDoctrine()->getRepository('FilmBundle:Film')->getFilmsByTitreJSON('f');
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();
        $listCat = $em->getRepository('FilmBundle:Categorie')->findAll();
        //$cat = $em->getRepository('FilmBundle:Categorie')->findBy($id);
        $listFilm = $em->getRepository('FilmBundle:Film')->findAll();
        $filmsByCat = $em->getRepository('FilmBundle:Film')->findBy(array('categorie'=> $cat));
        $em = $this->getDoctrine()->getManager();       
        //print_r($request->getClientIp());

        $paginator  = $this->get('knp_paginator');
        $films = $paginator->paginate(
        (null === $cat) ? $listFilm: $filmsByCat,
        $request->query->get('page', 1)/*page number*/,
        4);/*limit per page*/
                // :: dossier vide et non plus Default
        /*if (null === $cat) {
        return $this->render('FilmBundle::index.html.twig', array('categories' => $listCat, 'films' => $films));
        }*/
        return $this->render('FilmBundle::index.html.twig', array('categories' => $listCat, 'films' => $films));

    }
    
    public function helloSymfonyAction()
    {
        return $this->render('FilmBundle:Default:helloSymfony.html.twig');
    }
    
    public function filmtestAction($v){
        
        return $this->render('FilmBundle:Default:filmtest.html.twig',array('name'=>$v));
        
    }
    
    
    public function calculAction($x, $v){
       
        $perim=($x+$v)*2;
        $surf=$v*$x;
        return $this->render('FilmBundle:Default:rectangle.html.twig',array('perim'=>$perim, 'surf'=>$surf));
        
    }
}
