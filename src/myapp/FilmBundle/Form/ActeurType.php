<?php

namespace myapp\FilmBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActeurType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // expanded est mis dans radio et checkbox mais pas dans select et select multiple sorte de distinguer
        //birthday type recemment ajoute avec php 5.4
        
        //radio/checkbox=>
        $builder
            ->add('nom','text', array('required'=>true))
            ->add('prenom','text',array('required'=>true))
            ->add('dateN', 'birthday',array('format'=>'dd-MM-yyyy', 'years'=>range(date('Y')+5,1940),)) // ya date aujourdhui d(y), 'label'=>'DOB'
            ->add('sexe','choice',array('choices'=>array('F'=>'Feminin', 'M'=>'Masculin'),'required'=>true,'expanded'=>true))
            ->add('films','entity',array('class'=>
                "FilmBundle:Film",'property'=>'titre', 'multiple'=>TRUE, 'required'=>TRUE
                ))

        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'myapp\FilmBundle\Entity\Acteur'
        ));
    }
}
