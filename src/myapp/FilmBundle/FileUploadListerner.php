<?php
namespace myapp\FilmBundle;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use myapp\FilmBundle\Entity\Film;
use myapp\FilmBundle\FileUploader;

class FileUploadListerner {
private $uploader;

    public function __construct(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    private function uploadFile($entity)
    {
        // upload only works for Product entities
        if (!$entity instanceof Film) {
            return;
        }

        $file = $entity->getPoster();

        // only upload new files
        if (!$file instanceof UploadedFile) {
            return;
        }

        $fileName = $this->uploader->upload($file);
        $entity->setPoster($fileName);
    }
    
    
       //This listener can also create the File instance based on the path when fetching entities from the database
    
    /*public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$entity instanceof Film) {
                return;
        }
        
        //$film->setPoster(new File('uploads/' . $film->getPoster(), false));

        $fileName = $entity->getPoster();
        
        $entity->setPoster(new File($fileName));
        print_r($entity);
    }*/
}

